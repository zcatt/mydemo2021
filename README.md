﻿# mydemo2021

Intro
===
Created in 2021
Demos and samples.

使用的环境，
ubuntu 20.04.1

Demo List
===
* linuxProgramming/
    * a01_signal        signal的基本使用.
    * a02_sharedLib     dlopen()/dlsym()/dlclose()装载动态库 *.so
    
* glibDemo/
    * a01_verInfo       版本信息相关.
    * a02_class         gtype创建类， 并实现interface， 以及notfiy挂接。 
                        vscode的meson定制

* gnome-gtk/
    * a01_HelloWorld	gtk3 helloWorld.
    * a02_bloatpad      using gtkwidget basically.  
                        默认menus.ui, help-overlay.ui; basic layout; accelerator, icon and dynamic menu; combo, textViewText, textBuffer, switch, toolbar, toggleToolButton, grid;
    * a03_drawing	cairo基本绘制和mouse event处理.
    * a04_mines	挖地雷。 展示基本的gtk编程。

* eclipseDemo/
    * minimalConsole	osgi的最小化console.
    * zcatt.examples.swt.hello	一个swt tutorial example.
    * zcatt.examples.osgi.hello	一个osgi plugin example.
    * zcatt.examples.osgi.serviceListener	展示service的register和unregister, 以及serviceListener. 同时演示了Bundle.getHeaders()和findEntries().
    * zcatt.examples.osgi.simpleService and zcatt.examples.osgi.simpleClient	展示了两个bundle使用service协作.
    * zcatt.examples.swt.graphics	image和font的绘制
	* zcatt.examples.class.demos	annotation实现的示例
	* zcatt.examples.java.demos		resourceBundle的示例


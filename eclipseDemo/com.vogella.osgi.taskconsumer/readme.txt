osgi service example

see https://www.vogella.com/tutorials/OSGi/article.html

contains the following projects
	com.vogella.osgi.runtime,		product project
	com.vogella.osgi.feature,		feature project
	com.vogella.tasks.model,  		define the service interface
	com.vogella.tasks.services,		impl the service
	com.vogella.osgi.taskconsumer,	use the service
	
	 
2022/1/20

# 操作,
start service后, start和stop client, 观察输出.

# osgi console命令
可以使用osgi_run.product运行该demo. 几个osgi console命令
	ss, 列出当前bundle的状态
	sta <n>, start bundle
	sto <n>, stop bundle	

# 3种创建osgi service方法
zcatt.ex001.*演示了3中创建osgi service的方法,

1.直接使用bundleContext.registerService()创建.	
2.使用declarative service(DS)创建.
	.配置manifest.mf, 添加Service-Component header
	.添加OSGI-INF/DeclarativeService.xml
	.提供impl class. 
3.使用annotation创建.
	.使用@Component, @Activate, @Deactivate等annotation.
	.eclipse自动生成Service-Component描述和OSGI-INF/下的component description xml file. 
	 注意需打开eclipse preference中的ds配置. 
	 
# ServiceTracker
	使用serviceTracker一般分两步,
		1.构造函数指定要跟踪的service
		2.open()开始跟踪, close()关闭结束. 期间使用serviceTracker.getService()/getServiceReference()使用service obj.
	client plugin通常在activator.start()中构建serviceTracker并open, 持有serviceTracker, 
	在activator.stop()关闭该serviceTracker.

	  
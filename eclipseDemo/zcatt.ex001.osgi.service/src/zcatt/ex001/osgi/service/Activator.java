package zcatt.ex001.osgi.service;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

	private static BundleContext context;
	private ServiceRegistration<BasicService> basicServReg;
	
	static BundleContext getContext() {
		return context;
	}

	//负责注册basicService
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		
		System.out.println(getClass().getName() + " start....");
		
		//register basicService
		
		BasicService servObj = new BasicServiceImpl();
		
		//此处直接提供service obj, 是为singleton service scope
		basicServReg = bundleContext.registerService(BasicService.class, servObj, null);		
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;

		System.out.println(getClass().getName() + " stop....");
		
		//注销service. 
		//stop会自动unregister service, 这里仅是展示自主unregister service		
		basicServReg.unregister();		
	}

}

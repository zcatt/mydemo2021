package zcatt.ex001.osgi.service;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

@Component
public class AnnotationServiceImpl implements AnnotationService {

	public AnnotationServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Activate
	void MyActivate(BundleContext bundleContext)
	{
		System.out.println(getClass().getName() + " activate!");		
		
	}

	@Deactivate
	void MyDeactivate(BundleContext bundleContext)
	{
		System.out.println(getClass().getName() + " deactivate!");		
	}

	@Override
	public void doService() {
		System.out.println(getClass().getName() + " do service!");
	}
	
}

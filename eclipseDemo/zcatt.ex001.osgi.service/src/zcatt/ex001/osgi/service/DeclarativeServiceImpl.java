package zcatt.ex001.osgi.service;

import org.osgi.framework.BundleContext;

public class DeclarativeServiceImpl implements DeclarativeService {

	public DeclarativeServiceImpl() {
	}

	void activate(BundleContext bundleContext)
	{
		System.out.println(getClass().getName() + " activate!");		
		
	}

	void deactivate(BundleContext bundleContext)
	{
		System.out.println(getClass().getName() + " deactivate!");		
	}

	@Override
	public void doService() {
		System.out.println(getClass().getName() + " do service!");
	}

}

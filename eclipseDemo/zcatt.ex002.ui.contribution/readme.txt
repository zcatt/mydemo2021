2022/1/22

Derived from eclipse.ui.example.contributions.

演示通过plugin.xml/extPoint添加command, handler, keyBinding.
	.添加command
	.添加handler
	.添加bindings
		.添加key scheme
		.添加key bindings
	.添加menus
		.添加menu
		.添加menuItem
		.添加dynamic menuItems
		.添加toolbar
		.添加toolbarItem
		.添加toolbar control, 两个笑脸

Window > Preference > General/keys中切换scheme到Ex002 Scheme, 观察Hello Menu的变化.

# 相关的extPoints

"org.eclipse.ui.commands", 		添加category, command
"org.eclipse.ui.handlers", 		关联command和handlerClass
"org.eclipse.ui.menus", 		设定menu, menuItem, toolbar, toolbarItem
"org.eclipse.ui.bindings", 		设定key bindings, 包括keyScheme, keyBinding


# 相关的interfaces

AbstractHandler
ExecutionEvent



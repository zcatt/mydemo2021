package zcatt.ex002.ui.contribution;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class AddMenuItemHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		if (window == null) {
			return null;
		}

		Shell shell = window.getShell();
		List<String> itemList = (List<String>) shell.getData(DynamicMenuListContributionItem.KEY);
		if(itemList == null)
		{
			itemList = new ArrayList<String>();
			shell.setData(DynamicMenuListContributionItem.KEY, itemList);			
		}
		
		if(itemList.size() > 9)
		{
			MessageDialog.openWarning(shell,
					"Warning",
					"Too many menuItems to add!");		
			
			return null;
		}
		
		itemList.add("menuItem");
		
		MessageDialog.openInformation(shell,
				"Message",
				"Menu item "+ itemList.size() +" is added!");		
		
		return null;
	}

}

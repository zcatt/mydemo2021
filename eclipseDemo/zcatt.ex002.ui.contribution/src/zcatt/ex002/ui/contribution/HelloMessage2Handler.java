package zcatt.ex002.ui.contribution;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.IHandlerService;

public class HelloMessage2Handler extends AbstractHandler {

	static final String ID = "zcatt.ex002.ui.contribution.command.helloMessage2";
//	public HelloMessageHandler() {
//	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		MessageDialog.openInformation(window.getShell(),
				"Hello Message2",
				"Hello, the world 2!");		
	
		return null;
	}
}

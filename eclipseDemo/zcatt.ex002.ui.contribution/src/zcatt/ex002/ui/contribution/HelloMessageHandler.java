package zcatt.ex002.ui.contribution;


//import javax.inject.Inject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
//import org.eclipse.jface.bindings.BindingManager;
//import org.eclipse.jface.bindings.Scheme;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
//import org.eclipse.ui.handlers.IHandlerService;
//import org.eclipse.ui.keys.IBindingService;

public class HelloMessageHandler extends AbstractHandler {

	//see plugin.xml
	static final String ID = "zcatt.ex002.ui.contribution.command.helloMessage2";
	static final String SCHEME_ID= "zcatt.ex002.ui.contribution.scheme";
	
//	public HelloMessageHandler() {
//	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		
		MessageDialog.openInformation(window.getShell(),
				"Hello Message",
				"Hello, the world!");		
		
		//为helloMessage2 command 挂接handler, 即使得菜单项helloMessage2可用.
//		IHandlerService handlerService = window.getService(IHandlerService.class);
//		handlerService.activateHandler(HelloMessage2Handler.ID, new HelloMessage2Handler());
		
//		IBindingService bindingService = window.getService(IBindingService.class);
//		System.out.println(bindingService.getActiveScheme());

		return null;
	}
}

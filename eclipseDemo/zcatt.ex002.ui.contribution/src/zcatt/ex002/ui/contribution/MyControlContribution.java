package zcatt.ex002.ui.contribution;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.menus.WorkbenchWindowControlContribution;

//给toolbar添加一个custom button.
public class MyControlContribution extends WorkbenchWindowControlContribution {

	@Override
	protected Control createControl(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);

		FillLayout layout = new FillLayout();
		layout.marginHeight = 1;
		layout.marginWidth = 2;
		comp.setLayout(layout);

//		Label ccCtrl = new Label(comp, SWT.BORDER | SWT.CENTER);
//		ccCtrl.setText("my control");
		
		Button btn = new Button(comp, SWT.PUSH);
		btn.setText("my btn");
		
		btn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				super.widgetSelected(e);
				
				MessageDialog.openInformation(btn.getShell(),
						"Hi",
						"This is my button!");						
			}
			
		});
		return null;
	}

}

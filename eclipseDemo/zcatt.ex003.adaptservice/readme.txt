2022/1/24

derived from org.eclipse.ui.examples.adapterservice
演示了,
	.extP="org.eclipse.core.runtime.adapters", 声明adapter.
	.EclipseContextFactory取得EclispeContextOSGi
	.IEclipseContext的set和get.
	.extP="org.eclipse.core.runtime.applications", 声明app
	
要点, 
	.eclipse使用EclipseContextOSGi做为保存services的root eclipseCtx.
	 取得方法有
	 	IEclipseContext osgiCtx = EclipseContextFactory.getServiceContext(bundleCtx);
	 	IEclipseContext context = osgiCtx.createChild();	//通常如此, 不污染osgiCtx
	 	IEclipseContext context = IServiceLocator.getService(IEclipseContext.class)
	 	例如, 
	 		IWorkbenchPart.getSite().getService(IEclipseContext.class)
	 		PlatformUI.getWorkbench().getService(IEclipseContext.class)
	.eclipseCtx中创建可注入的service的方法, 以Adapter.class service为例,
	 	context.set(Adapter.class.getName(), ContextInjectionFactory.make(EclipseAdapter.class, context));
	.注册EclipseAdapter adapter的两种方法
		.使用extP="org.eclipse.core.runtime.adapters"声明的adapter.
		.直接编码,使用
			IAdapterManager adapterMgr = AdapterManager.getDefault()
			或者,
			IAdapterManager adapterMgr = context.get(IAdapterManager.class);
			或者,
			IAdapterManager adapterMgr = Platform.getAdapterManager();
			
			adapterMgr.registerAdapters()
	.取得adapter service obj的方法有两种,
		.直接编码
			Adapter adapter = context.get(Adapter.class);	 	
		.使用@Inject
			localCtx.set(Adapter.class, ContextInjectionFactory.make(EclipseAdapter.class, localCtx));		
			................		
		
			ContextInjectionFactory.inject(MyClassObj, context);
			
			class MyClass {
				@Inject
				Adapter adapter;
				............
				............
			}	 	
	

package zcatt.ex003.adaptservice;

import javax.inject.Inject;

import org.eclipse.core.internal.runtime.AdapterManager;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.e4.core.services.adapter.Adapter;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

@SuppressWarnings("restriction")
public class App implements IApplication {

	@Inject
	Adapter adapter;
	
	public App() {
	}

	@Override
	public Object start(IApplicationContext context) throws Exception {
		System.out.println("App start.....");
		
		//手工编码注册adapater
		//eclipse framework中可以用以下几种方式得到IAdapterManger
		//IAdapterManager adapterMgr = AdapterManager.getDefault();	
		//IAdapterManager adapterMgr = Platform.getAdapterManager();
		IAdapterManager adapterMgr = Activator.getEclipseCtx().get(IAdapterManager.class);
		adapterMgr.registerAdapters(new IcAdapter(), Object.class);		
		
		//两种取得adapter的service的方法, 
		//方法1. 使用注入方法取得adapter
		Activator.injectContext(this);	//L11
		
		//方法2. 手工编码取得adapter
		//adapter = Activator.getEclipseCtx().get(Adapter.class);		
		
		Assert.isNotNull(adapter);
		
		//使用adapter
		
		CB cb = new CB();
		
		//adapt to self
		CB b = adapter.adapt(cb, CB.class);
		System.out.println((b!=null) ? "OK, adapt to CB." : "FAIL, adapt to CB");

		//adapt to super
		CA a = adapter.adapt(cb, CA.class);
		System.out.println((a!=null) ? "OK, adapt to CA." : "FAIL, adapt to CA");

		//adapt to child by CcAdapter service
		CC c = adapter.adapt(cb, CC.class);
		System.out.println((c!=null) ? "OK, adapt to CC." : "FAIL, adapt to CC");		
		
		//adapt to child by IcAdapter service
		IC ic = adapter.adapt(cb, IC.class);
		System.out.println((ic!=null) ? "OK, adapt to IC." : "FAIL, adapt to IC");		
		
		return IApplication.EXIT_OK;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
	}

}

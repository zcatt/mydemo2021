package zcatt.ex003.adaptservice;

import org.eclipse.core.runtime.IAdapterFactory;

public class CcAdapter implements IAdapterFactory {

	public CcAdapter() {
	}

	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
//		System.out.println("CcAdapter.getAdapter()");

		if(CC.class.equals(adapterType))
		{
			if(CA.class.isInstance(adaptableObject))
			{
				System.out.println("CcAdapter adapts one obj.");
				return adapterType.cast(new CC());
			}
		}
		return null;
	}

	@Override
	public Class<?>[] getAdapterList() {
		return new Class[] {CC.class};
	}

}

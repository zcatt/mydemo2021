package zcatt.ex003.adaptservice;

import org.eclipse.core.runtime.IAdapterFactory;

public class IcAdapter implements IAdapterFactory {

	public IcAdapter() {
	}

	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		System.out.println("IcAdapter.getAdapter()");
		
		if(IC.class.equals(adapterType))
		{
			if(CA.class.isInstance(adaptableObject))	//zf, 源obj应是CA obj.
			{
				System.out.println("IcAapter adapts one obj.");
				return adapterType.cast(new IC() {});
			}
		}
		return null;
	}

	@Override
	public Class<?>[] getAdapterList() {	//zf, 支持的目标type
		return new Class[] {IC.class};
	}

}

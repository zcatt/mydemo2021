2022/1/25

演示了, 
	.使用extP="org.eclipse.ui.services"/<serviceFactory>配置IServiceLocator支持的service
		text中contextMenu中"Call my service"menuItem会调用myService, 由myService弹出对话框
	.使用extP="org.eclipse.ui.services"/<sourceProvider>配置ISourceProvider.class对应的service, 提供variable.以及使用variable.
		text中contextMenu中"Toggle command"menuItem会调用mySourceProvider, 实现切换toolbar item的enbaled状态. 
		
			 	
要点,
	.<serviceFactory factoryClass="...">需要继承AbstractServiceFactory
	 代码中取得service obj的方式,
			IMyService service = getSite().getService(IMyService.class);
	 
	.<sourceProvider provider="...">需要继承AbstractSourceProvider.
	 代码中取得sourceProvider的方式, 
 			ISourceProviderService sources = getSite().getService(ISourceProviderService.class);
			MySourceProvider mySourceProvider = (MySourceProvider) sources.getSourceProvider(MySourceProvider.VARIABLE);
	 
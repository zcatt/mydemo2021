package zcatt.ex005.ui.service;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.services.IServiceLocator;

public class MyService implements IMyService {

	IServiceLocator locator;
	public MyService(IServiceLocator locator) {
		this.locator = locator;
	}

	@Override
	public void doService(Shell shell) {
//		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
//		Shell shell = locator.getService(IWorkbenchWindow.class).getShell();
		MessageDialog.openInformation(shell,
				"My service",
				"Do my service");		
	}

}

package zcatt.ex005.ui.service;

import org.eclipse.ui.services.AbstractServiceFactory;
import org.eclipse.ui.services.IServiceLocator;

public class MyServiceFactory extends AbstractServiceFactory {

	public MyServiceFactory() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object create(Class serviceInterface, IServiceLocator parentLocator, IServiceLocator locator) {
		if (!IMyService.class.equals(serviceInterface)) {
			return null;
		}
		return new MyService(locator);
	}

}

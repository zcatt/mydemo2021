package zcatt.ex005.ui.service;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.ui.AbstractSourceProvider;
import org.eclipse.ui.ISources;

public class MySourceProvider extends AbstractSourceProvider {
	public static final String VARIABLE = "zcatt.ex005.ui.service.commandstate";
	private static final String[] PROVIDED_SOURCE_NAMES = new String[] { VARIABLE };
	
	boolean isEnabled = true;
	
	public MySourceProvider() {
	}

	public void toggle()
	{
		isEnabled = !isEnabled;
		fireSourceChanged(ISources.ACTIVE_SITE << 1, VARIABLE, isEnabled);
	}
	
	@Override
	public void dispose() {
	}

	@Override
	public Map<?,?> getCurrentState() {
		Map<String, Object> m = new HashMap<>();
		m.put(VARIABLE, isEnabled);
		return m;
	}

	@Override
	public String[] getProvidedSourceNames() {
		return PROVIDED_SOURCE_NAMES;
	}

}

package zcatt.ex005.ui.service;


import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.*;
import org.eclipse.ui.services.ISourceProviderService;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.graphics.Image;
import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.*;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import javax.inject.Inject;


/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class MyView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "zcatt.ex005.ui.service.MyView";

	@Inject IWorkbench workbench;
	
	Text text;
	Action action1;
	Action action2;	 


	@Override
	public void createPartControl(Composite parent) {
		text = new Text(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		text.setText("zcatt.ex005.ui.service\n");
		
		makeActions();
		hookContextMenu();
//		contributeToActionBars();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				MyView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(text);
		text.setMenu(menu);
		//getSite().registerContextMenu(menuMgr, null);
	}

//	private void contributeToActionBars() {
//		IActionBars bars = getViewSite().getActionBars();
//		fillLocalPullDown(bars.getMenuManager());
//		fillLocalToolBar(bars.getToolBarManager());
//	}
//
//	private void fillLocalPullDown(IMenuManager manager) {
//		manager.add(action1);
//		manager.add(new Separator());
//		manager.add(action2);
//	}
//
//
//	private void fillLocalToolBar(IToolBarManager manager) {
//		manager.add(action1);
//		manager.add(action2);
//	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(action1);
		manager.add(action2);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void makeActions() {
		action1 = new Action() {
			public void run() {
				//调用plugin.xml中声明的IMyService service
				IMyService service = getSite().getService(IMyService.class);
				service.doService(text.getShell());				
			}
		};
		action1.setText("Call my service");
		action1.setToolTipText("Call my service tooltip");
		
		action2 = new Action() {
			public void run() {
				ISourceProviderService sources = getSite().getService(ISourceProviderService.class);
				MySourceProvider mySourceProvider = (MySourceProvider) sources.getSourceProvider(MySourceProvider.VARIABLE);
				mySourceProvider.toggle();
				
				showMessage("Toggle the command.\n Attention to the 'smile' toolButton.");				
			}
		};
		action2.setText("Toggle command");
		action2.setToolTipText("Toggle command tooltip");
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(
			text.getShell(),
			"My View",
			message);
	}

	@Override
	public void setFocus() {
		text.setFocus();
	}
}

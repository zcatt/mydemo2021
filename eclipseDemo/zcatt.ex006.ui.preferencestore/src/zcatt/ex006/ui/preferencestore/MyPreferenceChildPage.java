package zcatt.ex006.ui.preferencestore;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class MyPreferenceChildPage extends FieldEditorPreferencePage 
									implements IWorkbenchPreferencePage {

	public MyPreferenceChildPage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Child page.");
	}

//	public MyPreferenceChildPage(int style) {
//		super(style);
//		// TODO Auto-generated constructor stub
//	}
//
//	public MyPreferenceChildPage(String title, int style) {
//		super(title, style);
//		// TODO Auto-generated constructor stub
//	}
//
//	public MyPreferenceChildPage(String title, ImageDescriptor image, int style) {
//		super(title, image, style);
//		// TODO Auto-generated constructor stub
//	}

	@Override
	protected void createFieldEditors() {
//		Label label;
//		label= new Label(getFieldEditorParent(), SWT.WRAP);
//		label.setText("nothing to do.");
//
//		addField(new BooleanFieldEditor(
//				MyPreferenceConstants.SNAPTOGRID,
//				"snap to grid",
//				getFieldEditorParent()));		
//		
//		label= new Label(getFieldEditorParent(), SWT.WRAP);
//		label.setText("nothing to do.");
	}

	@Override
	public void init(IWorkbench workbench) {
		// TODO Auto-generated method stub
		
	}

}

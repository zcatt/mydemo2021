package zcatt.ex006.ui.preferencestore;

public class MyPreferenceConstants {
	public static final String STYLE = "style";
	public static final String STYLE_NORMAL = "simpleLine";
	public static final String STYLE_DASH = "dashLine";
	public static final String STYLE_DOT = "dotLine";
	
	public static final String WIDTH = "width";
	
	public static final String SNAPTOGRID = "snapToGrid";
}

package zcatt.ex006.ui.preferencestore;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
//import org.eclipse.ui.preferences.ScopedPreferenceStore;

public class MyPreferenceInitializer extends AbstractPreferenceInitializer {

	public MyPreferenceInitializer() {
	}

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(MyPreferenceConstants.STYLE, MyPreferenceConstants.STYLE_NORMAL);
		store.setDefault(MyPreferenceConstants.WIDTH, 1);
		store.setDefault(MyPreferenceConstants.SNAPTOGRID, false);

		
//		store.setValue(MyPreferenceConstants.SNAPTOGRID, true);
//		
//		ScopedPreferenceStore scopedStore = (ScopedPreferenceStore) store;
//		try {
//			scopedStore.save();			
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
	}
}

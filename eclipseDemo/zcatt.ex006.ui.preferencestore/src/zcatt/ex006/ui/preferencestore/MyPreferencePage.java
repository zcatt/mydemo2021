package zcatt.ex006.ui.preferencestore;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class MyPreferencePage extends FieldEditorPreferencePage 
							implements IWorkbenchPreferencePage {

	public MyPreferencePage() {
		super(GRID);				//注意此句
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("My Preference Page");
		
	}

//	public MyPreferencePage(int style) {
//		super(style);
//	}
//
//	public MyPreferencePage(String title, int style) {
//		super(title, style);
//	}
//
//	public MyPreferencePage(String title, ImageDescriptor image, int style) {
//		super(title, image, style);
//	}

	@Override
	protected void createFieldEditors() {
		addField(new RadioGroupFieldEditor(
				MyPreferenceConstants.STYLE,
				"line style:",
				1,
				new String[][] {
						{"simple line", MyPreferenceConstants.STYLE_NORMAL},
						{"dash line", MyPreferenceConstants.STYLE_DASH },
						{"dot line", MyPreferenceConstants.STYLE_DASH }
				},
				getFieldEditorParent()));
		IntegerFieldEditor editor = new IntegerFieldEditor(
				MyPreferenceConstants.WIDTH,
				"line width:",
				getFieldEditorParent());
		editor.setValidRange(0, 10);
		addField(editor);
		
		Label label = new Label(getFieldEditorParent(), SWT.WRAP);
		label.setText("options:");
		addField(new BooleanFieldEditor(
				MyPreferenceConstants.SNAPTOGRID,
				"snap to grid",
				getFieldEditorParent()));		
	}

	@Override
	public void init(IWorkbench workbench) {
	}

}

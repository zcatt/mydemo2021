package zcatt.ex008.ui.navigator;

import org.eclipse.core.resources.IFile;

public class ChapterTreeData {
	IFile file;
	String name;

	public ChapterTreeData(String name, IFile file) {
		this.name = name;
		this.file = file;		
	}
	
	public IFile getFile()
	{
		return file;
	}
	
	public String getName()
	{
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof ChapterTreeData
				&&((ChapterTreeData)obj).getName().equals(name);
	}

	@Override
	public String toString() {
		return name;
	}
	
	

}

package zcatt.ex008.ui.navigator;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.navigator.IDescriptionProvider;

public class MyLabelProvider extends LabelProvider implements IDescriptionProvider {

	@Override
	public Image getImage(Object element) {
		if (element instanceof ChapterTreeData)
			return PlatformUI.getWorkbench().getSharedImages().getImage(
					ISharedImages.IMG_OBJS_INFO_TSK);
		return null;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof ChapterTreeData) {
			ChapterTreeData data = (ChapterTreeData) element;
			return data.getName();
		}
		return null;
	}

	@Override
	public String getDescription(Object anElement) {
		if (anElement instanceof ChapterTreeData) {
			ChapterTreeData data = (ChapterTreeData) anElement;
			return "Property: " + data.getName();
		}
		return null;
	}
}

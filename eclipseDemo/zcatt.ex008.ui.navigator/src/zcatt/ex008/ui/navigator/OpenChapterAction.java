package zcatt.ex008.ui.navigator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.FindReplaceDocumentAdapter;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

public class OpenChapterAction extends Action {
	private IWorkbenchPage page;
	private ChapterTreeData data;
	private ISelectionProvider provider;

	public OpenChapterAction(IWorkbenchPage p, ISelectionProvider selectionProvider) {
		setText("Open Chapter");
		page = p;
		provider = selectionProvider;
	}

	
	@Override
	public boolean isEnabled() {
		ISelection selection = provider.getSelection();
		if(!selection.isEmpty()) {
			IStructuredSelection sSelection = (IStructuredSelection) selection;
			if(sSelection.size() == 1 &&
				sSelection.getFirstElement() instanceof ChapterTreeData)
			{
				data = ((ChapterTreeData)sSelection.getFirstElement());
				return true;
			}
		}
		return false;
	}

	@Override
	public void run() {
		try {
			if(isEnabled()) {
				IFile file = data.getFile();
				IEditorPart editor = IDE.openEditor(page, file);		//zf, open

				if (editor instanceof ITextEditor) {
					ITextEditor textEditor = (ITextEditor) editor;

					IDocumentProvider documentProvider =
						textEditor.getDocumentProvider();
					IDocument document =
						documentProvider.getDocument(editor.getEditorInput());

					FindReplaceDocumentAdapter searchAdapter = new FindReplaceDocumentAdapter(document);

					try {
						String searchText = data.getName(); 
						IRegion region = searchAdapter.find(0,
															searchText,
															true /* forwardSearch */,
															true /* caseSensitive */,
															false /* wholeWord */,
															false /* regExSearch */);

						((ITextEditor)editor).selectAndReveal(region.getOffset(), region.getLength());

					} catch (BadLocationException e) {
						Platform.getLog(OpenChapterAction.class).error("Could not open property!", e);
						MessageDialog.openError(Display.getDefault().getActiveShell(),
								"Error Opening Chapter",  
								"Could not open chapter!"); 
					}
					return;
				}
			}
		} catch (PartInitException e) {
			Platform.getLog(OpenChapterAction.class).error("Could not open chapter!", e);
			MessageDialog.openError(Display.getDefault().getActiveShell(),
					"Error Opening Chapter", 
					"Could not open chapter!");  
		}
	}



}

package zcatt.demo;

import java.util.EventListener;

public interface ChangeListener extends EventListener {
	public void change(ChangeEvent evt);
}

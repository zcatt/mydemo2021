package zcatt.demo;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)			//RUNTIME方能保证Class的getAnnotation()等能够有效.
@Target({ TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR })
public @interface Gender {
	String description() default "male";

}

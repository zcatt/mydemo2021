package zcatt.demo;

import static java.lang.System.out;

@Gender(description="male")
public class Manager extends Person implements ChangeListener {

	int years;
	public Manager()
	{
		years = 0;
	}

	public void printClassName()
	{
		out.println("Manager class.");
	}
	
	public int getJobYears()
	{
		out.println("Manager.getJobYears() is called, years = "+years);
		return years;
	}
	public void setJobYears(int years)
	{
		this.years = years;		
	}
	
	@Override
	protected void protectMethod()
	{		
	}
	
	@Override
	public void change(ChangeEvent evt) {
	}
	

}

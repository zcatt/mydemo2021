package zcatt.demo;

import static java.lang.System.out;

@Gender(description="undefine")
public abstract class Person {
	String name;
	public Person()
	{}
	
	public void printClassName()
	{
		out.println("Person class.");
	}
	
	public String getName()
	{
		return name;		
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	protected abstract void protectMethod();
	
	private void privMethod()
	{}
	
}

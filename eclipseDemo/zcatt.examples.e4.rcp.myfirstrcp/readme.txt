zcatt.examples.e4.rcp.myfirstrcp
eclipse 4.20

This is a simple e4 rcp sample created by plugin project wizard. 

Attention to choose in these wizard steps,
	."This plug-in  will make contribution to the UI" checkbox.
	.(e4) RCP application
	."create sample content(part, menu, command...)" checkbox.
package zcatt.examples.java.demos;

import java.util.ResourceBundle;

public class Snippet001_ResourceBundleDemo {

	public static void main(String[] args) {
		ResourceBundle resBundle = ResourceBundle.getBundle("myresource");
		String s = resBundle.getString("HelloWorld");
		System.out.println(s);
	}

}

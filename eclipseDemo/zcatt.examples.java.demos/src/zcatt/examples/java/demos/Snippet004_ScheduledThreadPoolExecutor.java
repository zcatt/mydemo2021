package zcatt.examples.java.demos;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/*
 * 本例演示ScheduledThreadPoolExecutor的使用.
 * 
 */

public class Snippet004_ScheduledThreadPoolExecutor {

	public static void main(String[] args) {
		System.out.println("Snippet004_ScheduledThreadPoolExecutor start....");
		
		ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(4);
		
		Runnable r = new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				}
				catch(Exception e)
				{				
				}
				
				System.out.println("thread("+Thread.currentThread().getId()+"), hello! ");
			}
			
		};
		
		stpe.scheduleAtFixedRate(r,  10, 500, TimeUnit.MILLISECONDS);
		stpe.scheduleAtFixedRate(r,  11, 500, TimeUnit.MILLISECONDS);
		stpe.scheduleAtFixedRate(r,  11, 500, TimeUnit.MILLISECONDS);
		stpe.scheduleAtFixedRate(r,  12, 500, TimeUnit.MILLISECONDS);
		
		try {
			Thread.sleep(5000);
		}
		catch(Exception e)
		{				
		}
		
		stpe.shutdown();	

		try {
			while(! stpe.awaitTermination(50, TimeUnit.MILLISECONDS))
			{
				System.out.println("Main thread waiting....");
			}
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		

		System.out.println("Snippet004_ScheduledThreadPoolExecutor over");
	}

}

package zcatt.examples.jface.demos;

import org.eclipse.swt.widgets.*;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.dialogs.MessageDialog;

public class MessageDialogDemo {

	public static void main(String[] args) {
		Display display = new Display();
		
		MessageDialogWithToggle diag =  MessageDialogWithToggle.openInformation(null, "sunny", "Is it a sunny day?", "sunny or not.", true, null, null);
		boolean res = diag.getToggleState();
		
		MessageDialog.openInformation(null, "Info", res ? "It is a lovely sunny day.": "It is not a sunny day.");
		
		
		display.dispose();
	}

}

package zcatt.examples.jface.demos;

import org.eclipse.jface.notifications.AbstractNotificationPopup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class NotificationPopupDemo {

	static Display display= new Display();	
	static Shell shell = new Shell(display);

	public static void main(String[] args) {
		shell.setText("NotificationPopupDemo");
		
		SelectionAdapter s = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String text = ((Button) e.widget).getText();
				NotificationPopUp notificationPopUp = new NotificationPopUp(
						display, text);
				notificationPopUp.open();
			}
		};
		
		
		shell.setLayout(new FillLayout(SWT.VERTICAL));
		Button a = new Button(shell, SWT.PUSH);
		a.setText("Popup notification");
		a.addSelectionListener(s);
		
		shell.setSize(800, 600);		
		shell.open();		

		while(!shell.isDisposed())
			if(!display.readAndDispatch())
				display.sleep();

		display.dispose();
	}


	private static class NotificationPopUp extends AbstractNotificationPopup {

		private String fText;

		public NotificationPopUp(Display display, String text) {
			super(display);
			this.fText = text;
			setParentShell(shell);
			setDelayClose(1000);
		}

		@Override
		protected String getPopupShellTitle() {
			return "Notification";
		}

		@Override
		protected void createContentArea(Composite parent) {
			Label label = new Label(parent, SWT.WRAP);
			label.setText(fText);
		}
	}



}

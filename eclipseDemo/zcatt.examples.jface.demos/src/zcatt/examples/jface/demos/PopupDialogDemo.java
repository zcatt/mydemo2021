package zcatt.examples.jface.demos;

//import static java.lang.System.out;

import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.jface.preference.JFacePreferences;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.*;


public class PopupDialogDemo {

	public static void main(String[] args) {
		Display display= new Display();
		
		Shell shell = new Shell(display);
		shell.setText("PopupDialogDemo");
		shell.setSize(800, 600);		
		shell.open();		
		
		JFaceResources.getColorRegistry().put(JFacePreferences.CONTENT_ASSIST_BACKGROUND_COLOR, new RGB(255,255,255));
		JFaceResources.getColorRegistry().put(JFacePreferences.CONTENT_ASSIST_FOREGROUND_COLOR, new RGB(255,0,0));
		PopupDialog diag = new PopupDialog(shell
									, PopupDialog.INFOPOPUP_SHELLSTYLE
									, false, false, false, false, false
									, "title text", "info text"
									);
		diag.open();
		
		while(!shell.isDisposed())
			if(!display.readAndDispatch())
				display.sleep();

		display.dispose();
	}

}

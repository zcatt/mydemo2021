/*
	演示了Window的使用. 
	jface Window是topLevel window. 
	若构造时的入参parentShell是null, 则创建的是独立的topLevel window, 否则就是child topLevel window.
	Window.setBlockOnOpen(true)则会在Window.open()时进入消息循环, 直至window dispose.
	window创建的三部曲,
		.new Winodw()
		.create()   可以省略, open()时会自动调用
		.open()		打开, 此后visible. 
 */
package zcatt.examples.jface.snippets;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Snippet001SimpleWindow extends Window {

	public Snippet001SimpleWindow(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		
		newShell.setText("snippet001");
	}

	@Override
	protected Control createContents(Composite parent) {
		//super.createContents(parent);
		Composite comp = new Composite(parent, SWT.BORDER);
		comp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		comp.setBackground(new Color(255, 255, 0));
		comp.setLayout(new GridLayout());
		Button btn = new Button(comp, SWT.PUSH);
		btn.setText("new window");
		btn.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));
		
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			Snippet001SimpleWindow w = new Snippet001SimpleWindow(getShell());
			w.open();
			w.getShell().setText("child window");
		}));
		return comp;
	}
/*
	@Override
	protected Point getInitialLocation(Point initialSize) {
		// TODO Auto-generated method stub
		return super.getInitialLocation(initialSize);
	}

*/

	@Override
	protected Point getInitialSize() {
		//return super.getInitialSize();
		return new Point(300,200);
	}
	
	public static void main(String[] args) {
		Display display = new Display();		//the default display, Display.getCurrent() 
		Snippet001SimpleWindow simpleWindow = new Snippet001SimpleWindow(null);
		simpleWindow.setBlockOnOpen(true);

		simpleWindow.open();
/*		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
*/
		
		display.dispose();
		assert Display.getCurrent() == null;
/*		
		if(Display.getCurrent() != null)
		{
			System.out.println("dispose display.");
			Display.getCurrent().dispose();
		}
*/		
	}

}

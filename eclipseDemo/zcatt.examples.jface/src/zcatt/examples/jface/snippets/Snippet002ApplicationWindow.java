/*
	Application自带layout, 完成对toolBar, coolBar, clientComposite, statusLine的布局.
	使用步骤,
		.new ApplicationWindow()
		.调用addMenuBar(), addToolBar(), addCoolBar(), 和addStatusLine()配置appWindow是否带相应部件
		.实现相应的createXXXManager(), 定制相关的menuBar, toolBar, coolBar, 和statusLine.
		.appWin.open()



 */
package zcatt.examples.jface.snippets;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class Snippet002ApplicationWindow extends ApplicationWindow {

	public static ApplicationWindow app;

	Action newAction;
	Action openAction;
	Action saveAction;
	Action saveAsAction;
	Action exitAction;
	Action copyAction;
	Action cutAction;
	Action pasteAction;
	Action helpAction;
	Text content;
	
	class MyAction extends Action{
		public MyAction(String text, String tooltip, int keycode, String img)
		{
			super(text);
			
			setToolTipText(tooltip);
			
			if(keycode != 0)
				setAccelerator(keycode);

			if(img != null && !img.isEmpty())
			{
				ImageDescriptor desc = ImageDescriptor.createFromFile(Snippet002ApplicationWindow.class, img);
				setImageDescriptor(desc);
			}
		}
		
		public void run()
		{
			MessageBox dialog = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_WARNING | SWT.OK);
			dialog.setText("Action");
			dialog.setMessage("do "+getText());
			dialog.open();
		}
	}
	
	public Snippet002ApplicationWindow() {
		super(null);
		
		app = this;
		
		newAction = new MyAction("&New", "new file", SWT.CTRL  + 'N', "file_new.png");
		openAction = new MyAction("&Open", "open file", SWT.CTRL  + 'O', "file_open.png");
		saveAction = new MyAction("&Save", "save file", SWT.CTRL  + 'S', "file_save.png");
		saveAsAction = new MyAction("Save &as", "save as file", SWT.CTRL  + 'A', "file_saveas.png");
		exitAction = new MyAction("E&xit", "exit app", 0, null);
		copyAction = new MyAction("&Copy", "copy to clipboard", SWT.CTRL  + 'C', "edit_copy.png");
		cutAction = new MyAction("Cut(&X)", "cut", SWT.CTRL  + 'X', "edit_cut.png");
		pasteAction = new MyAction("&Paste", "paste from clipboard", SWT.CTRL  + 'P', "edit_paste.png");
		helpAction = new MyAction("&Help", "help", 0, "help.png");
		
		
		addMenuBar();
		addToolBar(SWT.FLAT);
		addStatusLine();
		
		//创建menu
		
		MenuManager menuBar = getMenuBarManager();
		
		MenuManager fileMenu = new MenuManager("文件(&F)");
		MenuManager editMenu = new MenuManager("编辑(&E)");
		MenuManager helpMenu = new MenuManager("帮助(&H)");
		
		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(helpMenu);
		
		fileMenu.add(newAction);
		fileMenu.add(openAction);
		fileMenu.add(new Separator());
		fileMenu.add(saveAction);
		fileMenu.add(saveAsAction);
		fileMenu.add(new Separator());
		fileMenu.add(exitAction);
		
		editMenu.add(copyAction);
		editMenu.add(cutAction);
		editMenu.add(pasteAction);
		
		helpMenu.add(helpAction);
		
		//创建toolbar

		ToolBarManager toolbarMgr = getToolBarManager();
		
		toolbarMgr.add(new GroupMarker("file"));
		toolbarMgr.add(new GroupMarker("edit"));
		toolbarMgr.add(new GroupMarker("help"));
		
		
		toolbarMgr.appendToGroup("file", newAction);
		toolbarMgr.appendToGroup("file", openAction);
		toolbarMgr.appendToGroup("file", saveAction);
		toolbarMgr.appendToGroup("file", saveAsAction);
		
		toolbarMgr.appendToGroup("edit", new Separator());
		toolbarMgr.appendToGroup("edit", copyAction);
		toolbarMgr.appendToGroup("edit", cutAction);
		toolbarMgr.appendToGroup("edit", pasteAction);		
		
		toolbarMgr.appendToGroup("help", new Separator());
		toolbarMgr.appendToGroup("help", helpAction);

		//statusLine initialization
		
		setStatus("Ready");

		
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);		
		shell.setText("Snippet002ApplicationWindow");
		shell.setMaximized(true);				
	}

	@Override
	protected Point getInitialSize() {
		//return super.getInitialSize();
		return new Point(300,200);
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite comp = new Composite(parent, SWT.BORDER);
		comp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		comp.setBackground(new Color(200, 200, 0));
		comp.setLayout(new GridLayout());
		Label label = new Label(comp, SWT.PUSH);
		label.setText("new window");
		label.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));
		
		return comp;
	}
	
	
//	
//	@Override
//	protected void createStatusLine(Shell shell) {
//		super.createStatusLine(shell);
//		setStatus("Ready");
//	}
//
//	
//	@Override
//	protected MenuManager createMenuManager() {
//		//MenuManager menuBar = new MenuManager();
//		MenuManager menuBar = super.createMenuManager();
//		
//		MenuManager fileMenu = new MenuManager("文件(&F)");
//		MenuManager editMenu = new MenuManager("编辑(&E)");
//		MenuManager helpMenu = new MenuManager("帮助(&H)");
//		
//		menuBar.add(fileMenu);
//		menuBar.add(editMenu);
//		menuBar.add(helpMenu);
//		
//		fileMenu.add(newAction);
//		fileMenu.add(openAction);
//		fileMenu.add(new Separator());
//		fileMenu.add(saveAction);
//		fileMenu.add(saveAsAction);
//		fileMenu.add(new Separator());
//		fileMenu.add(exitAction);
//		
//		editMenu.add(copyAction);
//		editMenu.add(cutAction);
//		editMenu.add(pasteAction);
//		
//		helpMenu.add(helpAction);
//		return menuBar;
//	}
//
//
//
//	@Override
//	protected ToolBarManager createToolBarManager(int style) {
//		ToolBarManager toolbarMgr = super.createToolBarManager(style);
//		
//		toolbarMgr.add(new GroupMarker("file"));
//		toolbarMgr.add(new GroupMarker("edit"));
//		toolbarMgr.add(new GroupMarker("help"));
//		
//		
//		toolbarMgr.appendToGroup("file", newAction);
//		toolbarMgr.appendToGroup("file", openAction);
//		toolbarMgr.appendToGroup("file", saveAction);
//		toolbarMgr.appendToGroup("file", saveAsAction);
//		
//		toolbarMgr.appendToGroup("edit", new Separator());
//		toolbarMgr.appendToGroup("edit", copyAction);
//		toolbarMgr.appendToGroup("edit", cutAction);
//		toolbarMgr.appendToGroup("edit", pasteAction);		
//		
//		toolbarMgr.appendToGroup("help", new Separator());
//		toolbarMgr.appendToGroup("help", helpAction);
//
//
//		return toolbarMgr;
//	}

	public static void main(String[] args) {
		Display display = new Display();		//the default display, Display.getCurrent()
		
		Snippet002ApplicationWindow appWin = new Snippet002ApplicationWindow();
		appWin.setBlockOnOpen(true);
		app.open();
		
		display.dispose();
	}

}

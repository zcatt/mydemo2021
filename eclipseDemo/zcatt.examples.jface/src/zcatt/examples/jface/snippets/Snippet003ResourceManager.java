package zcatt.examples.jface.snippets;

import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Snippet003ResourceManager extends ApplicationWindow
{
	ResourceManager resMgr;
	ImageRegistry imageRegistry;
	
	public Snippet003ResourceManager() {
		super(null);
	
		resMgr = new LocalResourceManager(JFaceResources.getResources());
		
		imageRegistry = new ImageRegistry(resMgr);	//指定resMgr负责imageDescriptor到image的转换.
		//imageRegistry = new ImageRegistry();		//等同于new ImageRegistry(JFaceResources.getResources(Display.getCurrent())).
		
		imageRegistry.put("new", ImageDescriptor.createFromFile(Snippet003ResourceManager.class, "file_new.png"));
		imageRegistry.put("open", ImageDescriptor.createFromFile(Snippet003ResourceManager.class, "file_open.png"));
		imageRegistry.put("save", ImageDescriptor.createFromFile(Snippet003ResourceManager.class, "file_save.png"));
		imageRegistry.put("copy", ImageDescriptor.createFromFile(Snippet003ResourceManager.class, "edit_copy.png"));
		imageRegistry.put("cut", ImageDescriptor.createFromFile(Snippet003ResourceManager.class, "edit_cut.png"));
		imageRegistry.put("paste", ImageDescriptor.createFromFile(Snippet003ResourceManager.class, "edit_paste.png"));					
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Snippet003ResourceManager");
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new FillLayout());
		Canvas canvas = new Canvas(comp, SWT.NONE);
		
		canvas.addPaintListener(this::paint);
		
		return comp;
	}
	
	void paint(PaintEvent event)
	{
		GC gc = event.gc;
		gc.drawImage(imageRegistry.get("new"), 100, 20);
		gc.drawImage(imageRegistry.get("open"), 150, 20);
		gc.drawImage(imageRegistry.get("save"), 200, 20);
		gc.drawImage(imageRegistry.get("copy"), 250, 20);
		gc.drawImage(imageRegistry.get("cut"), 300, 20);
		gc.drawImage(imageRegistry.get("paste"), 350, 20);
		
		gc.drawImage(resMgr.createImage(imageRegistry.getDescriptor("new")), 100, 120);
		gc.drawImage(resMgr.createImage(imageRegistry.getDescriptor("open")), 150, 120);
		gc.drawImage(resMgr.createImage(imageRegistry.getDescriptor("save")), 200, 120);
		
	}

	@Override
	protected Point getInitialSize() {
		return new Point(600, 500);
	}


	public static void main(String[] args) {
		Display display = new Display();
		
		ApplicationWindow appWin = new Snippet003ResourceManager();
		appWin.setBlockOnOpen(true);
		appWin.open();
		
		display.dispose();
	}

}

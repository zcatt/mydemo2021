package zcatt.examples.jface.snippets;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class Snippet004Dialog extends ApplicationWindow {

	public static void main(String[] args) {
		Display display = new Display();
		
		ApplicationWindow appWin = new Snippet004Dialog();
		appWin.setBlockOnOpen(true);
		appWin.open();
		
		display.dispose();
	}

	public Snippet004Dialog() {
		super(null);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Snippet004Dialog");
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		Button btn = new Button(comp, SWT.PUSH);
		btn.setText("pop dialog");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			Dialog dialog = new MyDialog(getShell());
			dialog.open();			

			MessageBox mb = new MessageBox(getShell(), SWT.OK|SWT.ICON_INFORMATION);
			mb.setText("buttonPressed");
			mb.setMessage("The buttonId pressed is "+ dialog.getReturnCode());
			mb.open();
			
		}));
		
		
		return comp;
	}

	@Override
	protected Point getInitialSize() {
		// TODO Auto-generated method stub
		return super.getInitialSize();
	}


	class MyDialog extends Dialog {

		protected MyDialog(Shell parentShell) {
			super(parentShell);
		}

		@Override
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);
			newShell.setText("MyDialog");
		}

		@Override
		protected void buttonPressed(int buttonId) {
			super.buttonPressed(buttonId);	//Dialog.buttonPressed()转发到okPressed()和cancelPressed()
	
			if(buttonId == IDialogConstants.RETRY_ID)
			{
				setReturnCode(IDialogConstants.RETRY_ID);
				close();
			}
			
		}

		@Override
		protected void createButtonsForButtonBar(Composite parent) {
			createButton(parent, IDialogConstants.RETRY_ID, IDialogConstants.RETRY_LABEL, false);
			super.createButtonsForButtonBar(parent);	//默认创建 ok, cancel
		}

		@Override
		protected Control createDialogArea(Composite parent) {
			Label label = new Label(parent, SWT.NONE);
			label.setText("This is my dialog");
			//return super.createContents(parent);
			return label;
		}
		
	}
}

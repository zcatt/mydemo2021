package zcatt.examples.jface.snippets;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import zcatt.examples.jface.snippets.Snippet004Dialog.MyDialog;

public class Snippet005IconAndMessageDialog extends ApplicationWindow {

	public static void main(String[] args) {
		Display display = new Display();
		
		ApplicationWindow appWin = new Snippet005IconAndMessageDialog();
		appWin.setBlockOnOpen(true);
		appWin.open();
		
		display.dispose();
	}
	
	public Snippet005IconAndMessageDialog() {
		super(null);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Snippet005IconAndMessageDialog");
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		Button btn;
		btn = new Button(comp, SWT.PUSH);
		btn.setText("ErrorDialog");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int code = 101;
			Status status = new Status(IStatus.ERROR, Snippet005IconAndMessageDialog.class, code, "error status", new Exception("my exception"));
			int ret = ErrorDialog.openError(getShell(), "ErrorDialog", "This is my message.", status);
			
			assert ret == code;
		}));

		btn = new Button(comp, SWT.PUSH);
		btn.setText("MessageDialog");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			boolean res = MessageDialog.openQuestion(getShell(), "MessageDialog", "Is it sunny?");
			MessageDialog.openInformation(getShell(), "Info", "You choose "+ (res ? "yes":"no"));			
		}));
		

		btn = new Button(comp, SWT.PUSH);
		btn.setText("ProgressMonitorDialog");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			IRunnableWithProgress runnable = new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
					IProgressMonitor blocking = monitor;

					blocking.beginTask("Vista Coolness", 100);
					for (int i = 0; i < 10; i++) {
						blocking.setBlocked(new Status(IStatus.WARNING, "Blocked", "This is blocked on Vista"));
						blocking.worked(5);
						Thread.currentThread().sleep(500);
						blocking.clearBlocked();
						blocking.worked(5);
						Thread.currentThread().sleep(500);
						if (monitor.isCanceled())				//响应ProgressMonitorDialog的cancel命令.
							return;
					}
					blocking.done();
					
				}				
			};
			
			ProgressMonitorDialog progressMonitorDialog = new ProgressMonitorDialog(getShell());
			try {
				progressMonitorDialog.run(true,  true, runnable);
			} catch (InvocationTargetException | InterruptedException e) {
				e.printStackTrace();
			}
			
			
		}));
		
		
		return comp;
	}



	class MyDialog extends Dialog {

		protected MyDialog(Shell parentShell) {
			super(parentShell);
		}

		@Override
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);
			newShell.setText("MyDialog");
		}

		@Override
		protected void buttonPressed(int buttonId) {
			super.buttonPressed(buttonId);	//Dialog.buttonPressed()转发到okPressed()和cancelPressed()
	
			if(buttonId == IDialogConstants.RETRY_ID)
			{
				setReturnCode(IDialogConstants.RETRY_ID);
				close();
			}
			
		}

		@Override
		protected void createButtonsForButtonBar(Composite parent) {
			createButton(parent, IDialogConstants.RETRY_ID, IDialogConstants.RETRY_LABEL, false);
			super.createButtonsForButtonBar(parent);	//默认创建 ok, cancel
		}

		@Override
		protected Control createDialogArea(Composite parent) {
			Label label = new Label(parent, SWT.NONE);
			label.setText("This is my dialog");
			//return super.createContents(parent);
			return label;
		}
		
	}

}

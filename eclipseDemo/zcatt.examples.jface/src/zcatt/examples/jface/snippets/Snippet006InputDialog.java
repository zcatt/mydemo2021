package zcatt.examples.jface.snippets;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Snippet006InputDialog extends ApplicationWindow {

	public static void main(String[] args) {
		Display display = new Display();
		
		ApplicationWindow appWin = new Snippet006InputDialog();
		appWin.setBlockOnOpen(true);
		appWin.open();
		
		display.dispose();
	}

	public Snippet006InputDialog() {
		super(null);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Snippet006InputDialog");
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		Button btn;
		btn = new Button(comp, SWT.PUSH);
		btn.setText("InputDialog");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			InputDialog dialog = new InputDialog(getShell()
					, "What's your name?"
					, "Enter your name(only alphabetic,dot,blank)",
					"noname", newText -> { 
						if(!newText.isEmpty() && !newText.matches("[a-zA-Z. ]*"))
						{
							return "Not a valid name";
						}
						return null;
					});
			if (dialog.open() == Window.OK)
			{
				MessageDialog.openConfirm(getShell(), "Confirm", "You enter  "+ dialog.getValue());
			}			
		}));

		
		
		return comp;
	}

}

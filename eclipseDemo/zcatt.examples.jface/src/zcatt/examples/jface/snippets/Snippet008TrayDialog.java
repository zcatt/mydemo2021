package zcatt.examples.jface.snippets;

import org.eclipse.jface.dialogs.DialogTray;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import zcatt.examples.jface.snippets.Snippet007PopupDialog.InfoPopupDialog;

public class Snippet008TrayDialog extends ApplicationWindow {

	public static void main(String[] args) {
		Display display = new Display();
		
		ApplicationWindow appWin = new Snippet008TrayDialog();
		appWin.setBlockOnOpen(true);
		appWin.create();
		appWin.getShell().setSize(300,200);
		appWin.open();
		
		display.dispose();
	}	
	
	
	InfoTrayDialog infoTrayDialog;
	
	public Snippet008TrayDialog() {
		super(null);
	}
	
/*	
	@Override
	public boolean close() {
		return super.close();
	}
*/
	
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Snippet008TrayDialog");
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		Button btn;
		btn = new Button(comp, SWT.PUSH);
		btn.setText("InfoTrayDialog");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			infoTrayDialog = new InfoTrayDialog(getShell());
			infoTrayDialog.create();
			infoTrayDialog.openTray(new MyDialogTray());	//openTray()使用到shell, 故需放置在create()之后
			infoTrayDialog.open();		//modal dialog
		}));

		return comp;
	}

	class InfoTrayDialog extends TrayDialog {

		protected InfoTrayDialog(Shell shell) {
			super(shell);
		}

		@Override
		protected Control createDialogArea(Composite parent) {
			// TODO Auto-generated method stub
			Composite comp = (Composite) super.createDialogArea(parent);
			
			Button btn = new Button(comp, SWT.PUSH);
			btn.setText("openTray or closeTray");
			btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
				if(getTray() == null)
				{
					try {
						openTray(new MyDialogTray());						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					closeTray();
				}
			}));
			
			return comp;
		}

		@Override
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);
			newShell.setText("Snippet008TrayDialog");
		}		
	}
	
	class MyDialogTray extends DialogTray{

		@Override
		protected Control createContents(Composite parent) {
			Text text = new Text(parent, SWT.MULTI | SWT.READ_ONLY | SWT.WRAP);
			text.setText("Hi!\nThis is MyDialogTray.\n");
			text.setBackground(new Color(200,200,0));
			
			return text;
		}
		
	}

}

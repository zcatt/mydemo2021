package zcatt.examples.jface.snippets;

import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.IPreferencePage;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.preference.PreferenceNode;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.custom.BusyIndicator;

import zcatt.examples.jface.snippets.Snippet008TrayDialog.InfoTrayDialog;
import zcatt.examples.jface.snippets.Snippet008TrayDialog.MyDialogTray;

public class Snippet009PreferenceDialog extends ApplicationWindow {

	public static void main(String[] args) {
		Display display = new Display();
		
		ApplicationWindow appWin = new Snippet009PreferenceDialog();
		appWin.setBlockOnOpen(true);
		appWin.create();
		appWin.getShell().setSize(300,200);
		appWin.open();
		
		display.dispose();
	}	

	
	public Snippet009PreferenceDialog() {
		super(null);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Snippet009PreferenceDialog");
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		Button btn;
		btn = new Button(comp, SWT.PUSH);
		btn.setText("show preferenceDialog");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			PreferenceManager manager = new PreferenceManager();


			IPreferenceNode pn1 = new MyPreferenceNode("pref1", "my preference 1");

/*			
			//一种构造preferenceNode的方式
			String className =MyPreferencePage.class.getName(); 
			IPreferenceNode pt = new PreferenceNode("prefNode"
					, "pref node"
					, ImageDescriptor.createFromFile(getClass(), "file_open.png")
					, className);	//此形式要求className必须带有默认构造函数.
*/			

			
			//s另一种构造preferenceNode的方式
			MyPreferencePage page = new MyPreferencePage();
			page.setImageDescriptor(ImageDescriptor.createFromFile(getClass(), "file_open.png"));
			page.setDesc("pref node");
			page.setTitle("PrefNode");			
			PreferenceNode pt = new PreferenceNode("prefNode", page);


			IPreferenceNode pn2 = new MyPreferenceNode("pref2", "my preference 2");
			pt.add(pn2);
			
			IPreferenceNode pn3 = new MyPreferenceNode("pref3", "my preference 3");
						
			manager.addToRoot(pn1);
			manager.addToRoot(pt);
			manager.addToRoot(pn3);

			PreferenceDialog dialog = new PreferenceDialog(getShell(), manager);
			//dialog.create();
			//dialog.setMessage(targetNode.getLabelText());
			dialog.open();
		}));

		return comp;
	}

	//leaf node
	class MyPreferenceNode implements IPreferenceNode{
		MyPreferencePage page;
		String desc;
		String title;

		
		public MyPreferenceNode()
		{
			desc = "no desc";
			title = "no title";
		}
		
		public MyPreferenceNode(String title, String desc)
		{
			this.desc = desc;
			this.title = title;
		}
		
		
		@Override
		public void add(IPreferenceNode node) {
		}

		@Override
		public void createPage() {
			page = new MyPreferencePage();
			page.setDesc(desc);
		}

		@Override
		public void disposeResources() {
			if(page != null)
			{
				page.dispose();
				page = null;
			}
			
		}

		@Override
		public IPreferenceNode findSubNode(String id) {
			return null;
		}

		@Override
		public String getId() {			
			return "zcatt.examples.jface.snippets.MyPreferenceNode";
		}

		@Override
		public Image getLabelImage() {
			return null;
		}

		@Override
		public String getLabelText() {
			return title;
		}

		@Override
		public IPreferencePage getPage() {
			return page;
		}

		@Override
		public IPreferenceNode[] getSubNodes() {
			//return null;						//不能使用return null, 因PreferenceContentProvider.hasChildren()会取.length
			return new IPreferenceNode[0];			
		}

		@Override
		public IPreferenceNode remove(String id) {
			return null;		//leaf node
		}

		@Override
		public boolean remove(IPreferenceNode node) {
			return false;		//leaf node
		}
		
	}
	
	public static class MyPreferencePage extends PreferencePage {
		String desc;
		
		public MyPreferencePage()
		{
			desc="(none)";
		}
		
		public void setDesc(String desc)
		{
			this.desc = desc;			
		}
		
		@Override
		protected Control createContents(Composite parent) {
			Composite container = new Composite(parent, SWT.NONE);
			container.setLayoutData(new GridData(GridData.FILL_BOTH));
			container.setLayout(new GridLayout(1, false));
			
			Label label = new Label(container, SWT.NONE);
			label.setText(desc);
			
			return container;
		}
		
	}
}

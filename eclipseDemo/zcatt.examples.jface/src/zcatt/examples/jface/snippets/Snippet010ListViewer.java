package zcatt.examples.jface.snippets;

import java.io.InputStream;

import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.preference.PreferenceNode;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;


import zcatt.examples.jface.snippets.Snippet009PreferenceDialog.MyPreferenceNode;
import zcatt.examples.jface.snippets.Snippet009PreferenceDialog.MyPreferencePage;


public class Snippet010ListViewer extends ApplicationWindow {

	public static void main(String[] args) {
		Display display = new Display();
		
		ApplicationWindow appWin = new Snippet010ListViewer();
		appWin.setBlockOnOpen(true);
		appWin.create();
		appWin.getShell().setSize(600,400);
		appWin.open();
		
		display.dispose();
	}	

	ListViewer listViewer;
	Text text;
	
	Image loadImage(Display display, String fileName)
	{
		InputStream is = Snippet010ListViewer.class.getResourceAsStream(fileName);
		if(is == null)
			return null;
		Image image = null;
		try {
			image = new Image(display, is);
		}
		catch(Exception e)
		{}
		finally
		{
			try {
				is.close();
			}
			catch(Exception e)
			{}
		}
		
		return image;			
	}
	
	public Snippet010ListViewer() {
		super(null);	
	}


	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Snippet010ListViewer");
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());

		Person[] persons= {
					new Person("Tom", 10), 
					new Person("Mary", 12), 
					new Person("David", 9),
				};
		
		//此处也可以先创建list后, 再在list obj上new ListViewer.
		listViewer = new ListViewer(comp, SWT.BORDER|SWT.SINGLE);
		listViewer.getControl().setBackground(new Color(250,250,0));
		listViewer.getControl().setLayoutData(new GridData(300, SWT.DEFAULT));
		
		//setLabelProvider()和setContentProvider()应当先于setInput()调用.
		listViewer.setLabelProvider(LabelProvider.createTextProvider(obj->{
			Person person = (Person) obj;
			return person.name +", "+person.age;					
		}));		

		//method 1. 通过input/contentProvider设置
//		listViewer.setContentProvider(ArrayContentProvider.getInstance());	//匹配persons是array
//		listViewer.setInput(persons);	//viewer.setInput()应当在setContentProvider()后调用
		
		//method2. 通过add()直接添加
		listViewer.add(persons);

		
		//此处直接设置swt listener. jface更正式的用法是override handleSelect().
//		List list = (List)listViewer.getControl();
//		list.addSelectionListener(new SelectionListener() {
//
//			@Override
//			public void widgetSelected(SelectionEvent event) {
//				List t = (List) event.widget;
//				String[] selections = t.getSelection();
//				text.append("select "+ selections[0]+"\n");				
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent event) {
//				List t = (List) event.widget;
//				String[] selections = t.getSelection();
//				text.append("default select "+ selections[0]+"\n");
//			}
//			
//		});

		listViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object obj;
				
				//method 1.
				//obj = listViewer.getStructuredSelection().getFirstElement();

				//method 2.
				obj = listViewer.getStructuredSelection().getFirstElement();
				
				Person p = (Person) obj;
				text.append("select "+ p.name+", ");							
				text.append("text "+ listViewer.getList().getSelection()[0]+"\n");							
			}
			
		});

		text = new Text(comp, SWT.MULTI|SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		text.setText("ListViewer sample.\n");
		return comp;
	}
	
	class Person{
		public String name;
		public int age;
		
		public Person(String name, int age)
		{
			this.name = name;
			this.age = age;
		}
	}

}

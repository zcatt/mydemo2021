package zcatt.examples.jface.snippets;

/*
  Derived from org.eclipse.ui.examples.fieldassist.
  演示了ControlDecoration的功能. 
  
 */
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.StatusDialog;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import zcatt.examples.jface.snippets.Snippet004Dialog.MyDialog;

public class Snippet015FieldAssist {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		
		shell.setLayout(new FillLayout());
		shell.setText("Snippet014CompositeRuler");
		//shell.setSize(400, 300);
		
		create(shell);
		shell.pack();
	
		shell.open();
		while(!shell.isDisposed())
		{
			if(!display.readAndDispatch())
				display.sleep();
		}
		
		shell.dispose();
	}
	
	
	static String DEC_CONTENTASSIST = "contentAssistDecoration";
	
	public static void create(Shell shell)
	{
		shell.setText("Snippet015FieldAssist");
		Composite comp = new Composite(shell, SWT.NONE);
		comp.setLayout(new GridLayout());
		Button btn = new Button(comp, SWT.PUSH);
		btn.setText("Pop dialog");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> {
			Dialog dialog = new MyDialog(shell);
			dialog.open();		
		}));		
	}
	
	public static class MyDialog extends StatusDialog{
		
		int marginWidth = 0;
		Text text;
		

		protected MyDialog(Shell parentShell) {
			super(parentShell);
			setTitle("MyDialog - Demo fieldassist");			
		}

		GridData getFieldGridData() {
			int margin = FieldDecorationRegistry.getDefault().getMaximumDecorationWidth();
			GridData data = new GridData();
			data.horizontalAlignment = SWT.FILL;
			data.widthHint = IDialogConstants.ENTRY_FIELD_WIDTH + margin;
			data.horizontalIndent = margin;
			data.grabExcessHorizontalSpace = true;
			return data;

		}

		FieldDecoration getContentAssistDecoration() {
			FieldDecorationRegistry registry = FieldDecorationRegistry.getDefault();
			FieldDecoration dec = registry.getFieldDecoration(DEC_CONTENTASSIST);
			if (dec == null) {
				FieldDecoration standardDecoration = registry.getFieldDecoration(FieldDecorationRegistry.DEC_CONTENT_PROPOSAL);
				registry.registerFieldDecoration(
						DEC_CONTENTASSIST
						, "content assist"
						, standardDecoration.getImage());
				dec = registry.getFieldDecoration(DEC_CONTENTASSIST);
			}
			return dec;
		}
		
		void handleModify(ControlDecoration cd, boolean show) {
			FieldDecoration dec = getContentAssistDecoration();
			if (show) {
				String s = text.getText();
				System.out.println("text=" + s);
				if(!s.equals("error"))
				{
					updateStatus(Status.OK_STATUS);
				}
				else
				{
					updateStatus(new Status(IStatus.ERROR,
							"myApp", 0, "Error content!", null));
					
				}
					

				cd.setImage(dec.getImage());
				cd.setDescriptionText(dec.getDescription());
				cd.setShowOnlyOnFocus(true);
				cd.show();
			} else {
				cd.hide();
			}
			
		}
		
		@Override
		protected Control createDialogArea(Composite parent) {
			Composite outer = (Composite) super.createDialogArea(parent);
			initializeDialogUnits(outer);
			
			Group group = new Group(outer, SWT.NONE);
			group.setText("Fields");
			group.setLayoutData(new GridData(GridData.FILL_BOTH));
			
			GridLayout layout = new GridLayout();
			layout.numColumns = 2;
			layout.marginHeight = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
			layout.marginWidth = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
			layout.verticalSpacing = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
			layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
			group.setLayout(layout);
			
			
			Label label = new Label(group, SWT.LEFT);
			label.setText("Name");
			
			text = new Text(group, SWT.BORDER);
			ControlDecoration dec = new ControlDecoration(text, SWT.LEFT | SWT.TOP);
			dec.setMarginWidth(marginWidth);
			
			text.addModifyListener(event -> handleModify(dec, true));

			//zf, 点击decoratin, 弹出messageDialog
			dec.addSelectionListener(SelectionListener.widgetSelectedAdapter(e-> {
				MessageDialog.openInformation(
						text.getShell(),
						"Decoration",
						"You clicked the decoration!");

			}));
			
			text.setText("noname");
			text.setLayoutData(getFieldGridData());
			
			applyDialogFont(outer);



			return outer;
		}

		@Override
		public boolean close() {
			// TODO Auto-generated method stub
			return super.close();
		}

		@Override
		protected boolean isResizable() {
			// TODO Auto-generated method stub
			return super.isResizable();
		}

		
	}

}

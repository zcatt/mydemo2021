package zcatt.examples.jface.snippets;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

/*
 * 简单的tableViewer
 */
public class Snippet016SimpleTableViewer {

	TableViewer viewer;
	
	public static class MyModel{
		int id;
		
		public MyModel(int id) {
			this.id = id;
		}
		
		@Override
		public String toString() {
			return "item"+id;
		}
		
		
	}
	public static class MyContentProvider implements IStructuredContentProvider{

		@Override
		public Object[] getElements(Object inputElement) {
			return null;
		}
		
	}
	
	
	public Snippet016SimpleTableViewer(Shell shell) {
		viewer = new TableViewer(shell);
		viewer.setLabelProvider(new LabelProvider());	//zf, 使用obj.toString()做为text
		viewer.setContentProvider(new MyContentProvider());
		
		//SWT方式创建column
		Table tb = viewer.getTable();
		TableColumn col = new TableColumn(tb, SWT.NONE);
		col.setWidth(100);
		col.setText("value");
		tb.setHeaderVisible(true);
		tb.setLinesVisible(true);
		
		//create model
		MyModel[] model = new MyModel[10];
		for(int i = 0; i< 10; i++)
		{
			model[i] = new MyModel(i+1);
		}
		
		viewer.setInput(model);	
	}

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		new Snippet016SimpleTableViewer(shell);
		shell.open();
		
		while(!shell.isDisposed())
		{
			if(!display.readAndDispatch())
				display.sleep();
		}
		
		display.dispose();
	}

}

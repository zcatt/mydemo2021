package zcatt.examples.jface.snippets;

import java.io.InputStream;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class Util {

	private Util() {
	}

	public static Image loadImage(String fileName)
	{
		InputStream is = Util.class.getResourceAsStream(fileName);
		if(is == null)
			return null;
		Image image = null;
		try {
			image = new Image(Display.getCurrent(), is);
		}
		catch(Exception e)
		{}
		finally
		{
			try {
				is.close();
			}
			catch(Exception e)
			{}
		}
		
		return image;		
	}

}

/*
 	osgi bundle的sample, 使用miniOsgi环境运行即可.
 	仅在start和stop是输出提示. 
 	需要使用osgi的命令行启动.
 	osgi 命令简单列表 
 	help, 列出命令清单
 	apps,	list all installed app IDs
 	startApp <appId>,	start app
 	stopApp <appId>,	stop app
 	ns [-v] [name],		列出namespace中的extension points
 	close,		shutdown and exit
 	shutdown,	shutdown the OSGi framework
 	status,		display installed bundles and registered services
 	ss,			display installed bundles(short status)
 	services,	display registered service details.
 	bundle,		display details for the specified bundle(s)
 	lb, 		list all installed bundles
 	sta/start,		start bundles
 	stop,		stop bundles
 	install,	
 	uninstall,
 	bundles,	display details for all installed bundles
 	update,		update bundle
 	exit,		exit immediately
 */

package zcatt.examples.osgi.hello;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Hello World!!");
	}
	
	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Goodbye World!!");
	}

}

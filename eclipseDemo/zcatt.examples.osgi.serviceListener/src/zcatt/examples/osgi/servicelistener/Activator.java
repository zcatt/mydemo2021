/*
	注意点,
	1.因使用到FileLocator, 而需要Import package org.eclipse.core.runtime
	2.在osgi console中可以使用stop命令, 而观察bundleListener的state变化.
	3.bundle在stop时, 会自动unregister 其注册的services, 删除自己的各种listener. 
		本例中在stop()中的removeListener和unregisterService仅是便于在log中观察.
	

 */
package zcatt.examples.osgi.servicelistener;

import static java.lang.System.out;

import java.util.Dictionary;
import java.util.Enumeration;

import org.osgi.framework.*;
//import org.eclipse.core.runtime.FileLocator;		//need Import-Package: org.eclipse.core.runtime;version="3.7.0"

public class Activator implements BundleActivator, BundleListener, ServiceListener {

	ServiceRegistration<?> helloServ;
	
	@Override
	public void start(BundleContext context) throws Exception {
		out.println("\nserviceListener start.....");
		
		context.addBundleListener(this);
		context.addServiceListener(this);		//zf, service listener
		
		//注册service
		HelloService servObj = new HelloServiceImpl();
		helloServ = context.registerService(HelloService.class.getName(), servObj, null); //zf, paired with servRef.unregister() or context.ungetService(servRef)
		
	
		//print bundle info
		Bundle bundle = context.getBundle();
		
		out.println("bundle state(STARTING is 8) = " + bundle.getState());

		Dictionary<String, String> dict = bundle.getHeaders();
		
		out.println();
		out.println("\nmanifest.......");
		for(Enumeration<String> key = dict.keys(); key.hasMoreElements();)
		{
			String attrName = key.nextElement();
			String attrValue = dict.get(attrName);
			out.println(attrName + " : " + attrValue);
		}
		out.println("manifest.......over");
		
		//print entries
		out.println();
		out.println("\nentries.......");
		for(Enumeration<java.net.URL> e = bundle.findEntries("/", "*", true); e.hasMoreElements();)
		{
			java.net.URL url = e.nextElement();
			out.println("url = " + url);
			//out.println("fileUrl = " + FileLocator.toFileURL(url));		//参见import中的注释, 需要Import-Package
		}
		out.println("entries.......over");
		
		out.println("serviceListener start.....over");
	}
	
	// issue 'stop' command in osgi console to stop this bundle
	@Override
	public void stop(BundleContext context) throws Exception {
		out.println("serviceListener stop......");
		
		//注销service. 
		//stop会自动unregister service, 这里仅是展示手动unregister service
		helloServ.unregister();		//trigger the ServiceEvent.UNREGISTERING									
		
		//stop会自动remove serviceListener, 这里仅是展示手动
		context.removeServiceListener(this);

		out.println("serviceListener stop......over");
	}

	@Override
	public void serviceChanged(ServiceEvent event) {
		String[] objClass = (String[]) event.getServiceReference().getProperty("objectClass");
		
		switch(event.getType())
		{
		case ServiceEvent.REGISTERED:
			out.println("serviceChanged.....   "+objClass[0]+ " registered.");
			break;
		case ServiceEvent.UNREGISTERING:
			out.println("serviceChanged.....   "+objClass[0]+ " unregistering.");
			break;
		case ServiceEvent.MODIFIED:
			out.println("serviceChanged.....   "+objClass[0]+ " modified.");
			break;
		default:
			out.println("serviceChanged.....   "+objClass[0]+ " default = " + event.getType());
			break;
		}
	}

	@Override
	public void bundleChanged(BundleEvent event) {
		Bundle bundle = event.getBundle();
		switch(event.getType())
		{
		case BundleEvent.INSTALLED:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " INSTALLED.");			
			break;
		case BundleEvent.LAZY_ACTIVATION:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " LAZY_ACTIVATION.");			
			break;
		case BundleEvent.RESOLVED:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " RESOLVED.");			
			break;
		case BundleEvent.STARTED:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " STARTED.");			
			break;
		case BundleEvent.STARTING:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " STARTING.");			
			break;
		case BundleEvent.STOPPED:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " STOPPED.");			
			break;
		case BundleEvent.STOPPING:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " STOPPING.");			
			break;
		case BundleEvent.UNINSTALLED:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " UNINSTALLED.");			
			break;
		case BundleEvent.UNRESOLVED:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " UNRESOLVED.");			
			break;
		case BundleEvent.UPDATED:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " UPDATED.");			
			break;
		default:
			out.println("bundleChanged.....   "+bundle.getSymbolicName()+ " "+event.getType());			
			break;
		
		}
		
		
	}
}

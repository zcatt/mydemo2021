package zcatt.examples.osgi.servicelistener;

//通常service和serviceImpl分开实现, 这样达到松耦合目的.
public interface HelloService {
	public void sayHello(String target);
}

package zcatt.examples.osgi.servicelistener;

public class HelloServiceImpl implements HelloService {

	@Override
	public void sayHello(String target) {
		System.out.println("hello, " + target + "!");

	}

}

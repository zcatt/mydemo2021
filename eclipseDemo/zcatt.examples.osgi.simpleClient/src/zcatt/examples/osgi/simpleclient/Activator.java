package zcatt.examples.osgi.simpleclient;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;

import zcatt.examples.osgi.simpleservice.ISimpleService;

public class Activator implements BundleActivator, ServiceListener {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Simple client start........");
		
		//context.addServiceListener(this, "(objectClass=" + ISimpleService.class.getName() + ")");
		context.addServiceListener(this);
		
		ServiceReference<ISimpleService> servRef = context.getServiceReference(ISimpleService.class);
		ISimpleService simpleServ = context.getService(servRef);
		try
		{
			System.out.println("use simpleService");
			simpleServ.sayHello("simpleClient");			
		}
		finally
		{
			context.ungetService(servRef);
		}
	}
	
	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Simple client stop........");
	}

	@Override
	public void serviceChanged(ServiceEvent event) {
        String[] objectClass = (String[]) event.getServiceReference().getProperty("objectClass");

            // If a dictionary service was registered, see if we
            // need one. If so, get a reference to it.
            if (event.getType() == ServiceEvent.REGISTERED)
            {
            	System.out.println("registered:: "+objectClass[0]);
            }		
		
	}

}

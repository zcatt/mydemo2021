package zcatt.examples.osgi.simpleservice;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Simple service start........");
		
		ISimpleService serv = new SimpleServiceImpl();
		context.registerService(ISimpleService.class, serv, null);
		
		System.out.println("Register "+ ISimpleService.class + " service.");
	}
	
	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Simple service stop.........");
	}

}

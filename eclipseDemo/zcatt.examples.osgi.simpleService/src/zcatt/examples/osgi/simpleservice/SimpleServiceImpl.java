package zcatt.examples.osgi.simpleservice;

public class SimpleServiceImpl implements ISimpleService {

	@Override
	public void sayHello(String target) {
		System.out.println("from simpleService:  Hello, "+target+"!");
	}

}

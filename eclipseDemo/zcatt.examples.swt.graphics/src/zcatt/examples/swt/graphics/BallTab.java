package zcatt.examples.swt.graphics;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Path;
import org.eclipse.swt.graphics.Transform;

public class BallTab extends AnimatedGraphicsTab {

	static int DIAMETER = 30;
	static int INC_X = 3;
	static int INC_Y = 3;
	static int INC_ALPHA = 3;
	int x, y;
	int incX, incY, incAlpha;
	int alpha;
	Color color;
	
	@Override
	public void dispose() {
		super.dispose();
		x = 0;
		y = 0;
		incX = INC_X;
		incY = INC_Y;		
		alpha = 255;
		incAlpha = -INC_ALPHA;
		color = null;
	}

	public BallTab(GraphicsExample example) {
		super(example);
		x = 0;
		y = 0;
		incX = INC_X;
		incY = INC_Y;		
		alpha = 255;
		incAlpha = INC_ALPHA;
		color = null;
	}

	@Override
	public void next(int width, int height) {
		x += incX;
		if(x+DIAMETER >= width || x < 0)
		{
			incX = -incX;
			x += incX *2;
		}

		y += incY;
		if(y+DIAMETER >= height || y < 0)
		{
			incY = -incY;
			y += incY *2;
		}

		alpha += incAlpha;
		if(alpha > 255  || alpha < 0)
		{
			incAlpha = -incAlpha;
			alpha += incAlpha *2;
		}		
		
		//System.out.printf("%d,%d;  %d,%d\n", x, y, width, height);		
	}

	@Override
	public String getText() {
		return "Ball";
	}

	public String getDescription()
	{
		return "Ball example";
	}
	
	public String getCategory()
	{
		return "Alpha";
	}
	
	public boolean getDoubleBuffered()
	{
		return false;
	}
	
	public void paint(GC gc, int width, int height)
	{	
		Device device = gc.getDevice();
		if(color == null)
			color = device.getSystemColor(SWT.COLOR_DARK_RED);
		
		//gc.drawRectangle(0,0, width-1, height-1);

		Transform transform = new Transform(device);
		transform.translate(x, y);
		gc.setTransform(transform);
		transform.dispose();
		
		Path path = new Path(device);
		path.addArc(0, 0, DIAMETER, DIAMETER, 0, 360);
		gc.setAlpha(alpha);
		gc.setBackground(color);
		gc.fillPath(path);
		gc.drawPath(path);
		path.dispose();
	}
	
}

package zcatt.examples.swt.graphics;

public interface ColorListener {
	public void setColor(GraphicsBackground background);
}

package zcatt.examples.swt.graphics;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Composite;

public abstract class GraphicsTab {
	GraphicsExample example;
	
	public GraphicsTab(GraphicsExample example)
	{		
		this.example = example;		
	}
	
	public void dispose()
	{}
	
	public abstract String getText();
	
	public String getDescription()
	{
		return "";
	}
	
	public String getCategory()
	{
		return "Misc";
	}
	
	public boolean getDoubleBuffered()
	{
		return false;
	}
	
	public void createControlPanel(Composite parent) {
	}
	
	public void paint(GC gc, int width, int height)
	{
		
	}

	
}

package zcatt.examples.swt.graphics;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

public class ImageTransformImage extends GraphicsTab {
	private Spinner transXSpinner1, transYSpinner1, rotateSpinner1, scaleXSpinner1, scaleYSpinner1; 
	private Spinner transXSpinner2, transYSpinner2, rotateSpinner2, scaleXSpinner2, scaleYSpinner2; 

	public ImageTransformImage(GraphicsExample example) {
		super(example);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public String getText() {
		return "Image";
	}


	@Override
	public void dispose() {
		super.dispose();
	}


	@Override
	public String getDescription() {
		return "transform image";
	}


	@Override
	public String getCategory() {
		return "Tranform";
	}


	Spinner createSpinner(Composite parent, int minValue, int maxValue, int incValue, int defValue)
	{
		Spinner spinner;
		spinner = new Spinner(parent, SWT.BORDER | SWT.WRAP);
		GC gc = new GC(spinner);
		int width = (int)(gc.getFontMetrics().getAverageCharacterWidth() * 5);
		gc.dispose();
		spinner.setLayoutData(new GridData(width, SWT.DEFAULT));
		spinner.setSelection(defValue);
		spinner.setMinimum(minValue);
		spinner.setMaximum(maxValue);
		spinner.setIncrement(incValue);
		spinner.addListener(SWT.Selection, event->example.canvas.redraw());
		
		return spinner;
	}
	
	@Override
	public void createControlPanel(Composite parent) {
		GridLayout gridLayout2 = new GridLayout(2, false);
		GridLayout gridLayout3 = new GridLayout(3, false);
		RowLayout rowLayout = new RowLayout(SWT.VERTICAL);
		Group group;
		Composite comp;
		Composite comp2;
		
		group = new Group(parent, SWT.NONE);
		group.setText("transform1");
		group.setLayout(gridLayout3);
		
		
		comp = new Composite(group, SWT.NONE);
		comp.setLayout(gridLayout2);
		new Label(comp, SWT.NONE).setText("Rotate");
		rotateSpinner1 = createSpinner(comp, -720, 720, 30, 0);

		comp = new Composite(group, SWT.NONE);
		comp.setLayout(rowLayout);
		
		comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(gridLayout2);
		new Label(comp2, SWT.NONE).setText("translateX");
		transXSpinner1 = createSpinner(comp2, -100, 300, 10, 0);

		comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(gridLayout2);
		new Label(comp2, SWT.NONE).setText("translateY");
		transYSpinner1 = createSpinner(comp2, -100, 300, 10, 0);		

		comp = new Composite(group, SWT.NONE);
		comp.setLayout(rowLayout);
		
		comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(gridLayout2);
		new Label(comp2, SWT.NONE).setText("scaleX");
		scaleXSpinner1 = createSpinner(comp2, 1, 400, 100, 100);
		scaleXSpinner1.setDigits(2);

		comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(gridLayout2);
		new Label(comp2, SWT.NONE).setText("scaleY");
		scaleYSpinner1 = createSpinner(comp2, 1, 400, 100, 100);
		scaleYSpinner1.setDigits(2);

		
		group = new Group(parent, SWT.NONE);
		group.setText("transform2");
		group.setLayout(gridLayout3);
		
		
		comp = new Composite(group, SWT.NONE);
		comp.setLayout(gridLayout2);
		new Label(comp, SWT.NONE).setText("Rotate");
		rotateSpinner2 = createSpinner(comp, -720, 720, 30, 0);

		comp = new Composite(group, SWT.NONE);
		comp.setLayout(rowLayout);
		
		comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(gridLayout2);
		new Label(comp2, SWT.NONE).setText("translateX");
		transXSpinner2 = createSpinner(comp2, -100, 300, 10, 0);

		comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(gridLayout2);
		new Label(comp2, SWT.NONE).setText("translateY");
		transYSpinner2 = createSpinner(comp2, -100, 300, 10, 0);

		comp = new Composite(group, SWT.NONE);
		comp.setLayout(rowLayout);
		
		comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(gridLayout2);
		new Label(comp2, SWT.NONE).setText("scaleX");
		scaleXSpinner2 = createSpinner(comp2, 1, 400, 10, 100);
		scaleXSpinner2.setDigits(2);

		comp2 = new Composite(comp, SWT.NONE);
		comp2.setLayout(gridLayout2);
		new Label(comp2, SWT.NONE).setText("scaleY");
		scaleYSpinner2 = createSpinner(comp2, 1, 400, 10, 100);	
		scaleYSpinner2.setDigits(2);

	}


	@Override
	public void paint(GC gc, int width, int height) {
		Device device = gc.getDevice();
		Image image = GraphicsExample.loadImage(device, "ace_club.jpg");
		
		Transform transform1 = new Transform(device);
		
		transform1.scale(scaleXSpinner1.getSelection()/100f, scaleYSpinner1.getSelection()/100f);
		transform1.translate(transXSpinner1.getSelection(), transYSpinner1.getSelection());

		//transform1.translate(rect.width/2, rect.height/2);
		transform1.rotate(rotateSpinner1.getSelection());
		//transform1.translate(-rect.width/2, -rect.height/2);
		
		Transform transform2 = new Transform(device);
		
		transform2.scale(scaleXSpinner2.getSelection()/100f, scaleYSpinner2.getSelection()/100f);
		transform2.translate(transXSpinner2.getSelection(), transYSpinner2.getSelection());

		//transform2.translate(rect.width/2, rect.height/2);
		transform2.rotate(rotateSpinner2.getSelection());
		//transform2.translate(-rect.width/2, -rect.height/2);
		
		transform1.multiply(transform2);
		gc.setTransform(transform1);
		gc.drawImage(image, 0, 0);
		
		transform1.dispose();
		transform2.dispose();
		image.dispose();		
	}

}

package zcatt.examples.swt.graphics;

import org.eclipse.swt.graphics.GC;

public class LineTab extends GraphicsTab {

	public LineTab(GraphicsExample example)
	{
		super(example);
	}

	public void dispose()
	{}
	
	@Override
	public String getText() {
		return "Line";
	}
	
	public String getDescription()
	{
		return "Draw line";
	}
	
	public String getCategory()
	{
		return "Lines";
	}
	
	public boolean getDoubleBuffered()
	{
		return false;
	}
	
	public void paint(GC gc, int width, int height)
	{
		gc.drawLine(0, 0, width, height);
		gc.drawLine(width, 0, 0, height);		
	}
	

}

package zcatt.examples.swt.graphics;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;

public class Shape {
	
	public Shape(Shell shell)
	{
		shell.setLayout(new FillLayout());
		Canvas canvas = new Canvas(shell, SWT.BORDER);
		canvas.addPaintListener(new PaintListener() {

			@Override
			public void paintControl(PaintEvent e) {
				GC gc = e.gc;
				
				LineAttributes lineAttr = gc.getLineAttributes();
				
				gc.drawLine(5, 50, 25, 100);
				
				gc.setLineWidth(4);
				gc.drawLine(15,50,35,100);				
				gc.setLineAttributes(lineAttr);		//zf, 复原lineAttrs
				
				gc.setLineStyle(SWT.LINE_DASHDOTDOT);
				gc.drawLine(25,50,45,100);				
				gc.setLineAttributes(lineAttr);

				
				gc.setBackground(new Color(255,0,0));
				gc.fillArc(100, 50, 50, 50, 0, 270);
				//gc.setBackground(oldBackground);
				gc.drawArc(100, 50, 50, 50, 0, 270);
				
				int[] polyLine= {
						200,50,
						250,50,
						250,100,
				};
				gc.drawPolyline(polyLine);
				for(int i = 0; i< polyLine.length; i+=2)
				{
					polyLine[i] += 100;					
				}
				
				//gc.setBackground(new Color(255,0,0));
				gc.fillPolygon(polyLine);
				//gc.setBackground(oldBackground);
				gc.drawPolygon(polyLine);
				
				//gc.setBackground(new Color(255,0,0));
				gc.fillRectangle(400, 50, 100, 50);
				//gc.setBackground(oldBackground);
				gc.drawRectangle(400, 50, 100, 50);
				
				//gc.setBackground(new Color(255,0,0));
				gc.fillRoundRectangle(550, 50, 100, 50, 20, 20);
				//gc.setBackground(oldBackground);
				gc.drawRoundRectangle(550, 50, 100, 50, 20, 20);
				
				gc.drawFocus(10, 150, 50, 100); 
				
				//font是重资源, 一般不应当如此用一次即销毁.此处仅是例子.
				FontData fd = shell.getFont().getFontData()[0];
				Font font = new Font(Display.getCurrent(), fd.getName(), 18, SWT.BOLD | SWT.ITALIC);
				gc.setFont(font);				
				
				gc.drawString("Hello,\tworld.\nHi!", 200, 150);
				gc.drawText("Hello,\tworld.\nHi!", 200, 180);
				gc.drawLine(200, 150, 300, 150);
				gc.drawLine(200, 150, 200, 200);
				assert font != shell.getFont();
				font.dispose();
				
				Path path = new Path(Display.getCurrent());
				path.moveTo((float) 400.4, 150);
				path.lineTo((float) 500.5, 300);				
				path.cubicTo(540, 225, 570, 175, 600,300);
				path.close();
				
				gc.setLineStyle(SWT.LINE_DASHDOTDOT);
				gc.drawPath(path);
				gc.setLineAttributes(lineAttr);
				gc.fillPath(path);
				path.dispose();				
			}
			
		});
	}
	
	public void dispose()
	{		
	}
	

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		Shape shape = new Shape(shell);
		
		shell.setText("Shapes");
		shell.setSize(800, 600);
		shell.open();
		while(!shell.isDisposed())
			if(!display.readAndDispatch())
				display.sleep();
		
		shape.dispose();
		display.dispose();

	}

}

package zcatt.examples.swt.graphics;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.TextLayout;
import org.eclipse.swt.graphics.TextStyle;

public class TextLayoutTab extends GraphicsTab {

	public TextLayoutTab(GraphicsExample example) {
		super(example);
		// TODO Auto-generated constructor stub
	}

	public void dispose()
	{}
	
	@Override
	public String getText() {
		return "TextLayout";
	}
	
	public String getDescription()
	{
		return "draw text";
	}
	
	public String getCategory()
	{
		return "Texts";
	}
	
	public boolean getDoubleBuffered()
	{
		return false;
	}
	
	public void paint(GC gc, int width, int height)
	{
		//gc.drawLine(0, 0, width, height);
		//gc.drawLine(width, 0, 0, height);		
		
		int POSX = 100;
		int POSY = 100;
		String text = "Hello, the world!\n你\t好\tabc!";
		// Hello, the world!\n你 \t 好 \t a b c !
		// 012345678901234567 8  9 0  1  2 3 4 5
		
		Font font;
		TextStyle textStyle1;
		TextStyle textStyle2;
		TextStyle textStyle3;

		TextLayout textLayout;
		
		font= new Font(gc.getDevice(), "Courier New", 48, /*SWT.BOLD |*/ SWT.ITALIC);
		textStyle1 = new TextStyle(font, new Color(0,0,0), new Color(255,255,255));
		textStyle2 = new TextStyle(font, new Color(0,255,0), new Color(255,255,0));
		textStyle3 = new TextStyle(font, new Color(0,0,255), new Color(0,255,255));
		
		textLayout= new TextLayout(gc.getDevice());
		textLayout.setTabs(new int[] {70,});
		textLayout.setFont(font);
		textLayout.setText(text);
		textLayout.setStyle(textStyle1, 0, 9);
		textLayout.setStyle(textStyle2, 14, 15);
		textLayout.setStyle(textStyle3, 18, 20);
		textLayout.draw(gc, POSX, POSY);
		
		Rectangle rect;		
		rect = textLayout.getBounds();
		System.out.println("rect="+rect);
		gc.setForeground(new Color(255,0,0));
		rect.x += POSX;
		rect.y += POSY;
		gc.drawRectangle(rect);
		
		for(int i = 0; i< text.length(); i++)
		{
			rect = textLayout.getBounds(i,i);
			rect.x += POSX;
			rect.y += POSY;
			gc.drawRectangle(rect);			
		}
		
		textLayout.dispose();
		font.dispose();

		
		POSY += 150;
	
		font = new Font(gc.getDevice(), "Courier New", 48, SWT.NONE);
		textLayout= new TextLayout(gc.getDevice());
		textLayout.setTabs(new int[] {70,});
		textLayout.setFont(font);
		textLayout.setText(text);
		textLayout.draw(gc, POSX, POSY);
		
		rect = textLayout.getBounds();
		gc.setForeground(new Color(255,0,0));
		rect.x += POSX;
		rect.y += POSY;
		gc.drawRectangle(rect);

		for(int i = 0; i< text.length(); i++)
		{
			rect = textLayout.getBounds(i,i);
			System.out.println("width="+rect.width);
			rect.x += POSX;
			rect.y += POSY;
			gc.drawRectangle(rect);			
		}		
		
		textLayout.dispose();
		font.dispose();
	}
	
	

}

package zcatt.examples.swt.hello;

import org.eclipse.swt.*;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class Hello {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setBounds(100,100, 320, 240);
		shell.setText("zcatt.hello swt example");		
		shell.setLayout(new FillLayout());
		Label lbl = new Label(shell, SWT.CENTER);
		lbl.setText("Hello, SWT!");		
		//shell.pack();
		shell.open();
		
		while(!shell.isDisposed())
			if(!display.readAndDispatch())
				display.sleep();
		
		display.dispose();
	}

}

package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;

public class ButtonTab extends WidgetTab {
	Image image;
	
	public ButtonTab(WidgetExample example) {
		super(example);
		image = WidgetExample.loadImage(example.display, "next.gif");		
	}

	@Override
	public void dispose() {
		image.dispose();
	}

	@Override
	public String getCategory() {
		return "Buttons";
	}

	@Override
	public String getDescription() {
		return "Show button";
	}

	@Override
	public void createControlPanel(Composite parent) {
	}


	@Override
	public String getText() {
		return "Button";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);

		GridLayout gridLayout;
		gridLayout = new GridLayout(2, true);
		comp.setLayout(gridLayout);
		
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				MessageBox mb = new MessageBox(example.shell, SWT.ICON_INFORMATION | SWT.OK);
				mb.setText("Check");
				
				Button b = (Button) e.widget;				
				mb.setMessage(b.getText() + " is " + (b.getSelection()? "on":"off") + ".");
				mb.open();
			}			
		};

		Button btn;
		Label label;
		
		btn = new Button(comp, SWT.ARROW | SWT.UP);
		btn.setText("arrow button");		
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.ARROW | SWT.UP");

		btn = new Button(comp, SWT.ARROW | SWT.LEFT|SWT.BORDER);
		btn.setText("arrow button");
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.ARROW | SWT.LEFT | SWT.BORDER");
		
		btn = new Button(comp, SWT.ARROW | SWT.RIGHT);
		btn.setText("arrow button");		
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.ARROW | SWT.RIGHT");

		btn = new Button(comp, SWT.ARROW | SWT.DOWN);
		btn.setText("arrow button");
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.ARROW | SWT.DOWN");

		
		btn = new Button(comp, SWT.PUSH);
		btn.setText("push button");
		btn.addSelectionListener(selectionAdapter);
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.PUSH");

		btn = new Button(comp, SWT.TOGGLE);
		btn.setText("toggle button");
		btn.addSelectionListener(selectionAdapter);
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.TOGGLE");

		btn = new Button(comp, SWT.PUSH);
		btn.setText("disabled button");
		btn.setEnabled(false);
		btn.addSelectionListener(selectionAdapter);
		label = new Label(comp, SWT.NONE);
		label.setText("setEnable(false).");
		
		btn = new Button(comp, SWT.PUSH|SWT.LEFT);
		btn.setText("left button");
		btn.addSelectionListener(selectionAdapter);
		btn.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.PUSH | SWT.LEFT");

		btn = new Button(comp, SWT.PUSH|SWT.CENTER);
		btn.setText("center button");
		btn.addSelectionListener(selectionAdapter);
		btn.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.PUSH | SWT.CENTER");

		btn = new Button(comp, SWT.PUSH|SWT.RIGHT);
		btn.setText("right button");
		btn.addSelectionListener(selectionAdapter);
		btn.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
		label = new Label(comp, SWT.NONE);
		label.setText("SWT.PUSH | SWT.RIGHT");
		
		Group group;
		group = new Group(comp, SWT.NONE);
		group.setText("group");
		group.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));

		gridLayout = new GridLayout(2, true);
		group.setLayout(gridLayout);
		
		btn = new Button(group, SWT.CHECK);
		btn.setText("check1 button");
		btn.addSelectionListener(selectionAdapter);
		label = new Label(group, SWT.NONE);
		label.setText("SWT.CHECK");

		btn = new Button(group, SWT.CHECK);
		btn.setText("tri-state button");
		//btn.setGrayed(true);
		btn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				MessageBox mb = new MessageBox(example.shell, SWT.ICON_INFORMATION | SWT.OK);
				mb.setText("Check");
				
				Button b = (Button) e.widget;				
				if (b.getSelection()) {
					if (!b.getGrayed()) {
						b.setGrayed(true);
					}
				} else {
					if (b.getGrayed()) {
						b.setGrayed(false);
						b.setSelection (true);
					}
				}

				mb.setMessage(b.getText() + " is " + (b.getGrayed()? "grayed" :(b.getSelection()? "on":"off")) + ".");
				mb.open();
			}			
			
			
		});
		label = new Label(group, SWT.NONE);
		label.setText("grayed button. used to implement 3-state check button");		
		
		btn = new Button(group, SWT.RADIO);
		btn.setText("radio1 button");
		btn.addSelectionListener(selectionAdapter);		
		label = new Label(group, SWT.NONE);
		label.setText("SWT.RADIO");
		
		btn = new Button(group, SWT.RADIO);
		btn.setText("radio2 button");
		btn.addSelectionListener(selectionAdapter);		
		label = new Label(group, SWT.NONE);
		label.setText("SWT.RADIO");
		

		btn = new Button(comp, SWT.PUSH);
		btn.setImage(image);					
		btn.setText("image button");
		label = new Label(comp, SWT.NONE);
		label.setText("image button. Image is left to text.");

		btn = new Button(comp, SWT.PUSH | SWT.RIGHT_TO_LEFT);
		btn.setImage(image);					
		btn.setText("image button");
		label = new Label(comp, SWT.NONE);
		label.setText("image button with SWT.RIGHT_TO_LEFT.");

		btn = new Button(comp, SWT.PUSH);
		btn.setText("color button");
		btn.setForeground(parent.getDisplay().getSystemColor(SWT.COLOR_RED));
		btn.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_YELLOW));
		label = new Label(comp, SWT.NONE);
		label.setText("color button.");		
	}
}

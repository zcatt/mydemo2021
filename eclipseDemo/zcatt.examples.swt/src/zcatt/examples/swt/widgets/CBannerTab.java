package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CBanner;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

public class CBannerTab extends WidgetTab {
	Text resultText;

	public CBannerTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "CBanner";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show CBanner";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("CBanner example.\n");
		resultText.append("CBanner较少使用.可用于摆放toolbars\n");
		
		Group grp;
		Button btn;

		grp= new Group(comp, SWT.NONE);
		grp.setText("CBanner");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(600, 200));
		
		CBanner cbanner;
		cbanner= new CBanner(grp, SWT.NONE);
		cbanner.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		cbanner.setSimple(false);
		
		btn = new Button(cbanner, SWT.PUSH);
		btn.setText("left");
		cbanner.setLeft(btn);
		
		btn = new Button(cbanner, SWT.PUSH);
		btn.setText("right");
		cbanner.setRight(btn);
		
		btn = new Button(cbanner, SWT.PUSH);
		btn.setText("bottom");
		cbanner.setBottom(btn);
		
		grp= new Group(comp, SWT.NONE);
		grp.setText("CBanner(simple)");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(600, 200));
		
		cbanner= new CBanner(grp, SWT.NONE);
		cbanner.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		cbanner.setSimple(true);
		
		btn = new Button(cbanner, SWT.PUSH);
		btn.setText("left");
		cbanner.setLeft(btn);
		
		btn = new Button(cbanner, SWT.PUSH);
		btn.setText("right");
		cbanner.setRight(btn);
		
		btn = new Button(cbanner, SWT.PUSH);
		btn.setText("bottom");
		cbanner.setBottom(btn);
		

	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

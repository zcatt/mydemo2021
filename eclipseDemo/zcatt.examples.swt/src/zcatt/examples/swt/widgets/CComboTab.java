package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class CComboTab extends WidgetTab {
	Text resultText;

	public CComboTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "CCombo";
	}

	@Override
	public String getCategory() {
		return "Choose";
	}

	@Override
	public String getDescription() {
		return "show CCombo";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(1, false));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		Group grp;
		CCombo combo;
		
		
		//两种填充list的方法, setItems()和add()
		String[] items= new String[] {
				"item 0 -asdf;ajsdfa;jkfasd;lfkjasasdfafdadsf",
				"item 1 ",
				"item 2 -asdf;ajsdfa;jkfasd;lfkjasasdfafdadsf",
				"item 3 ",
				"item 4 -asdf;ajsdfa;jkfasd;lfkjasasdfafdadsf",
				"item 5 ",
				"item 6 -asdf;ajsdfa;jkfasd;lfkjasasdfafdadsf",
				"item 7 ",
				"item 8 -asdf;ajsdfa;jkfasd;lfkjasasdfafdadsf",
				"item 9 ",
				"item 10 -asdf;ajsdfa;jkfasd;lfkjasasdfafdadsf",
		};
		
		SelectionListener selectionListener = SelectionListener.widgetSelectedAdapter(event->{
			CCombo c = (CCombo) event.widget;
			resultText.append("select "+c.getSelectionIndex()+", cnt= "+c.getText()+"\n");
		});
		ModifyListener modifyListener = event->{
			CCombo c = (CCombo) event.widget;
			resultText.append("modify "+c.getSelectionIndex()+", cnt= "+c.getText()+"\n");
			
		};
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("default");
		grp.setLayout(new GridLayout());
		
		combo = new CCombo(grp, SWT.NONE);
		combo.setItems(items);
		combo.select(5);
		combo.addSelectionListener(selectionListener);
		combo.addModifyListener(modifyListener);

		grp = new Group(comp, SWT.NONE);
		grp.setText("FLAT");
		grp.setLayout(new GridLayout());
		
		combo = new CCombo(grp, SWT.FLAT);
		combo.setItems(items);
		combo.select(5);
		combo.addSelectionListener(selectionListener);
		combo.addModifyListener(modifyListener);

		grp = new Group(comp, SWT.NONE);
		grp.setText("LEAD");
		grp.setLayout(new GridLayout());
		
		combo = new CCombo(grp, SWT.LEAD);
		combo.setTextLimit(9);
		combo.setItems(items);
		combo.select(5);
		combo.addSelectionListener(selectionListener);
		combo.addModifyListener(modifyListener);	

		grp = new Group(comp, SWT.NONE);
		grp.setText("TRAIL");
		grp.setLayout(new GridLayout());
		
		combo = new CCombo(grp, SWT.TRAIL);
		combo.setTextLimit(9);
		combo.setItems(items);
		combo.select(5);
		combo.addSelectionListener(selectionListener);
		combo.addModifyListener(modifyListener);	

		grp = new Group(comp, SWT.NONE);
		grp.setText("CENTER");
		grp.setLayout(new GridLayout());
		
		combo = new CCombo(grp, SWT.CENTER);
		combo.setTextLimit(9);
		combo.setItems(items);
		combo.select(5);
		combo.addSelectionListener(selectionListener);
		combo.addModifyListener(modifyListener);	

	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

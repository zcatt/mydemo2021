package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class CLabelTab extends WidgetTab {
	Image image;

	public CLabelTab(WidgetExample example) {
		super(example);
		image = WidgetExample.loadImage(example.display, "pattern2.jpg");
	}

	@Override
	public void dispose() {
		image.dispose();
		image = null;	
	}

	@Override
	public String getText() {
		return "CLabel";
	}

	@Override
	public String getCategory() {
		return "Labels";
	}

	@Override
	public String getDescription() {
		return "show CLabel";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.BORDER);
		
		GridLayout gridLayout = new GridLayout(1, true);
		comp.setLayout(gridLayout);	
		
		
		CLabel label;
		
		label = new CLabel(comp, SWT.NONE);		
		label.setText("Default style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));


		label = new CLabel(comp, SWT.BORDER);		
		label.setText("BORDER style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));

		label = new CLabel(comp, SWT.SHADOW_IN);		
		label.setText("SHADOW_IN style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));
		
		label = new CLabel(comp, SWT.SHADOW_OUT);		
		label.setText("SHADOW_OUT style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));

		label = new CLabel(comp, SWT.LEFT|SWT.BORDER);		
		label.setText("LEFT style");
		label.setLayoutData(new GridData(200, 50));
		
		label = new CLabel(comp, SWT.CENTER|SWT.BORDER);		
		label.setText("CENTER style");
		label.setImage(image);				
		label.setLayoutData(new GridData(200, 50));
		
		label = new CLabel(comp, SWT.RIGHT|SWT.BORDER);		
		label.setImage(image);				
		label.setText("RIGHT style");
		label.setLayoutData(new GridData(200, 50));
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

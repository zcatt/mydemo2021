package zcatt.examples.swt.widgets;

import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Text;

public class CTabFolderTab extends WidgetTab {
	Text resultText;
	CTabFolder tabFolder;
	Button btnClose;
	Button btnBottom;
	Button btnFlat;
	Button btnBorder;
	Button btnMulti;

	Button btnBorderVisible;
	Button btnHighlightEnabled;
	Button btnMRUVisible;
	Button btnInsertMark;
	Button btnSimple;
	Button btnSingle;
	Button btnUnselectedCloseVisible;
	Button btnUnselectedImageVisible;

	Image img;
	
	public CTabFolderTab(WidgetExample example) {
		super(example);
		img = WidgetExample.loadImage(example.display, "pattern1.jpg");
	}

	@Override
	public void dispose() {
		if(img != null)
		{
			img.dispose();
			img = null;
		}
	}

	@Override
	public String getText() {
		return "CTabFolder";
	}

	@Override
	public String getCategory() {
		return "Choose";
	}

	@Override
	public String getDescription() {
		return "show CTabFolder";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("CTabFoler example.\n");
		
		Group grp = new Group(comp, SWT.NONE);
		grp.setText("CTabFolder");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		int style = calcStyle();
		
		tabFolder = new CTabFolder(grp, style);
		tabFolder.setLayoutData(new GridData(300, 100));
		tabFolder.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			resultText.append("select "+tabFolder.getSelectionIndex()+"\n");
		}));
		
		String[] names = new String[] {
				"tab 0",
				"tab 1",
				"tab 2",
				"tab 3",
				"tab 4",
				"tab 5",
				"tab 6",
		};
		
		for(int i = 0; i< names.length; i++)
		{
			CTabItem item = new CTabItem(tabFolder, SWT.NONE);
			item.setText(names[i]);
			item.setImage(img);			

			Text text = new Text(tabFolder, SWT.WRAP | SWT.MULTI);
			text.setText("content of tab "+i+"\n");
			item.setControl(text);			
		}
		
		tabFolder.setSelection(0);		
		tabFolder.setSimple(false);		
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		GridLayout gridLayout;
		gridLayout = new GridLayout(3, true);
		comp.setLayout(gridLayout);
		
		Group grp;
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("styles");;
		grp.setLayout(new GridLayout());

		
		btnClose = new Button(grp, SWT.CHECK);
		btnClose.setText("CLOSE style");
		btnClose.addSelectionListener(widgetSelectedAdapter(event -> {
			example.recreateWidgetComp();
		}));
		
		btnBottom = new Button(grp, SWT.CHECK);
		btnBottom.setText("BOTTOM style");
		btnBottom.addSelectionListener(widgetSelectedAdapter(event -> {
			example.recreateWidgetComp();
		}));
		
		btnFlat = new Button(grp, SWT.CHECK);
		btnFlat.setText("FLAT style");
		btnFlat.addSelectionListener(widgetSelectedAdapter(event -> {
			example.recreateWidgetComp();
		}));
		
		btnBorder = new Button(grp, SWT.CHECK);
		btnBorder.setText("BORDER style");
		btnBorder.addSelectionListener(widgetSelectedAdapter(event -> {
			example.recreateWidgetComp();
		}));
		
		btnMulti = new Button(grp, SWT.CHECK);
		btnMulti.setText("MULTI style");
		btnMulti.setSelection(true);
		btnMulti.addSelectionListener(widgetSelectedAdapter(event -> {
			example.recreateWidgetComp();
		}));
		

		grp = new Group(comp, SWT.NONE);
		grp.setText("options");;
		grp.setLayout(new GridLayout());
		
		
		btnBorderVisible = new Button(grp, SWT.CHECK);
		btnBorderVisible.setText("border visible");
		btnBorderVisible.addSelectionListener(widgetSelectedAdapter(event -> {
			if(tabFolder != null)
				tabFolder.setBorderVisible(btnBorderVisible.getSelection());
		}));
		
		btnHighlightEnabled = new Button(grp, SWT.CHECK);
		btnHighlightEnabled.setText("highlight enabled");
		btnHighlightEnabled.addSelectionListener(widgetSelectedAdapter(event -> {
			if(tabFolder != null)
				tabFolder.setHighlightEnabled(btnHighlightEnabled.getSelection());
		}));
		
		btnMRUVisible = new Button(grp, SWT.CHECK);
		btnMRUVisible.setText("MRU visible");
		btnMRUVisible.addSelectionListener(widgetSelectedAdapter(event -> {
			if(tabFolder != null)
				tabFolder.setMRUVisible(false);
		}));
		
		btnInsertMark = new Button(grp, SWT.CHECK);
		btnInsertMark.setText("insert mark");
		btnInsertMark.addSelectionListener(widgetSelectedAdapter(event -> {
			if(tabFolder != null)
			{
				tabFolder.setInsertMark(0, true);
				tabFolder.setInsertMark(2, false);
				tabFolder.setInsertMark(2, true);
			}
		}));
		
		btnSimple = new Button(grp, SWT.CHECK);
		btnSimple.setText("simple");
		btnSimple.addSelectionListener(widgetSelectedAdapter(event -> {
			if(tabFolder != null)
			{
				tabFolder.setSimple(btnSimple.getSelection());
			}
		}));

		btnSingle = new Button(grp, SWT.CHECK);
		btnSingle.setText("single");
		btnSingle.addSelectionListener(widgetSelectedAdapter(event -> {
			if(tabFolder != null)
			{
				tabFolder.setSingle(btnSingle.getSelection());
			}
		}));

		btnUnselectedCloseVisible = new Button(grp, SWT.CHECK);
		btnUnselectedCloseVisible.setText("unselected close visible");
		btnUnselectedCloseVisible.addSelectionListener(widgetSelectedAdapter(event -> {
			if(tabFolder != null)
			{
				tabFolder.setUnselectedCloseVisible(btnUnselectedCloseVisible.getSelection());
			}
		}));
		
		btnUnselectedImageVisible = new Button(grp, SWT.CHECK);
		btnUnselectedImageVisible.setText("unselected image visible");
		btnUnselectedImageVisible.addSelectionListener(widgetSelectedAdapter(event -> {
			if(tabFolder != null)
			{
				tabFolder.setUnselectedImageVisible(btnUnselectedImageVisible.getSelection());
			}
		}));
		
	}

	int calcStyle()
	{
		int style = 0;
		
		if(btnClose.getSelection())
			style |= SWT.CLOSE;
		
		style |= btnBottom.getSelection() ? SWT.BOTTOM : SWT.TOP;
		
		if(btnFlat.getSelection())
			style |= SWT.FLAT;
		
		if(btnBorder.getSelection())
			style |= SWT.BORDER;
		
		style |= btnMulti.getSelection()? SWT.MULTI : SWT.SINGLE;
		
		return style;
	}
}

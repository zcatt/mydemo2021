package zcatt.examples.swt.widgets;

import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Caret;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.IME;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;

public class CaretTab extends WidgetTab {
	final static int POS_X = 10;
	final static int POS_Y = 10;
	final static int DEFAULT_WIDTH = 1;
	final static int DEFAULT_HEIGHT = 16;
	
	Point ptCaret;
	
	Image image;
	Canvas canvas;
	Caret caret;
	

	public CaretTab(WidgetExample example) {
		super(example);
		ptCaret = new Point(POS_X, POS_Y);
		image = WidgetExample.loadImage(example.shell.getDisplay(), "pattern2.jpg");
	}

	@Override
	public void dispose() {
		image.dispose();
	}

	@Override
	public String getText() {
		return "Caret";
	}

	@Override
	public String getCategory() {
		return "Carets";
	}

	@Override
	public String getDescription() {
		return "show caret";
	}

	@Override
	public void createWidget(Composite parent) {
		canvas = new Canvas(parent, SWT.NONE);
		Caret oldCaret = canvas.getCaret();
		if(oldCaret != null)
			oldCaret.dispose();
		
		caret = new Caret(canvas, SWT.NONE);		
		caret.setBounds(ptCaret.x, ptCaret.y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
		canvas.setCaret(caret);

		canvas.addDisposeListener(event->{
			//TODO
		});
		
		canvas.addPaintListener(event -> {
			//System.out.println(java.time.Instant.now());
			Rectangle rect = canvas.getClientArea();

		
			GC gc = event.gc;
			gc.drawLine(0, 0, rect.width -1, rect.height -1);
			gc.drawLine(rect.width -1, 0, 0, rect.height -1);
			
/*			
			assert caret == canvas.getCaret();
			if(caret != null)
			{
				gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_RED));
				gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_YELLOW));
				gc.fillRectangle(caret.getBounds());
			}
*/						
		});

		//添加keyListener以允许canvas获得focus. see Composite.hooksKeys().
		canvas.addKeyListener(new KeyAdapter() {});		
		
		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				caret.setLocation(e.x,  e.y);
			}
		});
		

	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		GridLayout gridLayout;
		gridLayout = new GridLayout(3, true);
		comp.setLayout(gridLayout);
		
		Group grp;
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("options");;
		gridLayout = new GridLayout();
		grp.setLayout(gridLayout);
		
		Button btn;
		
		btn = new Button(grp, SWT.CHECK);
		btn.setText("use image");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			Button btnUseImage = (Button) event.widget;

			if(btnUseImage.getSelection())
			{				
				caret.setImage(image);		//zf, setImage()效果会覆盖setBounds()
			}
			else
			{				
				caret.setImage(null);
			}
			//canvas.redraw();
			canvas.setFocus();
		}));
		
		btn = new Button(grp, SWT.CHECK);
		btn.setText("1 x 40 shape");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			
			Button btnShape = (Button) event.widget;

			//caret.setImage(null);

			if(btnShape.getSelection())
			{
				caret.setBounds(ptCaret.x, ptCaret.y, 10, 40);
			}
			else
			{
				caret.setBounds(ptCaret.x, ptCaret.y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
			}
			//canvas.redraw();
			canvas.setFocus();
		}));
		

/*		
		grp = new Group(comp, SWT.NONE);
		grp.setText("radio");;
		gridLayout = new GridLayout();
		grp.setLayout(gridLayout);

		grp = new Group(comp, SWT.NONE);
		grp.setText("custom");
		gridLayout = new GridLayout();
		grp.setLayout(gridLayout);
*/		
		
		
	}

}

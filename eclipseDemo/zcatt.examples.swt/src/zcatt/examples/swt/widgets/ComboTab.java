package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class ComboTab extends WidgetTab {
	Text resultText;

	public ComboTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Combo";
	}

	@Override
	public String getCategory() {
		return "Choose";
	}

	@Override
	public String getDescription() {
		return "show combo";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(2, false));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		Label label;
		Combo combo;
		
		label = new Label(comp, SWT.NONE);
		label.setText("DROP_DOWN combo:");
		
		
		//两种填充list的方法, setItems()和add()
		String[] items= new String[] {
				"item 0",
				"item 1",
				"item 2",
				"item 3",
				"item 4",
				"item 5",
				"item 6",
				"item 7",
				"item 8",
				"item 9",
				"item 10",
		};
		
		SelectionListener selectionListener = SelectionListener.widgetSelectedAdapter(event->{
			Combo c = (Combo) event.widget;
			resultText.append("select "+c.getSelectionIndex()+", cnt= "+c.getText()+"\n");
		});
		ModifyListener modifyListener = event->{
			Combo c = (Combo) event.widget;
			resultText.append("select "+c.getSelectionIndex()+", cnt= "+c.getText()+"\n");
			
		};
		
		combo = new Combo(comp, SWT.DROP_DOWN);
		combo.setItems(items);
		combo.select(5);
		combo.addSelectionListener(selectionListener);
		combo.addModifyListener(modifyListener);

		label = new Label(comp, SWT.NONE);
		label.setText("DROP_DOWN|READ_ONLY combo:");
		
		combo = new Combo(comp, SWT.DROP_DOWN|SWT.READ_ONLY);
		combo.setItems(items);
		combo.select(5);
		combo.addSelectionListener(selectionListener);
		
		label = new Label(comp, SWT.NONE);
		label.setText("SIMPLE combo:");
		
		combo = new Combo(comp, SWT.SIMPLE);		//simple combo选择时会有多个selectionEvent触发
		combo.setItems(items);
		combo.select(5);
		combo.addSelectionListener(selectionListener);
		combo.addModifyListener(modifyListener);
	}

	@Override
	public void createControlPanel(Composite parent) {
	}

}

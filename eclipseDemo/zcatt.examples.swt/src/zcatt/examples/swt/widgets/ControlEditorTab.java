package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

public class ControlEditorTab extends WidgetTab {
	Text resultText;
	Color color;
	
	public ControlEditorTab(WidgetExample example) {
		super(example);
		color = new Color(null, 255, 0, 0);
	}

	@Override
	public void dispose() {
		color.dispose();
	}

	@Override
	public String getText() {
		return "ControlEditor";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show ControlEditor";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("ControlEditor example.\n");
		resultText.append("ControlEditor本身不是widget.通常设置于canvas上, 因canvas一般不使用layout.\n");
		resultText.append("ControlEditor应当不必要明确调用dispose()\n");
		
		Group grp = new Group(comp, SWT.NONE);
		grp.setText("ControlEditor");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		//grp.setLayoutData(new GridData(600,350));
		
		Canvas canvas = new Canvas(grp, SWT.BORDER);
		canvas.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		//canvas.setBounds(10, 20, 400, 300);
		canvas.setBackground(color);
		
		ControlEditor editor1 = new ControlEditor (canvas);
		ControlEditor editor2 = new ControlEditor (canvas);
		// The editor will be a button in the bottom right corner of the canvas.
		// When selected, it will launch a Color dialog that will change the background
		// of the canvas.
		
		Button button = new Button(canvas, SWT.PUSH);
		button.setText("Select Color...");
		button.addSelectionListener (new SelectionAdapter() {
		        public void widgetSelected(SelectionEvent e) {
		                ColorDialog dialog = new ColorDialog(example.shell);
		                dialog.open();
		                RGB rgb = dialog.getRGB();
		                if (rgb != null) {
		                        if (color != null) color.dispose();
		                        color = new Color(null, rgb);
		                        canvas.setBackground(color);
		                }
		
		        }
		});
		
		Button button2 = new Button(canvas, SWT.PUSH);
		button2.setText("button");

		Point size;
		editor1.horizontalAlignment = SWT.CENTER;
		editor1.verticalAlignment = SWT.CENTER;
		editor1.grabHorizontal = false;
		editor1.grabVertical = false;
		size= button.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		editor1.minimumWidth = size.x;
		editor1.minimumHeight = size.y;
		editor1.setEditor (button);		
		editor2.horizontalAlignment = SWT.RIGHT;
		editor2.verticalAlignment = SWT.BOTTOM;
		editor2.grabHorizontal = false;
		editor2.grabVertical = false;
		size= button2.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		editor2.minimumWidth = size.x;
		editor2.minimumHeight = size.y;
		editor2.setEditor (button2);		
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

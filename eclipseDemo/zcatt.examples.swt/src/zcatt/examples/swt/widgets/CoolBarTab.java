package zcatt.examples.swt.widgets;

import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class CoolBarTab extends WidgetTab {
	Text resultText;
	
	Image copyImg;
	Image cutImg;
	Image pasteImg;
	Image deleteImg;

	public CoolBarTab(WidgetExample example) {
		super(example);
		copyImg = WidgetExample.loadImage(example.display, "cmd_Copy.gif");
		cutImg = WidgetExample.loadImage(example.display, "cmd_Cut.gif");
		pasteImg = WidgetExample.loadImage(example.display, "cmd_Paste.gif");
		deleteImg = WidgetExample.loadImage(example.display, "cmd_Delete.gif");
	}

	@Override
	public void dispose() {
		copyImg.dispose();
		cutImg.dispose();
		pasteImg.dispose();
		deleteImg.dispose();
	}

	@Override
	public String getText() {
		return "CoolBar";
	}

	@Override
	public String getCategory() {
		return "ToolBars";
	}

	@Override
	public String getDescription() {
		return "show coolBar";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(1, false));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("CoolBar and CoolItem examples.\n");

		Group grp;
		CoolBar coolbar;
		CoolItem coolitem;
		ToolBar toolbar;
		ToolItem toolitem;

		grp = new Group(comp, SWT.NONE);		
		grp.setText("HORIZONTAL and DROP_DOWN(CoolItem)");
		grp.setLayout(new RowLayout(SWT.VERTICAL));
		
		coolbar = new CoolBar(grp, SWT.HORIZONTAL);
		
		SelectionListener coolItemListener = new SelectionAdapter() {
			Menu menu= null;	//由parent shell负责dispose
			@Override
			public void widgetSelected(SelectionEvent event) {
				
				if(event.detail == SWT.ARROW)
				{
					if(menu != null)
					{
						menu.dispose();
						menu = null;
						return;
					}

					//get coolItem bounds
					CoolItem coolItem = (CoolItem) event.widget;
					CoolBar coolBar = (CoolBar) coolItem.getParent();
					Rectangle itemBounds = coolItem.getBounds ();
					itemBounds.width = event.x - itemBounds.x;
					Point pt = coolBar.toDisplay(new Point (itemBounds.x, itemBounds.y));
					itemBounds.x = pt.x;
					itemBounds.y = pt.y;
					
					//calc which one toolItem past the bounds of the coolItem 
					ToolBar toolBar = (ToolBar) coolItem.getControl ();
					ToolItem[] tools = toolBar.getItems ();
					int toolCount = tools.length;

					int i = 0;
					while (i < toolCount) {
						Rectangle toolBounds = tools[i].getBounds ();
						pt = toolBar.toDisplay(new Point(toolBounds.x, toolBounds.y));
						toolBounds.x = pt.x;
						toolBounds.y = pt.y;
						Rectangle intersection = itemBounds.intersection (toolBounds);
						if (!intersection.equals (toolBounds)) break;
						i++;
					}

					//popup menu
					menu = new Menu(example.shell, SWT.POP_UP);
					for (int j = i; j < toolCount; j++) {
						ToolItem tool = tools[j];
						Image image = tool.getImage();
						if (image == null) {
							new MenuItem (menu, SWT.SEPARATOR);
						} else {
							MenuItem mi = new MenuItem(menu, SWT.NONE);
							mi.setText(tool.getText());
							mi.setImage(image);
						}
					}
					
					pt = coolBar.toDisplay(new Point(event.x, event.y));
					menu.setLocation (pt.x, pt.y);
					menu.setVisible (true);
					while (menu != null && !menu.isDisposed() && menu.isVisible ()) {
						if (!example.display.readAndDispatch ()) example.display.sleep ();
					}
					if (menu != null) {
						menu.dispose ();
						menu = null;
					}					
				}
			}
			
		};
		
		
		SelectionListener toolItemListener = new SelectionAdapter() {
			Menu menu= null;	//由parent shell负责dispose
			@Override
			public void widgetSelected(SelectionEvent event) {
				if(menu == null)
				{
					menu = new Menu(example.shell, SWT.POP_UP);
					MenuItem mi;
					mi = new MenuItem(menu, SWT.NONE);
					mi.setText("item 1");
					mi.addSelectionListener(widgetSelectedAdapter(e-> resultText.append("item 1" +" clicked.\n")));
					mi = new MenuItem(menu, SWT.NONE);
					mi.setText("item 2");
					mi.addSelectionListener(widgetSelectedAdapter(e-> resultText.append("item 2" +" clicked.\n")));
					mi = new MenuItem(menu, SWT.NONE);
					mi.setText("item 3");
					mi.addSelectionListener(widgetSelectedAdapter(e-> resultText.append("item 3" +" clicked.\n")));
					mi = new MenuItem(menu, SWT.NONE);
					mi.setText("item 4");					
					mi.addSelectionListener(widgetSelectedAdapter(e-> resultText.append("item 4" +" clicked.\n")));
				}
				
				if(event.detail == SWT.ARROW)
				{
					final ToolItem toolItem = (ToolItem) event.widget;
					final ToolBar  toolBar = toolItem.getParent();

					Rectangle toolItemBounds = toolItem.getBounds();
					Point point = toolBar.toDisplay(new Point(toolItemBounds.x, toolItemBounds.y));
					menu.setLocation(point.x, point.y + toolItemBounds.height);
					menu.setVisible(true);
				}
				else
				{
					ToolItem ti = (ToolItem) event.widget;
					resultText.append(ti.getText() +" clicked.\n");					
				}
			}
			
		};
		
		
		//#1, the push toolItems
		toolbar = new ToolBar(coolbar, SWT.HORIZONTAL);
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setImage(copyImg);
		toolitem.setText("copy");
		toolitem.setToolTipText("copy");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setImage(pasteImg);
		toolitem.setText("paste");
		toolitem.setToolTipText("paste");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setImage(cutImg);
		toolitem.setText("cut");
		toolitem.setToolTipText("cut");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setImage(deleteImg);
		toolitem.setText("delete");
		toolitem.setToolTipText("delete");
		toolitem.addSelectionListener(toolItemListener);
		coolitem = new CoolItem(coolbar, SWT.DROP_DOWN);
		coolitem.setControl(toolbar);
		coolitem.addSelectionListener(coolItemListener);
		

		//#2.drop_down toolItems
		toolbar = new ToolBar(coolbar, SWT.HORIZONTAL);
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setImage(copyImg);
		toolitem.setText("copy");
		toolitem.setToolTipText("copy");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setImage(pasteImg);
		toolitem.setText("paste");
		toolitem.setToolTipText("paste");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setImage(cutImg);
		toolitem.setText("cut");
		toolitem.setToolTipText("cut");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setImage(deleteImg);
		toolitem.setText("delete");
		toolitem.setToolTipText("delete");		
		toolitem.addSelectionListener(toolItemListener);
		coolitem = new CoolItem(coolbar, SWT.DROP_DOWN);
		coolitem.setControl(toolbar);
		coolitem.addSelectionListener(coolItemListener);
		
		//#3.radio toolItems
		toolbar = new ToolBar(coolbar, SWT.HORIZONTAL);
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setImage(copyImg);
		toolitem.setText("copy");
		toolitem.setToolTipText("copy");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setImage(pasteImg);
		toolitem.setText("paste");
		toolitem.setToolTipText("paste");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setImage(cutImg);
		toolitem.setText("cut");
		toolitem.setToolTipText("cut");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setImage(deleteImg);
		toolitem.setText("delete");
		toolitem.setToolTipText("delete");
		toolitem.addSelectionListener(toolItemListener);
		coolitem = new CoolItem(coolbar, SWT.DROP_DOWN);
		coolitem.setControl(toolbar);
		coolitem.addSelectionListener(coolItemListener);

		//#4.check toolItems
		toolbar = new ToolBar(coolbar, SWT.HORIZONTAL);
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setImage(copyImg);
		toolitem.setText("copy");
		toolitem.setToolTipText("copy");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setImage(pasteImg);
		toolitem.setText("paste");
		toolitem.setToolTipText("paste");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setImage(cutImg);
		toolitem.setText("cut");
		toolitem.setToolTipText("cut");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setImage(deleteImg);
		toolitem.setText("delete");
		toolitem.setToolTipText("delete");
		toolitem.addSelectionListener(toolItemListener);
		coolitem = new CoolItem(coolbar, SWT.DROP_DOWN);
		coolitem.setControl(toolbar);
		coolitem.addSelectionListener(coolItemListener);
		
		//设置coolitem的size. coolbar据此布局
		CoolItem[] coolItems = coolbar.getItems();
		for (CoolItem coolItem : coolItems) {
			Control control = coolItem.getControl();
			Point size = control.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			Point coolSize = coolItem.computeSize(size.x, size.y);
			if (control instanceof ToolBar) {
				ToolBar bar = (ToolBar)control;
				if (bar.getItemCount() > 0) {
						size.x = bar.getItem(0).getWidth();	//horizontal. 至少容纳一个toolItem 
				}
			}
			coolItem.setMinimumSize(size);			
			coolItem.setPreferredSize(coolSize);
			coolItem.setSize(coolSize);
		}
		
		coolbar.setWrapIndices(new int[] {1,3});		

		grp = new Group(comp, SWT.NONE);		
		grp.setText("HORIZONTAL, FLAT and DROP_DOWN(CoolItem)");
		grp.setLayout(new RowLayout(SWT.VERTICAL));
		
		coolbar = new CoolBar(grp, SWT.HORIZONTAL|SWT.FLAT);		
		
		//#1, the push toolItems
		toolbar = new ToolBar(coolbar, SWT.HORIZONTAL);
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setImage(copyImg);
		toolitem.setText("copy");
		toolitem.setToolTipText("copy");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setImage(pasteImg);
		toolitem.setText("paste");
		toolitem.setToolTipText("paste");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setImage(cutImg);
		toolitem.setText("cut");
		toolitem.setToolTipText("cut");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setImage(deleteImg);
		toolitem.setText("delete");
		toolitem.setToolTipText("delete");
		toolitem.addSelectionListener(toolItemListener);
		coolitem = new CoolItem(coolbar, SWT.DROP_DOWN);
		coolitem.setControl(toolbar);
		coolitem.addSelectionListener(coolItemListener);
		

		//#2.drop_down toolItems
		toolbar = new ToolBar(coolbar, SWT.HORIZONTAL);
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setImage(copyImg);
		toolitem.setText("copy");
		toolitem.setToolTipText("copy");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setImage(pasteImg);
		toolitem.setText("paste");
		toolitem.setToolTipText("paste");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setImage(cutImg);
		toolitem.setText("cut");
		toolitem.setToolTipText("cut");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setImage(deleteImg);
		toolitem.setText("delete");
		toolitem.setToolTipText("delete");		
		toolitem.addSelectionListener(toolItemListener);
		coolitem = new CoolItem(coolbar, SWT.DROP_DOWN);
		coolitem.setControl(toolbar);
		coolitem.addSelectionListener(coolItemListener);
		
		//#3.radio toolItems
		toolbar = new ToolBar(coolbar, SWT.HORIZONTAL);
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setImage(copyImg);
		toolitem.setText("copy");
		toolitem.setToolTipText("copy");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setImage(pasteImg);
		toolitem.setText("paste");
		toolitem.setToolTipText("paste");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setImage(cutImg);
		toolitem.setText("cut");
		toolitem.setToolTipText("cut");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setImage(deleteImg);
		toolitem.setText("delete");
		toolitem.setToolTipText("delete");
		toolitem.addSelectionListener(toolItemListener);
		coolitem = new CoolItem(coolbar, SWT.DROP_DOWN);
		coolitem.setControl(toolbar);
		coolitem.addSelectionListener(coolItemListener);

		//#4.check toolItems
		toolbar = new ToolBar(coolbar, SWT.HORIZONTAL);
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setImage(copyImg);
		toolitem.setText("copy");
		toolitem.setToolTipText("copy");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setImage(pasteImg);
		toolitem.setText("paste");
		toolitem.setToolTipText("paste");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setImage(cutImg);
		toolitem.setText("cut");
		toolitem.setToolTipText("cut");
		toolitem.addSelectionListener(toolItemListener);
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setImage(deleteImg);
		toolitem.setText("delete");
		toolitem.setToolTipText("delete");
		toolitem.addSelectionListener(toolItemListener);
		coolitem = new CoolItem(coolbar, SWT.DROP_DOWN);
		coolitem.setControl(toolbar);
		coolitem.addSelectionListener(coolItemListener);
		
		//设置coolitem的size. coolbar据此布局
		coolItems = coolbar.getItems();
		for (CoolItem coolItem : coolItems) {
			Control control = coolItem.getControl();
			Point size = control.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			Point coolSize = coolItem.computeSize(size.x, size.y);
			if (control instanceof ToolBar) {
				ToolBar bar = (ToolBar)control;
				if (bar.getItemCount() > 0) {
						size.x = bar.getItem(0).getWidth();	//horizontal. 至少容纳一个toolItem 
				}
			}
			coolItem.setMinimumSize(size);			
			coolItem.setPreferredSize(coolSize);
			coolItem.setSize(coolSize);
		}
		
		coolbar.setWrapIndices(new int[] {1,3});		

	}

	@Override
	public void createControlPanel(Composite parent) {
	}

}

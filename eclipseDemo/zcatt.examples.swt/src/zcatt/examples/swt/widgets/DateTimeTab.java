package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

public class DateTimeTab extends WidgetTab {
	Text resultText;

	public DateTimeTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "DateTime";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show DateTime";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(1, false));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("DateTime examples.\n");
		
		Group grp;
		DateTime datetime;
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("TIME");
		grp.setLayout(new GridLayout(1, false));
		
		datetime = new DateTime(grp, SWT.TIME);
		datetime.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			DateTime dt = (DateTime) event.widget;
			resultText.append("Time is "+dt+"\n");
		}));
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("DATE | LONG");
		grp.setLayout(new GridLayout(1, false));
		
		datetime = new DateTime(grp, SWT.DATE|SWT.LONG);
		datetime.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			DateTime dt = (DateTime) event.widget;
			resultText.append("Date is "+dt+"\n");
		}));
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("CALENDER_WEEKNUMBER");
		grp.setLayout(new GridLayout(1, false));
		
		datetime = new DateTime(grp, SWT.CALENDAR| SWT.CALENDAR_WEEKNUMBERS);
		datetime.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			DateTime dt = (DateTime) event.widget;
			resultText.append("Calendar is "+dt+"\n");
		}));	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

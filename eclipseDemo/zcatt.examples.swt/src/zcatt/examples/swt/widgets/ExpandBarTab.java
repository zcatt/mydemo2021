package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Text;

public class ExpandBarTab extends WidgetTab {
	Text resultText;

	Image copyImg;
	Image cutImg;
	Image pasteImg;
	Image deleteImg;

	public ExpandBarTab(WidgetExample example) {
		super(example);
		copyImg = WidgetExample.loadImage(example.display, "cmd_Copy.gif");
		cutImg = WidgetExample.loadImage(example.display, "cmd_Cut.gif");
		pasteImg = WidgetExample.loadImage(example.display, "cmd_Paste.gif");
		deleteImg = WidgetExample.loadImage(example.display, "cmd_Delete.gif");
	}

	@Override
	public void dispose() {
		copyImg.dispose();
		cutImg.dispose();
		pasteImg.dispose();
		deleteImg.dispose();
	}

	@Override
	public String getText() {
		return "ExpandBar";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show ExpandBar";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("ExpandBar examples.\n");
		
		
		ExpandBar eb;
		ExpandItem ei;
		Composite composite;
		
		eb= new ExpandBar(comp, SWT.V_SCROLL);
		eb.addExpandListener(ExpandListener.itemExpandedAdapter(event->{
			ExpandBar bar = (ExpandBar) event.widget;
			ExpandItem curItem = (ExpandItem) event.item;
			resultText.append("Item "+ curItem.getText()+ " expanded.\n");
			for(int i = 0; i< bar.getItemCount(); i++)
			{
				ExpandItem e = bar.getItem(i);
				if(e == curItem)
					continue;
				if(e.getExpanded())
				{
					e.setExpanded(false);
					resultText.append("Item "+ e.getText()+ " collapsed.\n");				
				}
			}
		}));

		ei = new ExpandItem(eb, SWT.NONE, 0);
		ei.setText("edit actions");
		ei.setImage(copyImg);

		
		composite = new Composite (eb, SWT.NONE);
		composite.setLayout(new GridLayout ());
		new Button (composite, SWT.PUSH).setText("copy");
		new Button (composite, SWT.PUSH).setText("paste");
		new Button (composite, SWT.PUSH).setText("cut");
		new Button (composite, SWT.PUSH).setText("delete");
		
		ei.setHeight(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		ei.setControl(composite);
		ei.setExpanded(true);

		ei = new ExpandItem(eb, SWT.NONE, 0);
		ei.setText("file actions");
		//ei.setImage(cutImg);

		composite = new Composite (eb, SWT.NONE);
		composite.setLayout(new GridLayout ());
		new Button (composite, SWT.PUSH).setText("new");
		new Button (composite, SWT.PUSH).setText("open");
		new Button (composite, SWT.PUSH).setText("save");
		
		ei.setHeight(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		ei.setControl(composite);
		//ei.setExpanded(true);
		
		
	
		
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

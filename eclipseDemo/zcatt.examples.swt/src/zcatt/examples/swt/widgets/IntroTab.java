package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class IntroTab extends WidgetTab {


	public IntroTab(WidgetExample example) {
		super(example);
	}

	@Override
	public String getText() {
		return "Introduction";
	}

	@Override
	public String getCategory()
	{
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "introduction tab";
	}

	@Override
	public void dispose() {
	}

	@Override
	public void createWidget(Composite parent) {
		
		Text text= new Text(parent, SWT.MULTI);
		text.setText("Widget Example\nThis example shows how to create and use some widgets.");
	}

	@Override
	public void createControlPanel(Composite parent) {
	}

}

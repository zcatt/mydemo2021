package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class LabelTab extends WidgetTab {
	Image image;

	public LabelTab(WidgetExample example) {
		super(example);
		image = WidgetExample.loadImage(example.display, "pattern2.jpg");
	}

	public void dispose()
	{
		image.dispose();
		image = null;	
	}
	
	@Override
	public void createControlPanel(Composite parent) {
	}

	@Override
	public String getCategory() {
		return "Labels";
	}

	@Override
	public String getText() {
		return "Label";
	}

	public String getDescription()
	{
		return "Show various styles of label.";
	}
	
	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		
		GridLayout gridLayout = new GridLayout(1, true);
		comp.setLayout(gridLayout);
		
		
		Label label;
		
		label = new Label(comp, SWT.NONE);		
		label.setText("Default style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));


		label = new Label(comp, SWT.BORDER);		
		label.setText("BORDER style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));

		label = new Label(comp, SWT.SHADOW_IN);		
		label.setText("SHADOW_IN style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));
		
		label = new Label(comp, SWT.SHADOW_OUT);		
		label.setText("SHADOW_OUT style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));

		label = new Label(comp, SWT.VERTICAL);		
		label.setText("VERTICAL style");
		//label.setLayoutData(new GridData (SWT.CENTER, SWT.CENTER, false, false));
		
		label = new Label(comp, SWT.VERTICAL);		
		label.setText("image");
		label.setImage(image);					//setImage()和setText(), 最后的设置决定了是显示文本还是图像.
		assert label.getText().equals("image");
		
		label = new Label(comp, SWT.VERTICAL);		
		label.setImage(image);					//setImage()和setText(), 最后的设置决定了是显示文本还是图像.
		label.setText("image");
		
	}

}

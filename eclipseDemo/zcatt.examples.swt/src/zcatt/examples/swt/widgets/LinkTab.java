package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Text;

public class LinkTab extends WidgetTab {
	Text resultText;

	public LinkTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Link";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show Link";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("ExpandBar examples.\n");

		Link link = new Link(comp, SWT.NONE);
		link.setText("go to <a href=\"http://www.baidu.com\">Baidu</a>");
		link.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			Link t = (Link)event.widget;
			resultText.append("Link text is ["+t.getText()+"]\n");
			resultText.append("Slected text is ["+event.text+"]\n");	//选中的hyperLink 
		}));
	}

	@Override
	public void createControlPanel(Composite parent) {
	}

}

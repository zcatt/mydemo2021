package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;


public class ListTab extends WidgetTab {
	Text resultText;

	public ListTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "List";
	}

	@Override
	public String getCategory() {
		return "Choose";
	}

	@Override
	public String getDescription() {
		return "show list";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("List example.\n");
		
		List list;
		list = new List(comp, SWT.MULTI|SWT.V_SCROLL);
		list.setItems(new String[] {
				"item 1",
				"item 2",
				"item 3",
				"item 4",
				"item 5",
				"item 6",
				"item 7",
				"item 8",
				"item 9",

				"item 10",				
				"item 11",
				"item 12",
				"item 13",
				"item 14",
				"item 15",
				"item 16",
				"item 17",
				"item 18",
				"item 19",
		});
		
		list.setLayoutData(new GridData(200, 100));
		
		list.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			List t = (List) event.widget;
			resultText.append("select "+t.getSelectionCount() +" items\n");
			for(String e: t.getSelection())
			{
				resultText.append("    "+e+"\n");				
			}
		}));

	}

	@Override
	public void createControlPanel(Composite parent) {
	}

}

package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ArmEvent;
import org.eclipse.swt.events.ArmListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class MenuTab extends WidgetTab {
	Text resultText;
	
	public Shell shell;
	
	Image copyImg;
	Image cutImg;
	Image pasteImg;
	Image deleteImg;
	

	public MenuTab(WidgetExample example) {
		super(example);
		shell = null;
		
		copyImg = WidgetExample.loadImage(example.display, "cmd_Copy.gif");
		cutImg = WidgetExample.loadImage(example.display, "cmd_Cut.gif");
		pasteImg = WidgetExample.loadImage(example.display, "cmd_Paste.gif");
		deleteImg = WidgetExample.loadImage(example.display, "cmd_Delete.gif");
		
	}

	@Override
	public void dispose() {
		if(shell != null)
		{
			shell.dispose();
			shell = null;
		}
		
		copyImg.dispose();
		cutImg.dispose();
		pasteImg.dispose();
		deleteImg.dispose();
		
	}

	@Override
	public String getText() {
		return "Menu";
	}

	@Override
	public String getCategory() {
		return "Menus";
	}

	@Override
	public String getDescription() {
		return "show menu";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Menu example.\n");

	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		GridLayout gridLayout;
		gridLayout = new GridLayout(3, true);
		comp.setLayout(gridLayout);

		Button btn;
		btn = new Button(comp, SWT.PUSH);
		btn.setText("create shell");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			if(shell != null)
				return;
			shell = new Shell(example.shell, SWT.SHELL_TRIM);
			shell.setLayout(new GridLayout());
			shell.setText("menu shell");
			shell.addDisposeListener(event2->{
				shell = null;
			});
			
			SelectionListener selectionListener = SelectionListener.widgetSelectedAdapter(event2->{
				MenuItem item = (MenuItem) event2.widget;
				resultText.append("menu item ["+item.getText()+"] selected.\n");				
			});
			ArmListener armListener = new ArmListener() {

				@Override
				public void widgetArmed(ArmEvent event2) {
					MenuItem item = (MenuItem) event2.widget;
					resultText.append("menu item ["+item.getText()+"] armed.\n");				
				}				
			};
			
			Label label;
			Menu menu;
			Menu subMenu;
			MenuItem menuItem;
			MenuItem menuItem2;
			
			menu = new Menu(shell, SWT.BAR);
			shell.setMenuBar(menu);
			
			menuItem = new MenuItem(menu, SWT.CASCADE);
			menuItem.setText("&File");
			menuItem.setAccelerator(SWT.MOD1 + 'F');
			menuItem.addSelectionListener(selectionListener);
			menuItem.addArmListener(armListener);

			subMenu = new Menu(shell, SWT.DROP_DOWN);
			menuItem.setMenu(subMenu);
			
			menuItem2 = new MenuItem(subMenu, SWT.PUSH);
			menuItem2.setText("&New\tCtrl+N");
			menuItem2.setAccelerator(SWT.MOD1 + 'N');
			menuItem2.addSelectionListener(selectionListener);
			menuItem2.addArmListener(armListener);

			menuItem2 = new MenuItem(subMenu, SWT.PUSH);
			menuItem2.setText("&Open\tCtrl+O");
			menuItem2.setAccelerator(SWT.MOD1 + 'O');
			menuItem2.addSelectionListener(selectionListener);
			menuItem2.addArmListener(armListener);

			menuItem2 = new MenuItem(subMenu, SWT.SEPARATOR);
			menuItem2 = new MenuItem(subMenu, SWT.PUSH);
			menuItem2.setText("&Exit");
			//menuItem2.setAccelerator(SWT.MOD1 + 'X');
			menuItem2.addSelectionListener(selectionListener);
			menuItem2.addArmListener(armListener);


			menuItem = new MenuItem(menu, SWT.CASCADE);
			menuItem.setText("&Edit");
			menuItem.setAccelerator(SWT.MOD1 + 'E');
			menuItem.addSelectionListener(selectionListener);
			menuItem.addArmListener(armListener);
			
			subMenu = new Menu(shell, SWT.DROP_DOWN);
			menuItem.setMenu(subMenu);

			menuItem2 = new MenuItem(subMenu, SWT.PUSH);
			menuItem2.setText("&Copy\tCtrl+C");
			menuItem2.setImage(copyImg);
			menuItem2.setAccelerator(SWT.MOD1 + 'C');
			menuItem2.addSelectionListener(selectionListener);
			menuItem2.addArmListener(armListener);

			menuItem2 = new MenuItem(subMenu, SWT.PUSH);
			menuItem2.setText("&Past\tCtrl+P");
			menuItem2.setImage(pasteImg);
			menuItem2.setAccelerator(SWT.MOD1 + 'P');
			menuItem2.addSelectionListener(selectionListener);
			menuItem2.addArmListener(armListener);

			menuItem2 = new MenuItem(subMenu, SWT.PUSH);
			menuItem2.setText("Cut\tCtrl+X");
			menuItem2.setImage(cutImg);
			menuItem2.setAccelerator(SWT.MOD1 + 'X');
			menuItem2.addSelectionListener(selectionListener);
			menuItem2.addArmListener(armListener);

			
			
			
			label= new Label(shell, SWT.BORDER);
			label.setText("click to popup menu");
			
			menu = new Menu(label);
			menuItem = new MenuItem(menu, SWT.PUSH);
			menuItem.setText("cmd 1");
			menuItem.addSelectionListener(selectionListener);
			menuItem.addArmListener(armListener);

			menuItem = new MenuItem(menu, SWT.PUSH);
			menuItem.setText("cmd 2");
			menuItem.addSelectionListener(selectionListener);
			menuItem.addArmListener(armListener);

		
			label.setMenu(menu);
			
			
			shell.setSize(400, 300);
			shell.open();
		}));
	}
	
	public Shell getShell()
	{
		return shell;
	}
	
	public void setShell(Shell shell)
	{
		if(this.shell != null)
			this.shell.dispose();
		this.shell = shell;
	}

}

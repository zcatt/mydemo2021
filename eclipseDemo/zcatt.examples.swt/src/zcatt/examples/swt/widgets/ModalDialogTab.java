package zcatt.examples.swt.widgets;

import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ModalDialogTab extends WidgetTab {

	Text resultText;
	
	public ModalDialogTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Modal dialog";
	}

	@Override
	public String getCategory() {
		return "Dialogs";
	}

	@Override
	public String getDescription() {
		return "custom implemented modal dialog";
	}

	@Override
	public void createWidget(Composite parent) {
		resultText = new Text(parent, SWT.MULTI | SWT.READ_ONLY);
		resultText.append("It seems that there is no difference between primary_modal and application_modal\n");
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		GridLayout gridLayout;
		gridLayout = new GridLayout(3, true);
		comp.setLayout(gridLayout);

		Button btn;
		
		btn = new Button(comp, SWT.PUSH);
		btn.setText("ApplicationModal Dialog");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			MyDialog diag = new MyDialog(example.shell, SWT.APPLICATION_MODAL);
			diag.setText("application_modal dialog");
			int res = diag.open();
			if(res == SWT.OK)
			{
				resultText.append("MyDialog OK\n");
			}
			else
			{
				resultText.append("MyDialog CANCEL.\n");
			}

		}));

		btn = new Button(comp, SWT.PUSH);
		btn.setText("PrimaryModal Dialog");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			MyDialog diag = new MyDialog(example.shell, SWT.PRIMARY_MODAL);
			diag.setText("primary_modal dialog");
			int res = diag.open();
			if(res == SWT.OK)
			{
				resultText.append("MyDialog OK\n");
			}
			else
			{
				resultText.append("MyDialog CANCEL.\n");
			}

		}));

	}

	
	public class MyDialog extends Dialog{
		int res;

		public MyDialog(Shell parent, int style) {
			super(parent, style);
			res = SWT.CANCEL;
		}
		
		public int getReturnCode()
		{
			return res;
		}
		
		public int open()
		{
			res = SWT.CANCEL;
			
			Shell parent = getParent();
            Display display = parent.getDisplay();
            
			int style = getStyle();
			assert (style & (SWT.APPLICATION_MODAL | SWT.PRIMARY_MODAL | SWT.SYSTEM_MODAL)) != 0;

            Shell shell = new Shell(parent, SWT.DIALOG_TRIM | style);
            shell.setText(getText());

            // Your code goes here (widget creation, set result, etc).
            shell.setLayout(new GridLayout(1, true));
            Label label = new Label(shell, SWT.NONE);
            label.setText("This is my dialog.");           
            Button btn = new Button(shell, SWT.PUSH);
            btn.setText("OK");
            btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
            	res = SWT.OK;
            	
            	Button button = (Button)event.widget;
            	button.getShell().dispose();
            }));
            
            Rectangle rect = display.getClientArea();
            shell.setLocation(rect.width/2, rect.height/2);

            shell.open();
            shell.pack();
            
            while (!shell.isDisposed()) {
                    if (!display.readAndDispatch()) display.sleep();
            }			
			
			return res;
		}
		
	}
}

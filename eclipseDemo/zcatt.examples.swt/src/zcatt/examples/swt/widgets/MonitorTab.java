package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Text;

public class MonitorTab extends WidgetTab {
	Text resultText;

	public MonitorTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Monitor";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show monitor";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Monitor example.\n");
		
		//两种方式取得monitor obj:
		//1. control.getMonitor()
		//2. display.getPrimaryMonitor()/getMonitors()
		Monitor[] monitors = parent.getDisplay().getMonitors();
		resultText.append("There are "+monitors.length +" monitors.\n");
		resultText.append("Cur monitor info, \n");
		
		Monitor monitor = resultText.getMonitor();
		resultText.append("    zoom: "+monitor.getZoom()+"\n");
		resultText.append("    clientArea: "+monitor.getClientArea()+"\n");
		resultText.append("    bounds: "+monitor.getBounds()+"\n");
		
		

	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

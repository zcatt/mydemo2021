package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.PopupList;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class PopupListTab extends WidgetTab {
	Text resultText;

	public PopupListTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "PopupList";
	}

	@Override
	public String getCategory() {
		return "Choose";
	}

	@Override
	public String getDescription() {
		return "show PopupList";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(2, true));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		resultText.append("TabFoler example.\n");
		
		Group grp;
		grp = new Group(comp, SWT.NONE);
		grp.setLayout(new GridLayout());
		grp.setText("PopupList demo\n");
		grp.setText("Mouse right down and select in popupList.\n");
		
		Canvas canvas;
		canvas = new Canvas(grp, SWT.NONE);
		canvas.setLayoutData(new GridData(400, 300));
		canvas.setBackground(new Color(255,255,0));
		canvas.addMouseListener(new MouseAdapter() {
			boolean reEntry = false;

			@Override
			public void mouseDown(MouseEvent e) {
				super.mouseDown(e);
				if(reEntry)
					return;
				
				reEntry = true;
				
				PopupList popupList = new PopupList(resultText.getShell(), SWT.NONE);
				
				String[] names = new String[] {
						"popupList item 1",
						"popupList item 2",
						"popupList item 3",
						"",
						"popupList item 4",
						"popupList item 5",
				};
				
				popupList.setItems(names);
				Shell shell = example.shell;
				Rectangle rect = new Rectangle(shell.getBounds().x, shell.getBounds().y, 300, 300);
				String res = popupList.open(rect);
				resultText.append(res == null ? "cancel popupList\n":("select text="+res+"\n"));
				
				reEntry = false;
			}			
		});

	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

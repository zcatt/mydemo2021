package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

public class ProgressBarTab extends WidgetTab {
	final static int MIN_VAL = 0;
	final static int MAX_VAL = 100;
	final static int SEL_VAL = 70;
	
	Text resultText;
	ProgressBar normalProgressBar;
	ProgressBar smoothProgressBar;
	ProgressBar indeterminateProgressBar;
	Spinner maxSpinner;		
	Spinner minSpinner;
	Spinner selSpinner;
	

	public ProgressBarTab(WidgetExample example) {
		super(example);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return "ProgressBar";
	}

	@Override
	public String getCategory() {
		// TODO Auto-generated method stub
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show progressBar";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("ProgressBar example.\n");
		
		Group grp;
		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		normalProgressBar = new ProgressBar(grp, SWT.HORIZONTAL);
		normalProgressBar.setMinimum(MIN_VAL);
		normalProgressBar.setMaximum(MAX_VAL);
		normalProgressBar.setSelection(SEL_VAL);
		normalProgressBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL|SMOOTH");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		smoothProgressBar = new ProgressBar(grp, SWT.HORIZONTAL|SWT.SMOOTH);
		smoothProgressBar.setMinimum(MIN_VAL);
		smoothProgressBar.setMaximum(MAX_VAL);
		smoothProgressBar.setSelection(SEL_VAL);
		smoothProgressBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL|INDETERMINATE");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		indeterminateProgressBar = new ProgressBar(grp, SWT.HORIZONTAL|SWT.INDETERMINATE);
		indeterminateProgressBar.setMinimum(MIN_VAL);
		indeterminateProgressBar.setMaximum(MAX_VAL);
		indeterminateProgressBar.setSelection(SEL_VAL);
		indeterminateProgressBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		

	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(3, false));
		
		Group grp;		
		
		Combo stateCombo;
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("max value");
		grp.setLayout(new GridLayout());
		maxSpinner = new Spinner(grp, SWT.BORDER);
		maxSpinner.setMaximum(10000);
		maxSpinner.setSelection(MAX_VAL);
		maxSpinner.setPageIncrement(100);
		maxSpinner.setIncrement(1);
		maxSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			normalProgressBar.setMaximum(maxSpinner.getSelection());
			smoothProgressBar.setMaximum(maxSpinner.getSelection());
			indeterminateProgressBar.setMaximum(maxSpinner.getSelection());
			updateSpinners();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("min value");
		grp.setLayout(new GridLayout());
		minSpinner = new Spinner(grp, SWT.BORDER);
		minSpinner.setMaximum(10000);
		minSpinner.setSelection(MIN_VAL);
		minSpinner.setPageIncrement(100);
		minSpinner.setIncrement(1);
		minSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			normalProgressBar.setMinimum(minSpinner.getSelection());
			smoothProgressBar.setMinimum(minSpinner.getSelection());
			indeterminateProgressBar.setMinimum(minSpinner.getSelection());
			updateSpinners();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("selection value");
		grp.setLayout(new GridLayout());
		selSpinner = new Spinner(grp, SWT.BORDER);
		selSpinner.setMaximum(10000);
		selSpinner.setSelection(SEL_VAL);
		selSpinner.setPageIncrement(100);
		selSpinner.setIncrement(1);
		selSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			normalProgressBar.setSelection(selSpinner.getSelection());
			smoothProgressBar.setSelection(selSpinner.getSelection());
			indeterminateProgressBar.setSelection(selSpinner.getSelection());
			updateSpinners();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("state value");
		grp.setLayout(new GridLayout());
		stateCombo = new Combo(grp, SWT.DROP_DOWN | SWT.READ_ONLY);
		stateCombo.setItems(new String[] {
				"SWT.NORMAL",
				"SWT.ERROR",
				"SWT.PAUSE",
		});
		stateCombo.select(0);
		stateCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int i = stateCombo.getSelectionIndex();
			int state;
			if(i == 0)
				state = SWT.NORMAL;
			else if (i == 1)
				state = SWT.ERROR;
			else
			{
				assert i == 2;
				state = SWT.PAUSE;
			}
			normalProgressBar.setState(state);
			smoothProgressBar.setState(state);
			indeterminateProgressBar.setState(state);
				
		}));
	}
	
	void updateSpinners()
	{
		if(maxSpinner.getSelection() != normalProgressBar.getMaximum())
			maxSpinner.setSelection(normalProgressBar.getMaximum());
		if(minSpinner.getSelection() != normalProgressBar.getMinimum())
			minSpinner.setSelection(normalProgressBar.getMinimum());
		if(selSpinner.getSelection() != normalProgressBar.getSelection())
			selSpinner.setSelection(normalProgressBar.getSelection());
	}

}

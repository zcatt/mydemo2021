package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

public class SashFormTab extends WidgetTab {
	Text resultText;

	static String[] listData = {
			"item 1",
			"item 2",
			"item 3",
			"item 4",
			"item 5",			
	};

	public SashFormTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "SashForm";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show SashForm";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		resultText.append("SashForm example.\n");
		
		Group grp;
		grp = new Group(comp, SWT.NONE);
		grp.setText("sashForm");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		SashForm sashFormH;
		SashForm sashFormV;
		
		sashFormH = new SashForm(grp, SWT.HORIZONTAL);
		sashFormH.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		List list = new List(sashFormH, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		list.setItems(listData);		
		
		sashFormV = new SashForm(sashFormH, SWT.VERTICAL);
		Text text1 = new Text(sashFormV, SWT.MULTI);
		text1.setText("top text\n");
		Text text2 = new Text(sashFormV, SWT.MULTI);
		text2.setText("bottom text\n");
		
		sashFormV.setWeights(new int[] {1,1});
		sashFormH.setWeights(new int[] {1,3});
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Text;

public class SashTab extends WidgetTab {
	final static int SASH_WIDTH = 5;
	final static int SASH_LIMIT = 20;
	
	Text resultText;
	Sash vSash;
	Sash hSash;
	Text leftText;
	Text rightText;

	public SashTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Sash";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show sash";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);

		//使用sash通常有两种layout方式, 
		//一种是使用container resizeListener以及处理sash selectionListener,直接设定各controls的bounds
		//一种是使用formLayout和sash selectionListener, 仅需调整sash的位置.
		//本tab使用第一种. 第二种在WidgetExample.java使用到. 
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.append("Sash example.\n");
		
		leftText = new Text(comp, SWT.MULTI|SWT.BORDER);
		leftText.append("left\ntext\n");
		
		rightText = new Text(comp, SWT.MULTI|SWT.BORDER);
		rightText.append("right\ntext\n");
		
		hSash = new Sash(comp, SWT.HORIZONTAL);
		hSash.setBackground(hSash.getDisplay().getSystemColor(SWT.COLOR_GREEN));
		vSash = new Sash(comp, SWT.VERTICAL );
		vSash.setBackground(hSash.getDisplay().getSystemColor(SWT.COLOR_GREEN));
		
		//根据comp parent调整各control的bounds
		comp.addControlListener(ControlListener.controlResizedAdapter(event -> resized(event)));
		//根据sash调整各control的bounds
		hSash.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> adjustHSash(event)));
		vSash.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> adjustVSash(event)));

	}

	@Override
	public void createControlPanel(Composite parent) {
	}

	void resized(ControlEvent event)
	{
		//每次comp尺寸调整时, 都将重新初始化布局.
		
		Composite comp = (Composite) event.widget;
		Rectangle rect = comp.getClientArea();
		int x = (rect.width - SASH_WIDTH)/2;
		int y = (rect.height - SASH_WIDTH)/2;
		
		layout(rect.width, rect.height, x, y);	
	}
	
	void adjustHSash(SelectionEvent event)
	{
		if(event.detail != SWT.DRAG)			//仅处理drop时刻
		{
			Composite comp = hSash.getParent();
			Rectangle rect = comp.getClientArea();
			Rectangle leftTextRect = leftText.getBounds();
			int x = leftTextRect.width;
			int y = Math.max(Math.min(event.y, rect.height - SASH_LIMIT), SASH_LIMIT);
			
			layout(rect.width, rect.height, x, y);	
		}
	}
	void adjustVSash(SelectionEvent event)
	{
		Composite comp = hSash.getParent();
		Rectangle rect = comp.getClientArea();
		Rectangle leftTextRect = leftText.getBounds();
		int x = Math.max(Math.min(event.x, rect.width - SASH_LIMIT), SASH_LIMIT);
		int y = leftTextRect.y - SASH_WIDTH;
		
		layout(rect.width, rect.height, x, y);		//drag/drop都处理
	}
	
	void layout(int clientWidth, int clientHeight, int x, int y)
	{
		resultText.setBounds(0, 0, clientWidth, y);
		leftText.setBounds(0, y+SASH_WIDTH, x, clientHeight - y - SASH_WIDTH);
		rightText.setBounds(x+SASH_WIDTH, y+SASH_WIDTH
				, clientWidth - x - SASH_WIDTH, clientHeight - y - SASH_WIDTH);
		hSash.setBounds(0, y, clientWidth, SASH_WIDTH);
		vSash.setBounds(x, y+SASH_WIDTH, SASH_WIDTH, clientHeight - y - SASH_WIDTH);		
	}
}

package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

public class ScaleTab extends WidgetTab {
	Text resultText;
	Scale scale;
	Spinner maxSpinner;		
	Spinner minSpinner;
	Spinner selSpinner;
	Spinner pageSpinner;
	Spinner stepSpinner;
	

	public ScaleTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Scale";
	}

	@Override
	public String getCategory() {
		return "Value";
	}

	@Override
	public String getDescription() {
		return "show scale";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Scale example.\n");
		
		Group grp;
		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		scale = new Scale(grp, SWT.HORIZONTAL);
		scale.setMinimum(0);
		scale.setMaximum(100);
		scale.setPageIncrement(10);
		scale.setIncrement(1);
		scale.setSelection(30);
		scale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		scale.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			updateScale();
		}));
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(3, false));
		
		Group grp;		
		
		Combo stateCombo;
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("max value");
		grp.setLayout(new GridLayout());
		maxSpinner = new Spinner(grp, SWT.BORDER);
		maxSpinner.setMaximum(10000);
		maxSpinner.setSelection(100);
		maxSpinner.setPageIncrement(100);
		maxSpinner.setIncrement(1);
		maxSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			scale.setMaximum(maxSpinner.getSelection());
			updateScale();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("min value");
		grp.setLayout(new GridLayout());
		minSpinner = new Spinner(grp, SWT.BORDER);
		minSpinner.setMaximum(10000);
		minSpinner.setSelection(0);
		minSpinner.setPageIncrement(10);
		minSpinner.setIncrement(1);
		minSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			scale.setMinimum(minSpinner.getSelection());
			updateScale();
		}));		

		grp = new Group(comp, SWT.NONE);
		grp.setText("page value");
		grp.setLayout(new GridLayout());
		pageSpinner = new Spinner(grp, SWT.BORDER);
		pageSpinner.setMaximum(10000);
		pageSpinner.setSelection(10);
		pageSpinner.setPageIncrement(10);
		pageSpinner.setIncrement(1);
		pageSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			scale.setPageIncrement(pageSpinner.getSelection());
			updateScale();
		}));		
		grp = new Group(comp, SWT.NONE);
		grp.setText("step value");
		grp.setLayout(new GridLayout());
		stepSpinner = new Spinner(grp, SWT.BORDER);
		stepSpinner.setMaximum(10000);
		stepSpinner.setSelection(1);
		stepSpinner.setPageIncrement(10);
		stepSpinner.setIncrement(1);
		stepSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			scale.setIncrement(stepSpinner.getSelection());
			updateScale();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("selection value");
		grp.setLayout(new GridLayout());
		selSpinner = new Spinner(grp, SWT.BORDER);
		selSpinner.setMaximum(10000);
		selSpinner.setSelection(30);
		selSpinner.setPageIncrement(100);
		selSpinner.setIncrement(1);
		selSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			scale.setSelection(selSpinner.getSelection());
			updateScale();
		}));		

	}
	
	void updateScale()
	{
		if(maxSpinner.getSelection() != scale.getMaximum())
			maxSpinner.setSelection(scale.getMaximum());
		if(minSpinner.getSelection() != scale.getMinimum())
			minSpinner.setSelection(scale.getMinimum());
		if(pageSpinner.getSelection() != scale.getPageIncrement())
			pageSpinner.setSelection(scale.getPageIncrement());
		if(stepSpinner.getSelection() != scale.getIncrement())
			stepSpinner.setSelection(scale.getIncrement());
		if(selSpinner.getSelection() != scale.getSelection())
			selSpinner.setSelection(scale.getSelection());
	}
	

}

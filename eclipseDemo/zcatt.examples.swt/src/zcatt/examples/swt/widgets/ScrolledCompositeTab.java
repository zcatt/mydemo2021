package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.internal.gdip.Rect;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

public class ScrolledCompositeTab extends WidgetTab {
	Text resultText;
	ScrolledComposite sc1;
	ScrolledComposite sc2;
	Canvas canvas1;
	Canvas canvas2;
	
	

	public ScrolledCompositeTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "ScrolledCompositeTab";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show ScrolledCompositeTab";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(2, false));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		resultText.append("ScrolledComposite example.\n");
		resultText.append("两种方式使用scrollBar.\n");
		
		Group grp;
		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("method 1");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		sc1 = new ScrolledComposite(grp, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		sc1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		canvas1 = new Canvas(sc1, SWT.NONE);
		canvas1.setBackground(new Color(255, 0, 0));
		canvas1.setBounds(0, 0, 100, 100);
		canvas1.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				Rectangle rect = canvas1.getBounds();
				GC gc = e.gc;
				gc.drawLine(rect.x, rect.y, rect.x+rect.width, rect.y + rect.height);
				gc.drawLine(rect.x, rect.y+rect.height, rect.x+rect.width, rect.y);
			}
			
		});
		canvas1.addControlListener(new ControlListener() {

			@Override
			public void controlMoved(ControlEvent e) {
				resultText.append("redraw1.\n");
				//canvas1.update();				
				canvas1.redraw();				
			}

			@Override
			public void controlResized(ControlEvent e) {
				resultText.append("redraw2.\n");
				//canvas1.update();				
				canvas1.redraw();				
			}
			
		});
		sc1.setContent(canvas1);
		
		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("method 2");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		sc2 = new ScrolledComposite(grp, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		sc2.setExpandHorizontal(true);
		sc2.setExpandHorizontal(true);
		sc2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		canvas2 = new Canvas(sc2, SWT.NONE);
		canvas2.setBackground(new Color(255, 255, 0));
		canvas2.setBounds(0, 0, 200, 200);
		canvas2.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				Rectangle rect = canvas2.getBounds();
				GC gc = e.gc;
				gc.drawLine(rect.x, rect.y, rect.x+rect.width, rect.y + rect.height);
				gc.drawLine(rect.x, rect.y+rect.height, rect.x+rect.width, rect.y);
				gc.drawRectangle(0, 0, 199, 199);
			}
			
		});
		canvas2.addControlListener(new ControlListener() {

			@Override
			public void controlMoved(ControlEvent e) {
				canvas2.redraw();				
			}

			@Override
			public void controlResized(ControlEvent e) {
				canvas2.redraw();				
			}
			
		});
		
		sc2.setContent(canvas2);
		sc2.setMinSize(200, 200);
		
		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("adjust canvas size");
		grp.setLayout(new RowLayout(SWT.HORIZONTAL));
		Button btn;
		btn = new Button(grp, SWT.PUSH);
		btn.setText("+");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			Rectangle rect = canvas1.getBounds();
			rect.width +=100;
			rect.height +=100;
			resultText.append("adjust canvas to w="+rect.width+", h="+rect.height+"\n");
			canvas1.setBounds(rect);			
		}));

		btn = new Button(grp, SWT.PUSH);
		btn.setText("-");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			Rectangle rect = canvas1.getBounds();
			rect.width -=100;
			rect.height -=100;
			if(rect.width > 0 && rect.height > 0)
			{
				resultText.append("adjust canvas to w="+rect.width+", h="+rect.height+"\n");
				canvas1.setBounds(rect);
			}
		}));

		
		grp = new Group(comp, SWT.NONE);
		grp.setText("adjust scrolledComposite min size");
		grp.setLayout(new RowLayout(SWT.HORIZONTAL));
		btn = new Button(grp, SWT.PUSH);
		btn.setText("+");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int minWidth = sc2.getMinWidth() +100;
			int minHeight = sc2.getMinHeight() +100;
			
			resultText.append("adjust minSize to w="+minWidth+", h="+minHeight+"\n");
			sc2.setMinSize(minWidth, minHeight);			
			sc2.requestLayout();
		}));

		btn = new Button(grp, SWT.PUSH);
		btn.setText("-");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int minWidth = sc2.getMinWidth() -100;
			int minHeight = sc2.getMinHeight() -100;
			
			if(minWidth > 0 && minHeight > 0)
			{
				resultText.append("adjust minSize to w="+minWidth+", h="+minHeight+"\n");
				sc2.setMinSize(minWidth, minHeight);
				sc2.requestLayout();
			}
		}));
		

	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

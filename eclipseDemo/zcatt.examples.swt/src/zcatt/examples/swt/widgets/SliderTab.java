package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

public class SliderTab extends WidgetTab {
	Text resultText;
	Slider slider;
	Spinner maxSpinner;		
	Spinner minSpinner;
	Spinner selSpinner;
	Spinner pageSpinner;
	Spinner stepSpinner;
	Spinner thumbSpinner;
	

	public SliderTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Slider";
	}

	@Override
	public String getCategory() {
		return "Value";
	}

	@Override
	public String getDescription() {
		return "show Slider";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Slider example.\n");
		
		Group grp;
		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		slider = new Slider(grp, SWT.HORIZONTAL);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setPageIncrement(10);
		slider.setIncrement(1);
		slider.setThumb(10);
		slider.setSelection(30);
		slider.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		slider.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			updateSlider();
		}));
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(3, false));
		
		Group grp;		
		
		Combo stateCombo;
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("max value");
		grp.setLayout(new GridLayout());
		maxSpinner = new Spinner(grp, SWT.BORDER);
		maxSpinner.setMaximum(10000);
		maxSpinner.setSelection(100);
		maxSpinner.setPageIncrement(100);
		maxSpinner.setIncrement(1);
		maxSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			slider.setMaximum(maxSpinner.getSelection());
			updateSlider();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("min value");
		grp.setLayout(new GridLayout());
		minSpinner = new Spinner(grp, SWT.BORDER);
		minSpinner.setMaximum(10000);
		minSpinner.setSelection(0);
		minSpinner.setPageIncrement(10);
		minSpinner.setIncrement(1);
		minSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			slider.setMinimum(minSpinner.getSelection());
			updateSlider();
		}));		

		grp = new Group(comp, SWT.NONE);
		grp.setText("page value");
		grp.setLayout(new GridLayout());
		pageSpinner = new Spinner(grp, SWT.BORDER);
		pageSpinner.setMaximum(10000);
		pageSpinner.setSelection(10);
		pageSpinner.setPageIncrement(10);
		pageSpinner.setIncrement(1);
		pageSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			slider.setPageIncrement(pageSpinner.getSelection());
			updateSlider();
		}));		
		grp = new Group(comp, SWT.NONE);
		grp.setText("step value");
		grp.setLayout(new GridLayout());
		stepSpinner = new Spinner(grp, SWT.BORDER);
		stepSpinner.setMaximum(10000);
		stepSpinner.setSelection(1);
		stepSpinner.setPageIncrement(10);
		stepSpinner.setIncrement(1);
		stepSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			slider.setIncrement(stepSpinner.getSelection());
			updateSlider();
		}));		
		grp = new Group(comp, SWT.NONE);
		grp.setText("thumb value");
		grp.setLayout(new GridLayout());
		thumbSpinner = new Spinner(grp, SWT.BORDER);
		thumbSpinner.setMaximum(10000);
		thumbSpinner.setSelection(10);
		thumbSpinner.setPageIncrement(10);
		thumbSpinner.setIncrement(1);
		thumbSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			slider.setThumb(thumbSpinner.getSelection());
			updateSlider();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("selection value");
		grp.setLayout(new GridLayout());
		selSpinner = new Spinner(grp, SWT.BORDER);
		selSpinner.setMaximum(10000);
		selSpinner.setSelection(30);
		selSpinner.setPageIncrement(100);
		selSpinner.setIncrement(1);
		selSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			slider.setSelection(selSpinner.getSelection());
			updateSlider();
		}));		

	}

	void updateSlider()
	{
		if(maxSpinner.getSelection() != slider.getMaximum())
			maxSpinner.setSelection(slider.getMaximum());
		if(minSpinner.getSelection() != slider.getMinimum())
			minSpinner.setSelection(slider.getMinimum());
		if(pageSpinner.getSelection() != slider.getPageIncrement())
			pageSpinner.setSelection(slider.getPageIncrement());
		if(stepSpinner.getSelection() != slider.getIncrement())
			stepSpinner.setSelection(slider.getIncrement());
		if(thumbSpinner.getSelection() != slider.getThumb())
			thumbSpinner.setSelection(slider.getThumb());
		if(selSpinner.getSelection() != slider.getSelection())
			selSpinner.setSelection(slider.getSelection());
	}
	
}

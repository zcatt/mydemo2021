package zcatt.examples.swt.widgets;

import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;


import javax.swing.filechooser.FileSystemView;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;

public class SpecialDialogsTab extends WidgetTab {


	Text resultText;
	
	public SpecialDialogsTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "SpecialDialog";
	}

	@Override
	public String getCategory() {
		return "Dialogs";
	}

	@Override
	public String getDescription() {
		return "show special dialogs";
	}

	@Override
	public void createWidget(Composite parent) {
		resultText = new Text(parent, SWT.MULTI | SWT.READ_ONLY);
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		GridLayout gridLayout;
		gridLayout = new GridLayout(3, true);
		comp.setLayout(gridLayout);

		Button btn;
		
		btn = new Button(comp, SWT.PUSH);
		btn.setText("ColorDialog");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			//Button btnColor = (Button) event.widget;

			ColorDialog diag = new ColorDialog(example.shell);
			RGB rgb = new RGB(255,0,0);

			diag.setRGB(rgb);
			rgb = diag.open();
			if(rgb != null)
			{
				resultText.append("select color = "+rgb+"\n");
				//resultText.setBackground(new Color(rgb));				
			}
			else
			{
				resultText.append("ColorDialog cancel.\n");
			}

		}));
		
		btn = new Button(comp, SWT.PUSH);
		btn.setText("FontDialog");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			//Button btnColor = (Button) event.widget;

			FontDialog diag = new FontDialog(example.shell);
			//diag.setEffectsVisible(true);
			diag.setRGB(new RGB(255,0,0));
			diag.setFontList(Display.getDefault().getSystemFont().getFontData());
			
			if(diag.open() != null)
			{
				FontData[] fds = diag.getFontList();
				resultText.append("select "+ fds.length+" fonts\n");
				int i = 0;
				for(FontData e: fds)
				{
					i++;
					resultText.append(""+i+": "+ e.getName() + ", " 
							+ e.getStyle() +", "
							+ e.getHeight()+"\n");					
				}				
			}
			else
			{
				resultText.append("FontDialog cancel.\n");
			}

		}));

		btn = new Button(comp, SWT.PUSH);
		btn.setText("FileDialog(save)");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			//Button btnColor = (Button) event.widget;

			FileDialog diag = new FileDialog(example.shell, SWT.SAVE);
			diag.setFileName("reame.txt");
			diag.setFilterExtensions(new String[]{"*.txt", "*.md"});
			diag.setFilterNames(new String[] {"text", "markdown"});
			diag.setOverwrite(true);		//提示overwrite.
			diag.setFilterPath(".");
			
			String absPath = diag.open(); 
			if(absPath != null)
			{
				resultText.append("SAVETO "+ diag.getFileName()+" \n");
				resultText.append("ABSPATH "+ absPath+"\n");				
			}
			else
			{
				resultText.append("FileDialog cancel.\n");
			}

		}));

		btn = new Button(comp, SWT.PUSH);
		btn.setText("FileDialog(open|multi)");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			//Button btnColor = (Button) event.widget;

			FileDialog diag = new FileDialog(example.shell, SWT.OPEN|SWT.MULTI);
			diag.setFileName("reame.txt");
			diag.setFilterExtensions(new String[]{"*.txt", "*.md", "*.*"});
			diag.setFilterNames(new String[] {"text", "markdown", "all"});
			diag.setOverwrite(true);		//提示overwrite.
			diag.setFilterPath(".");
			
			if(diag.open() != null)
			{
				String[] fns = diag.getFileNames();
				resultText.append("Open "+ fns.length+" files\n");
				for(int i = 0; i<fns.length; i++)
				{
					resultText.append(""+ i + ": "+ fns[i] + "\n");					
				}
			}
			else
			{
				resultText.append("FileDialog cancel.\n");
			}

		}));

		btn = new Button(comp, SWT.PUSH);
		btn.setText("PrintDialog");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			//Button btnColor = (Button) event.widget;

			PrintDialog diag = new PrintDialog(example.shell, SWT.NONE);
			
			if(diag.open() != null)
			{
				PrinterData pd = diag.getPrinterData();
				resultText.append("Print to "+ pd+" files\n");
			}
			else
			{
				resultText.append("PrintDialog cancel.\n");
			}

		}));

		btn = new Button(comp, SWT.PUSH);
		btn.setText("DirectoryDialog");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			//Button btnColor = (Button) event.widget;

			DirectoryDialog diag = new DirectoryDialog(example.shell, SWT.NONE);
			
			//java.io.File homeDir = FileSystemView.getFileSystemView().getHomeDirectory();
			//diag.setFilterPath(homeDir.getPath());
			diag.setFilterPath(System.getProperty("user.home"));
			String res = diag.open();	//directotryDialog没有提供获取结果的method, 只能保存open()返回值.
			
			if(res != null)
			{
				resultText.append("Select directory of "+ res+"\n");
			}
			else
			{
				resultText.append("PrintDialog cancel.\n");
			}

		}));

		btn = new Button(comp, SWT.PUSH);
		btn.setText("DirectoryDialog");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			//Button btnColor = (Button) event.widget;

			DirectoryDialog diag = new DirectoryDialog(example.shell, SWT.NONE);
			
			//java.io.File homeDir = FileSystemView.getFileSystemView().getHomeDirectory();
			//diag.setFilterPath(homeDir.getPath());
			diag.setFilterPath(System.getProperty("user.home"));
			String res = diag.open();	//directotryDialog没有提供获取结果的method, 只能保存open()返回值.
			
			if(res != null)
			{
				resultText.append("Select directory of "+ res+"\n");
			}
			else
			{
				resultText.append("PrintDialog cancel.\n");
			}

		}));
		
		btn = new Button(comp, SWT.PUSH);
		btn.setText("MessageBox");
		btn.addSelectionListener(widgetSelectedAdapter(event -> {
			//Button btnColor = (Button) event.widget;

			MessageBox mb = new MessageBox(example.shell, SWT.ICON_WARNING|SWT.OK|SWT.CANCEL);
			mb.setText("message box");
			mb.setMessage("Hello, this is a message box.");
			int res = mb.open();
			if(res == SWT.OK)
			{
				resultText.append("OK is clicked\n");
				
			}
			else if(res == SWT.CANCEL)
			{
				resultText.append("CANCEL is clicked\n");				
			}
		}));
	}

}

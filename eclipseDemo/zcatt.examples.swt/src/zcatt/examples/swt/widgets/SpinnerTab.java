package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

public class SpinnerTab extends WidgetTab {
	Text resultText;
	Spinner spinner;
	Spinner maxSpinner;		
	Spinner minSpinner;
	Spinner selSpinner;
	Spinner pageSpinner;
	Spinner stepSpinner;

	public SpinnerTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Spinner";
	}

	@Override
	public String getCategory() {
		return "Value";
	}

	@Override
	public String getDescription() {
		return "show Spinner";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Scale example.\n");
		
		Group grp;
		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL");
		grp.setLayout(new GridLayout());
		//grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		spinner = new Spinner(grp, SWT.HORIZONTAL);
		spinner.setMinimum(0);
		spinner.setMaximum(100);
		spinner.setPageIncrement(10);
		spinner.setIncrement(1);
		spinner.setSelection(30);
		spinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		spinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			updateSpinner();
			resultText.append("value "+spinner.getSelection()+"\n");
		}));
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(3, false));
		
		Group grp;		
		
		Combo stateCombo;
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("max value");
		grp.setLayout(new GridLayout());
		maxSpinner = new Spinner(grp, SWT.BORDER);
		maxSpinner.setMaximum(10000);
		maxSpinner.setSelection(100);
		maxSpinner.setPageIncrement(100);
		maxSpinner.setIncrement(1);
		maxSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			spinner.setMaximum(maxSpinner.getSelection());
			updateSpinner();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("min value");
		grp.setLayout(new GridLayout());
		minSpinner = new Spinner(grp, SWT.BORDER);
		minSpinner.setMaximum(10000);
		minSpinner.setSelection(0);
		minSpinner.setPageIncrement(10);
		minSpinner.setIncrement(1);
		minSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			spinner.setMinimum(minSpinner.getSelection());
			updateSpinner();
		}));		

		grp = new Group(comp, SWT.NONE);
		grp.setText("page value");
		grp.setLayout(new GridLayout());
		pageSpinner = new Spinner(grp, SWT.BORDER);
		pageSpinner.setMaximum(10000);
		pageSpinner.setSelection(10);
		pageSpinner.setPageIncrement(10);
		pageSpinner.setIncrement(1);
		pageSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			spinner.setPageIncrement(pageSpinner.getSelection());
			updateSpinner();
		}));		
		grp = new Group(comp, SWT.NONE);
		grp.setText("step value");
		grp.setLayout(new GridLayout());
		stepSpinner = new Spinner(grp, SWT.BORDER);
		stepSpinner.setMaximum(10000);
		stepSpinner.setSelection(1);
		stepSpinner.setPageIncrement(10);
		stepSpinner.setIncrement(1);
		stepSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			spinner.setIncrement(stepSpinner.getSelection());
			updateSpinner();
		}));		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("selection value");
		grp.setLayout(new GridLayout());
		selSpinner = new Spinner(grp, SWT.BORDER);
		selSpinner.setMaximum(10000);
		selSpinner.setSelection(30);
		selSpinner.setPageIncrement(100);
		selSpinner.setIncrement(1);
		selSpinner.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			spinner.setSelection(selSpinner.getSelection());
			updateSpinner();
		}));		
	}

	void updateSpinner()
	{
		if(maxSpinner.getSelection() != spinner.getMaximum())
			maxSpinner.setSelection(spinner.getMaximum());
		if(minSpinner.getSelection() != spinner.getMinimum())
			minSpinner.setSelection(spinner.getMinimum());
		if(pageSpinner.getSelection() != spinner.getPageIncrement())
			pageSpinner.setSelection(spinner.getPageIncrement());
		if(stepSpinner.getSelection() != spinner.getIncrement())
			stepSpinner.setSelection(spinner.getIncrement());
		if(selSpinner.getSelection() != spinner.getSelection())
			selSpinner.setSelection(spinner.getSelection());
	}
	
}

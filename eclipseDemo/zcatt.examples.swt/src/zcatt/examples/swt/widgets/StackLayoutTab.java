package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

public class StackLayoutTab extends WidgetTab {
	Text resultText;
	Canvas[] canvasArray;
	Group container;
	StackLayout slayout;
	
	Button btn0;
	Button btn1;
	Button btn2;
	Button btn3;
	Button btn4;

	public StackLayoutTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "StackLayout";
	}

	@Override
	public String getCategory() {
		return "Layout";
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(2, false));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		resultText.append("StackLayout example.\n");
		
		Group grp;		
		
		grp = new Group(comp, SWT.NONE);
		container = grp;
		grp.setText("use stackLayout");
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		slayout = new StackLayout();
		grp.setLayout(slayout);
		
		canvasArray = new Canvas[5];
		
		canvasArray[0] = new Canvas(grp, SWT.BORDER);
		canvasArray[0].setBackground(new Color(255, 0, 0));
		
		canvasArray[1] = new Canvas(grp, SWT.BORDER);
		canvasArray[1].setBackground(new Color(0, 255, 0));
		
		canvasArray[2] = new Canvas(grp, SWT.BORDER);
		canvasArray[2].setBackground(new Color(0, 0, 255));
		
		canvasArray[3] = new Canvas(grp, SWT.BORDER);
		canvasArray[3].setBackground(new Color(255, 255, 0));
		
		canvasArray[4] = new Canvas(grp, SWT.BORDER);
		canvasArray[4].setBackground(new Color(255, 0, 255));
		

		grp = new Group(comp, SWT.NONE);
		grp.setText("bring to top");
		grp.setLayout(new GridLayout());
		//grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		btn0 = new Button(grp, SWT.RADIO);
		btn0.setText("red");
		btn0.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			if(btn0.getSelection())
			{
				slayout.topControl = canvasArray[0];
				container.layout();
			}
		}));
		
		
		btn1 = new Button(grp, SWT.RADIO);
		btn1.setText("green");
		btn1.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			if(btn1.getSelection())
			{
				slayout.topControl = canvasArray[1];
				container.layout();
			}
		}));
		
		btn2 = new Button(grp, SWT.RADIO);
		btn2.setText("blue");
		btn2.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			if(btn2.getSelection())
			{
				slayout.topControl = canvasArray[2];
				container.layout();
			}
		}));
		
		btn3 = new Button(grp, SWT.RADIO);
		btn3.setText("yellow");
		btn3.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			if(btn3.getSelection())
			{
				slayout.topControl = canvasArray[3];
				container.layout();
			}
		}));
		
		btn4 = new Button(grp, SWT.RADIO);
		btn4.setText("purple");
		btn4.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			if(btn4.getSelection())
			{
				slayout.topControl = canvasArray[4];
				container.layout();
			}
		}));
		
		btn0.setSelection(true);
		slayout.topControl = canvasArray[0];
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

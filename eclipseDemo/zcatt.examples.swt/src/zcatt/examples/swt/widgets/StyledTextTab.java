package zcatt.examples.swt.widgets;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.Bullet;
import org.eclipse.swt.custom.LineStyleEvent;
import org.eclipse.swt.custom.LineStyleListener;
import org.eclipse.swt.custom.ST;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GlyphMetrics;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

public class StyledTextTab extends WidgetTab {
	Text resultText;
	StyledText styledText;
	Text keywordText;
	
	Color keywordFgColor = new Color(255,255,0);
	Color keywordBgColor = new Color(0,0,255);
	String keyword = "linux";
	String content = 
			"Welcome to Linux.org's \n"
			+"\"Getting Started with Linux: Beginner Level Course\". If you're new to Linux and want to find out how to use the fastest growing operating system today, all you have to do is follow these lessons and you'll be using Linux efficiently in no time.\n"
			+ "Getting Started with Linux: Beginner Level Course is designed as a self-study course. One of the things that makes this course unique is that at any point during a course, you can add a note, or comment. \n"
			+ "This is done in the comments bar on the right hand side of the screen. These comments can be made public or private, and can take any form that you like. Feel free to use them to ask questions, answer other users questions, post code updates, or suggest different methodologies for solving problems.";

	public StyledTextTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "StyleText";
	}

	@Override
	public String getCategory() {
		return "Texts";
	}

	@Override
	public String getDescription() {
		return "show StyledText";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("StyledText example.\n");
		
		Group grp;		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("StyledText");
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));		
		grp.setLayout(new FillLayout());
		
		styledText = new StyledText(grp, SWT.MULTI|SWT.WRAP| SWT.V_SCROLL | SWT.H_SCROLL);
		styledText.addLineStyleListener(new KeywordStyleListener());
		styledText.setBackground(new Color(255, 255, 200));

		// Guard against superfluous mouse move events -- defer action until later
		example.display.asyncExec(() -> styledText.setText(content));
		//styledText.setText(content);
		
		//doHighlight();
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new RowLayout());
		
		keywordText = new Text(comp, SWT.BORDER|SWT.SINGLE);
		keywordText.setLayoutData(new RowData(150, SWT.DEFAULT));
		keywordText.setText(keyword);
		keywordText.setTextLimit(20);
		
		Button highlightBtn = new Button(comp, SWT.PUSH);
		highlightBtn.setText("highlight");
		highlightBtn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			doHighlight();
		}));			
	}
	
	void doHighlight()
	{
		keyword = keywordText.getText().toLowerCase().strip();
		styledText.redraw();		
	}
	
	class KeywordStyleListener implements LineStyleListener
	{
		@Override
		public void lineGetStyle(LineStyleEvent event) {			//渲染 文本
			int lineIndex = styledText.getLineAtOffset(event.lineOffset);
			String lineText = event.lineText.toLowerCase();
			List<StyleRange> styles = new ArrayList<>();
			
			System.out.println("line "+event.lineOffset +"="+lineText);
			int offset = -1;
			
			while(true)
			{
				offset = lineText.indexOf(keyword, offset);
				if(offset < 0)
					break;
				
				StyleRange style = new StyleRange(offset + event.lineOffset, keyword.length(), keywordFgColor, keywordBgColor);
				style.fontStyle = SWT.BOLD;
				styles.add(style);
				
				offset += keyword.length();				
			}
			
			event.styles = styles.toArray(new StyleRange[styles.size()]);
			System.out.println("line indent= "+event.indent);
			event.indent = 20;		//行缩进
			event.wrapIndent = 0;	//折行缩进
			
			//line bullet, 即行首的符号
			if(lineIndex %2 == 0)
			{
				StyleRange style = new StyleRange();
				style.metrics = new GlyphMetrics(0, 0, 100);
				style.foreground = example.display.getSystemColor(SWT.COLOR_RED);
				
				Bullet bullet;
				bullet = new Bullet(ST.BULLET_NUMBER | ST.BULLET_TEXT, style);
				//bullet = new Bullet(ST.BULLET_LETTER_LOWER | ST.BULLET_TEXT, style);				
				bullet.text = ".";
				event.bullet = bullet;
				event.bulletIndex = lineIndex;
				
			}
			
		}		
	}

}

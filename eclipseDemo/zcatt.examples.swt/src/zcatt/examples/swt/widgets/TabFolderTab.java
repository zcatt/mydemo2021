package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

public class TabFolderTab extends WidgetTab {
	Text resultText;
	TabFolder topTabFolder;
	TabFolder bottomTabFolder;
	
	Image img;

	public TabFolderTab(WidgetExample example) {
		super(example);
		img = WidgetExample.loadImage(example.display, "pattern1.jpg");
	}

	@Override
	public void dispose() {
		if(img != null)
		{
			img.dispose();
			img = null;
		}
	}

	@Override
	public String getText() {
		return "TabFolder";
	}

	@Override
	public String getCategory() {
		return "Choose";
	}

	@Override
	public String getDescription() {
		return "show TabFolder";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(2, true));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		resultText.append("TabFoler example.\n");
		
		String[] names = new String[] {
				"item 1",
				"item 2",
				"item 3",
				"item 4",
		};
		
		topTabFolder = new TabFolder(comp, SWT.TOP);
		bottomTabFolder = new TabFolder(comp, SWT.BOTTOM);
		
		TabItem item;
		for(String e: names)
		{
			item = new TabItem(topTabFolder, SWT.NONE);
			item.setText(e);
			item.setImage(img);
			item.setToolTipText("show "+e);			
			
			item = new TabItem(bottomTabFolder, SWT.NONE);
			item.setText(e);
			//item.setImage(img);
			item.setToolTipText("show "+e);						
		}
		
		topTabFolder.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			TabItem e = (TabItem) event.item;
			resultText.append("select "+e.getText()+"\n");
		}));		
	}

	@Override
	public void createControlPanel(Composite parent) {
	}

}

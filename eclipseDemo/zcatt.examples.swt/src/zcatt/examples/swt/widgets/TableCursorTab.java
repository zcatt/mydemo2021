package zcatt.examples.swt.widgets;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class TableCursorTab extends WidgetTab {
	Text resultText;
	Table table;
	TableCursor cursor;
	ControlEditor editor;
	
	public TableCursorTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "TableCursor";
	}

	@Override
	public String getCategory() {
		return "Tables";
	}

	@Override
	public String getDescription() {
		return "show TableCursor";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("TableCursor example.\n");
		
		Group grp;		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("table");
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));		
		grp.setLayout(new GridLayout());
		
		table = new Table(grp, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
		
		table.setLayoutData(new GridData(GridData.FILL_BOTH));
		TableColumn column1 = new TableColumn(table, SWT.NONE);
		TableColumn column2 = new TableColumn(table, SWT.NONE);
		TableColumn column3 = new TableColumn(table, SWT.NONE);
		for (int i = 0; i < 100; i++) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(new String[] {"cell " + i + " 0", 	"cell " + i + " 1", "cell " + i + " 2" });
		}
		column1.pack();
		column2.pack();
		column3.pack();
		
		cursor = new TableCursor(table, SWT.NONE);
		
		editor = new ControlEditor(cursor);
		editor.grabHorizontal = true;
		editor.grabVertical = true;
		
		cursor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				table.setSelection(new TableItem[] { cursor.getRow()});
			}
			
			// "enter"时创建tableEditor, 其收到"enter", "esc"时自动退出tableEditor
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				final Text text = new Text(cursor, SWT.NONE);
				TableItem row = cursor.getRow();
				int column = cursor.getColumn();
				text.setText(row.getText(column));
				text.addKeyListener(KeyListener.keyPressedAdapter(event -> {
						// close the text editor and copy the data over
						// when the user hits "ENTER"
						if (event.character == SWT.CR) {
							TableItem localRow = cursor.getRow();
							int localColumn = cursor.getColumn();
							localRow.setText(localColumn, text.getText());
							text.dispose();
						}
						// close the text editor when the user hits "ESC"
						if (event.character == SWT.ESC) {
							text.dispose();
						}
				}));
				// close the text editor when the user tabs away
				text.addFocusListener(FocusListener.focusLostAdapter(event-> text.dispose()));
				editor.setEditor(text);
				text.setFocus();
			}
		});
		
		cursor.addKeyListener(KeyListener.keyPressedAdapter(e-> {
			if (e.keyCode == SWT.CTRL
				|| e.keyCode == SWT.SHIFT
				|| (e.stateMask & SWT.CONTROL) != 0
				|| (e.stateMask & SWT.SHIFT) != 0) {
				cursor.setVisible(false);
			}
		}));

		cursor.addMouseListener(MouseListener.mouseDownAdapter(e -> {
			final Text text = new Text(cursor, SWT.NONE);
			TableItem row = cursor.getRow();
			int column = cursor.getColumn();
			text.setText(row.getText(column));
			text.addKeyListener(KeyListener.keyPressedAdapter(event -> {	//zf, 此keyListener与上相同
				// close the text editor and copy the data over
				// when the user hits "ENTER"
				if (event.character == SWT.CR) {
					TableItem localRow = cursor.getRow();
					int localColumn = cursor.getColumn();
					localRow.setText(localColumn, text.getText());
					text.dispose();
				}
				// close the text editor when the user hits "ESC"
				if (event.character == SWT.ESC) {
					text.dispose();
				}
			}));
			// close the text editor when the user clicks away
			text.addFocusListener(FocusListener.focusLostAdapter(event -> text.dispose()));
			editor.setEditor(text);
			text.setFocus();
		}));

		table.addKeyListener(KeyListener.keyReleasedAdapter(e-> {
			if (e.keyCode == SWT.CONTROL && (e.stateMask & SWT.SHIFT) != 0) return;
			if (e.keyCode == SWT.SHIFT && (e.stateMask & SWT.CONTROL) != 0) return;
			if (e.keyCode != SWT.CONTROL && (e.stateMask & SWT.CONTROL) != 0) return;
			if (e.keyCode != SWT.SHIFT && (e.stateMask & SWT.SHIFT) != 0) return;

			TableItem[] selection = table.getSelection();
			TableItem row = (selection.length == 0) ? table.getItem(table.getTopIndex()) : selection[0];
			table.showItem(row);
			cursor.setSelection(row, cursor.getColumn());
			cursor.setVisible(true);
			cursor.setFocus();
	}));
		

	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

package zcatt.examples.swt.widgets;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;


public class TableTab extends WidgetTab {
	final static int ROW_COUNT =9;
	final static int COL_COUNT = 16;
	
	Text resultText;
	Table table;
	Button btnMulti;
	Button btnCheck;
	Button btnFullSelection;
	Button btnHideSelection;
	Button btnHeaderVisible;
	Button btnLineVisible;
	Combo comboSortDirection;
	Combo comboTopIndex;
	Combo comboShowItem;
	Combo comboColumnAlignment;
	Button btnMovable;
	Button btnSizable;

	Image image1;
	Image image2;
	
	public TableTab(WidgetExample example) {
		super(example);

		image1 = WidgetExample.loadImage(example.display, "pattern1.jpg");		
		image2 = WidgetExample.loadImage(example.display, "pattern2.jpg");		
	}

	@Override
	public void dispose() {
		image1.dispose();
		image1 = null;
		image2.dispose();
		image2 = null;
	}

	@Override
	public String getText() {
		return "Table";
	}

	@Override
	public String getCategory() {
		return "Tables";
	}

	@Override
	public String getDescription() {
		return "show table";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Table example.\n");
		
		Group grp = new Group(comp, SWT.NONE);
		grp.setText("table");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(600,150));

		//#. new table
		table = new Table(grp, calcTableStyle());
		table.setLayoutData(new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL));
		//table.setLayoutData(new GridData(500, 100));
		
		//#. new tableColumn
		SelectionListener columnListener = SelectionListener.widgetSelectedAdapter(event->{
			TableColumn curColumn = (TableColumn) event.widget;
			if(curColumn != table.getSortColumn())
			{
				table.setSortColumn(curColumn);
			}
			else
			{
				int sortDirection = table.getSortDirection();
				switch(sortDirection)
				{
				case SWT.NONE: 
					sortDirection = SWT.UP;
					break;
				case SWT.UP:
					sortDirection = SWT.DOWN;
					break;
				case SWT.DOWN:
					sortDirection = SWT.NONE;
					break;
				}
				table.setSortDirection(sortDirection);
				
				
				TableItem[] tis = table.getItems();
				if(sortDirection != SWT.NONE) {
					Arrays.sort(tis, (a, b)->{
						String sa = a.getText();
						String sb = b.getText();
						int res = sa.compareTo(sb);
						
						if(table.getSortDirection() == SWT.DOWN)
							res = -res;
						return res;
						
					});				
				}
				
				String[] itemNames = new String[ROW_COUNT*COL_COUNT];
				for(int row = 0; row < ROW_COUNT; row++)
				{
					for(int col = 0; col<COL_COUNT; col++)
					{
						itemNames[row*COL_COUNT + col] = tis[row].getText(col);
					}
				}

				table.removeAll();
				for(int row = 0; row < ROW_COUNT; row++)
				{
					TableItem ti = new TableItem(table, SWT.NONE);
					for(int col = 0; col<COL_COUNT; col++)
					{
						if(col == 0)
							ti.setImage(col, image2);
						ti.setText(col, itemNames[row*COL_COUNT + col]);
					}
				}
				
			}
		});
		
		for(int i = 0; i<COL_COUNT; i++)
		{
			TableColumn tc = new TableColumn(table, calcTableColumnStyle());
			tc.setImage(image1);
			tc.setText("head "+i);
			tc.setToolTipText("this is column "+i);		
			
			tc.addSelectionListener(columnListener);
		}
		table.setSortColumn(table.getColumn(0));
		
		//#. new tableItem
		for(int row = 0; row < ROW_COUNT; row++)
		{
			TableItem ti = new TableItem(table, SWT.NONE);
			for(int col = 0; col<COL_COUNT; col++)
			{
				if(col == 0)
					ti.setImage(col, image2);
				ti.setText(col, "cell "+row+","+col);
			}
			if(row == 3)
			{
				ti.setBackground(table.getDisplay().getSystemColor(SWT.COLOR_GRAY));
				ti.setBackground(1, table.getDisplay().getSystemColor(SWT.COLOR_YELLOW));
			}
		}
		
		//#.pack
		for(TableColumn tc: table.getColumns())
		{
			tc.pack();
		}
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);

		GridLayout gridLayout;
		gridLayout = new GridLayout(3, true);
		comp.setLayout(gridLayout);
		
		Group grp;
		Group grp2;
		Button btn;
		
		
		grp= new Group(comp, SWT.NONE);
		grp.setText("table styles");
		grp.setLayout(new GridLayout());
		
		
		SelectionListener selectionAdapter = SelectionListener.widgetSelectedAdapter(event ->{
			example.recreateWidgetComp();
		});

		btnMulti = new Button(grp, SWT.CHECK);
		btnMulti.setText("MULTI style");
		//btnSingle.setSelection(false);
		btnMulti.addSelectionListener(selectionAdapter);	

		btnCheck = new Button(grp, SWT.CHECK);
		btnCheck.setText("CHECK style");
		//btnSingle.setSelection(false);
		btnCheck.addSelectionListener(selectionAdapter);	
		
		btnFullSelection = new Button(grp, SWT.CHECK);
		btnFullSelection.setText("full selection style");
		btnFullSelection.addSelectionListener(selectionAdapter);

		btnHideSelection = new Button(grp, SWT.CHECK);
		btnHideSelection.setText("hide selection style");
		btnHideSelection.addSelectionListener(selectionAdapter);
		
		

		grp= new Group(comp, SWT.NONE);
		grp.setText("table options");
		grp.setLayout(new GridLayout());
		
		btnHeaderVisible = new Button(grp, SWT.CHECK);
		btnHeaderVisible.setText("header visible");
		btnHeaderVisible.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			table.setHeaderVisible(btnHeaderVisible.getSelection());
		}));
		
		btnLineVisible = new Button(grp, SWT.CHECK);
		btnLineVisible.setText("line visible");
		btnLineVisible.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			table.setLinesVisible(btnLineVisible.getSelection());
		}));

		grp2= new Group(grp, SWT.NONE);
		grp2.setText("sort direction");
		grp2.setLayout(new GridLayout());

		comboSortDirection = new Combo(grp2, SWT.DROP_DOWN|SWT.READ_ONLY);
		comboSortDirection.setItems(new String[] {
				"NONE",
				"UP",
				"DOWN",
		});
		comboSortDirection.select(0);
		comboSortDirection.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int curSortDirection = table.getSortDirection();
			
			int sd = comboSortDirection.getSelectionIndex();
			if (sd == 0)
				sd = SWT.NONE;
			else if(sd == 1)
				sd = SWT.UP;
			else 
				sd = SWT.DOWN;
			
			if(sd == curSortDirection)
				return;
			
			table.setSortDirection(sd);
			
			if(sd != SWT.NONE) {
				TableItem[] tis = table.getItems();
				Arrays.sort(tis, (a, b)->{
					String sa = a.getText();
					String sb = b.getText();
					return sa.compareTo(sb) ;				
				});				
			}
			
		}));

		grp2= new Group(grp, SWT.NONE);
		grp2.setText("show item");
		grp2.setLayout(new GridLayout());
		
		comboShowItem = new Combo(grp2, SWT.DROP_DOWN|SWT.READ_ONLY);
		String[] names= new String[ROW_COUNT];
		for(int i = 0; i< names.length; i++)
		{
			names[i] = Integer.toString(i);
		}
		comboShowItem.setItems(names);
		comboShowItem.select(0);
		comboShowItem.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int i = comboShowItem.getSelectionIndex();
			assert i>=0 && i< table.getItemCount();
			TableItem ti = table.getItem(i);			
			table.showItem(ti);
		}));

		grp2= new Group(grp, SWT.NONE);
		grp2.setText("top index");
		grp2.setLayout(new GridLayout());
		
		comboTopIndex = new Combo(grp2, SWT.DROP_DOWN|SWT.READ_ONLY);
		names= new String[ROW_COUNT];
		for(int i = 0; i< names.length; i++)
		{
			names[i] = Integer.toString(i);
		}
		comboTopIndex.setItems(names);
		comboTopIndex.select(0);
		comboTopIndex.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int i = comboTopIndex.getSelectionIndex();
			assert i>=0 && i< table.getItemCount();
			TableItem ti = table.getItem(i);			
			table.setTopIndex(i);
		}));

		

		grp= new Group(comp, SWT.NONE);
		grp.setText("table column options");
		grp.setLayout(new GridLayout());
	
		
		btnMovable = new Button(grp, SWT.CHECK);
		btnMovable.setText("column movable");
		btnMovable.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			boolean movable = btnMovable.getSelection();
			for(TableColumn tc : table.getColumns())
			{
				tc.setMoveable(movable);
			}
			
		}));

		btnSizable = new Button(grp, SWT.CHECK);
		btnSizable.setText("column sizable");
		btnSizable.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			boolean sizable = btnSizable.getSelection();
			for(TableColumn tc : table.getColumns())
			{
				tc.setMoveable(sizable);
			}
			
		}));
		
		grp2= new Group(grp, SWT.NONE);
		grp2.setText("alignment");
		grp2.setLayout(new GridLayout());

		comboColumnAlignment = new Combo(grp2, SWT.DROP_DOWN|SWT.READ_ONLY);
		comboColumnAlignment.setItems(new String[] {
				"CENTER",
				"LEFT",
				"RIGHT",
		});
		comboColumnAlignment.select(0);
		comboColumnAlignment.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int alignment = comboColumnAlignment.getSelectionIndex();
			if(alignment == 0)
				alignment = SWT.CENTER;
			else if(alignment == 1)
				alignment = SWT.LEFT;
			else 
				alignment = SWT.RIGHT;
			
			for(TableColumn tc : table.getColumns())
			{
				tc.setAlignment(alignment);
			}
			
		}));
		
	}

	
	int calcTableStyle()
	{
		int res = SWT.BORDER |SWT.H_SCROLL | SWT.V_SCROLL;
		res |= btnMulti.getSelection() ? SWT.MULTI : SWT.SINGLE;
		if(btnCheck.getSelection())
			res |= SWT.CHECK;
		if(btnFullSelection.getSelection())
			res |= SWT.FULL_SELECTION;
		if(btnHideSelection.getSelection())
			res |= SWT.HIDE_SELECTION;
		
		return res;
	}

	int calcTableColumnStyle()
	{
		int i = comboColumnAlignment.getSelectionIndex();
		if(i == 0)
			i = SWT.CENTER;
		else if(i == 1)
			i = SWT.LEFT;
		else 
			i = SWT.RIGHT;
		
		return i;
	}
}

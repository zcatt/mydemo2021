package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class ToolBarTab extends WidgetTab {
	Text resultText;
	
	Image copyImg;
	Image cutImg;
	Image pasteImg;
	Image deleteImg;
	Image hotImg;

	public ToolBarTab(WidgetExample example) {
		super(example);
		copyImg = WidgetExample.loadImage(example.display, "cmd_Copy.gif");
		cutImg = WidgetExample.loadImage(example.display, "cmd_Cut.gif");
		pasteImg = WidgetExample.loadImage(example.display, "cmd_Paste.gif");
		deleteImg = WidgetExample.loadImage(example.display, "cmd_Delete.gif");
		hotImg = WidgetExample.loadImage(example.display, "pattern1.jpg");
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "ToolBar";
	}

	@Override
	public String getCategory() {
		return "ToolBars";
	}

	@Override
	public String getDescription() {
		return "show ToolBar";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(1, false));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("ToolBar and ToolItem examples.\n");

		Group grp;
		ToolBar toolbar;
		ToolItem toolitem;
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL");
		grp.setLayout(new GridLayout());
		
		toolbar = new ToolBar(grp, SWT.HORIZONTAL);
		
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setText("push");
		toolitem.setToolTipText("SWT.PUSH style toolItem");
		toolitem.setImage(copyImg);
		toolitem.setHotImage(hotImg);
		
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		//toolitem.setText("radio");
		toolitem.setToolTipText("SWT.RADIO style toolItem");
		toolitem.setImage(cutImg);
		toolitem.setHotImage(hotImg);

		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setText("radio");
		toolitem.setToolTipText("SWT.RADIO style toolItem");
		//toolitem.setImage(pasteImg);
		toolitem.setHotImage(hotImg);
		
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		//toolitem.setText("check");
		toolitem.setToolTipText("SWT.CHECK style toolItem");
		toolitem.setImage(deleteImg);
		toolitem.setHotImage(hotImg);
		
		new ToolItem(toolbar, SWT.SEPARATOR);
		
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		//toolitem.setText("dropDown");
		toolitem.setToolTipText("SWT.DROP_DOWN style toolItem");
		toolitem.setImage(deleteImg);
		toolitem.setHotImage(hotImg);
		toolitem.addSelectionListener(new SelectionAdapter() {
			Menu menu = null;
			@Override
			public void widgetSelected(SelectionEvent event) {
				ToolBar toolbar = ((ToolItem) event.widget).getParent();
				if (menu == null) {
					// Lazy create the menu.
					menu = new Menu(toolbar.getShell(), SWT.POP_UP);
					MenuItem menuItem;
					menuItem = new MenuItem(menu, SWT.NONE);
					menuItem.setText("item 1");
					menuItem = new MenuItem(menu, SWT.NONE);
					menuItem.setText("item 2");
					menuItem = new MenuItem(menu, SWT.NONE);
					menuItem.setText("item 3");
					menuItem = new MenuItem(menu, SWT.NONE);
					menuItem.setText("item 4");
				}
			
				if (event.detail == SWT.ARROW) {
					final ToolItem toolItem = (ToolItem) event.widget;
	
					Point point = toolbar.toDisplay(new Point(event.x, event.y));
					menu.setLocation(point.x, point.y);
					menu.setVisible(true);
				}				
			}			
		});
		
		
		
		
		

		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL|WRAP");
		grp.setLayout(new GridLayout());

		toolbar = new ToolBar(grp, SWT.HORIZONTAL|SWT.WRAP);
		
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		//toolitem.setText("push");
		toolitem.setToolTipText("SWT.PUSH style toolItem");
		toolitem.setImage(copyImg);
		toolitem.setHotImage(hotImg);
		
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		//toolitem.setText("radio");
		toolitem.setToolTipText("SWT.RADIO style toolItem");
		toolitem.setImage(cutImg);
		toolitem.setHotImage(hotImg);

		toolitem = new ToolItem(toolbar, SWT.RADIO);
		//toolitem.setText("radio");
		toolitem.setToolTipText("SWT.RADIO style toolItem");
		toolitem.setImage(pasteImg);
		toolitem.setHotImage(hotImg);
		
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		//toolitem.setText("check");
		toolitem.setToolTipText("SWT.CHECK style toolItem");
		toolitem.setImage(deleteImg);
		toolitem.setHotImage(deleteImg);
		
		new ToolItem(toolbar, SWT.SEPARATOR);
		
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		//toolitem.setText("dropDown");
		toolitem.setToolTipText("SWT.DROP_DOWN style toolItem");
		toolitem.setImage(deleteImg);
		toolitem.setHotImage(deleteImg);
		
		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("HORIZONTAL|FLAT");
		grp.setLayout(new GridLayout());
		
		toolbar = new ToolBar(grp, SWT.HORIZONTAL|SWT.FLAT);
		
		toolitem = new ToolItem(toolbar, SWT.PUSH);
		toolitem.setText("push");
		toolitem.setToolTipText("SWT.PUSH style toolItem");
		toolitem.setImage(copyImg);
		toolitem.setHotImage(hotImg);
		
		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setText("radio");
		toolitem.setToolTipText("SWT.RADIO style toolItem");
		toolitem.setImage(cutImg);
		toolitem.setHotImage(hotImg);

		toolitem = new ToolItem(toolbar, SWT.RADIO);
		toolitem.setText("radio");
		toolitem.setToolTipText("SWT.RADIO style toolItem");
		toolitem.setImage(pasteImg);
		toolitem.setHotImage(hotImg);
		
		toolitem = new ToolItem(toolbar, SWT.CHECK);
		toolitem.setText("check");
		toolitem.setToolTipText("SWT.CHECK style toolItem");
		toolitem.setImage(deleteImg);
		toolitem.setHotImage(deleteImg);
		
		new ToolItem(toolbar, SWT.SEPARATOR);
		
		toolitem = new ToolItem(toolbar, SWT.DROP_DOWN);
		toolitem.setText("dropDown");
		toolitem.setToolTipText("SWT.DROP_DOWN style toolItem");
		toolitem.setImage(deleteImg);
		toolitem.setHotImage(deleteImg);
		
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

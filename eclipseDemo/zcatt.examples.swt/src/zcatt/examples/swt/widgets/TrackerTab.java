package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tracker;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;


public class TrackerTab extends WidgetTab {
	Text resultText;
	Image image1;

	public TrackerTab(WidgetExample example) {
		super(example);
		image1 = WidgetExample.loadImage(example.display, "pattern1.jpg");		
	}

	@Override
	public void dispose() {
		image1.dispose();
		image1 = null;
	}

	@Override
	public String getText() {
		return "Tracker";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Tracker example.\n");
		resultText.append("Mouse down and drag....\n");
		
		Group grp = new Group(comp, SWT.NONE);
		grp.setText("tree");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(600,400));
		
		Canvas canvas;
		canvas = new Canvas(grp, SWT.NONE);
		canvas.setBackground(comp.getDisplay().getSystemColor(SWT.COLOR_YELLOW));
		canvas.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		canvas.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDown(MouseEvent event) {
				// TODO Auto-generated method stub
				super.mouseDown(event);
				Tracker tracker = new Tracker(canvas, SWT.NONE);
				tracker.setRectangles(new Rectangle[] {
						new Rectangle(event.x, event.y, 100,100),
						new Rectangle(event.x+100, event.y+100, 100,100),
				});
				tracker.setStippled(true);
				tracker.setCursor(canvas.getDisplay().getSystemCursor(SWT.CURSOR_HAND));
				
				
				if(!tracker.open())
				{
					resultText.append("user cancels the tracker\n");
				}
				else
				{
					Rectangle[] rects = tracker.getRectangles();
					//resultText.append("result: "+rects[0]+"\n");
					resultText.append("result: \n");
					for(Rectangle e: rects)
						resultText.append(e+"\n");
						
				}

			}

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseUp(e);
			}
			
		});
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

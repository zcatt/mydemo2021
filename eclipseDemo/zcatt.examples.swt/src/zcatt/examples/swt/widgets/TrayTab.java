package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;

public class TrayTab extends WidgetTab {
	Text resultText;
	Tray tray;
	TrayItem trayItem;
	ToolTip tip;

	Text inputText;

	Image image1;
	
	public TrayTab(WidgetExample example) {
		super(example);
		tray = null;
		trayItem = null;
		image1 = WidgetExample.loadImage(example.display, "pattern1.jpg");		
	}

	@Override
	public void dispose() {
		if(trayItem != null)
		{
			trayItem.dispose();
			trayItem = null;
		}
		image1.dispose();
		image1 = null;
	}

	@Override
	public String getText() {
		return "Tray";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show Tray and TrayItem";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout(1, false));
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("CoolBar and CoolItem examples.\n");
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);

		GridLayout gridLayout;
		gridLayout = new GridLayout(2, true);
		comp.setLayout(gridLayout);
		
		Text inputText = new Text(comp, SWT.SINGLE|SWT.BORDER);
		inputText.setText("Hello, the world.");
		
		tip = new ToolTip(parent.getShell(), SWT.BALLOON | SWT.ICON_INFORMATION);
		tip.setText("tooltip");

		tray = parent.getDisplay().getSystemTray();
		trayItem = new TrayItem(tray, SWT.NONE);
		trayItem.setImage(image1);
		//trayItem.setHighlightImage(image1);
		trayItem.setToolTip(tip);
		
		
		Button btn = new Button(comp, SWT.PUSH);
		btn.setText("trig trayItem");
		btn.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			tip.setMessage(inputText.getText());
			tip.setVisible(true);
			trayItem.setToolTipText(inputText.getText());
			//trayItem.setVisible(true);
		}));
	}
}

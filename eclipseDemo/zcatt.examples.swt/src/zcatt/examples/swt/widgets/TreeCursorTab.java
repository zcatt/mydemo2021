package zcatt.examples.swt.widgets;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.custom.TreeCursor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

public class TreeCursorTab extends WidgetTab {
	Text resultText;
	Tree tree;
	TreeCursor cursor;
	ControlEditor editor;
	
	public TreeCursorTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "TreeCursor";
	}

	@Override
	public String getCategory() {
		return "Trees";
	}

	@Override
	public String getDescription() {
		return "show TreeCursor";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("TreeCursor example.\n");
		
		Group grp;		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("tree");
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));		
		grp.setLayout(new GridLayout());

		tree = new Tree(grp, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
		tree.setLayoutData(new GridData(GridData.FILL_BOTH));
		tree.setHeaderVisible(true);
		TreeColumn column1 = new TreeColumn(tree, SWT.NONE);
		TreeColumn column2 = new TreeColumn(tree, SWT.NONE);
		TreeColumn column3 = new TreeColumn(tree, SWT.NONE);
		for (int i = 0; i < 9; i++) {
			TreeItem item = new TreeItem(tree, SWT.NONE);
			item.setText(new String[] { "root " + i + " 0", "root " + i + " 1", "root " + i + " 2" });
			for (int j = 0; j < 9; j++) {
				TreeItem item2 = new TreeItem(item, SWT.NONE);
				item2.setText(new String[] { "child " + j + " 0", "child " + j + " 1", "child " + j + " 2" });
				TreeItem current = item2;
				for (int k = 0; k < 5; k++) {
					TreeItem item3 = new TreeItem(current, SWT.NONE);
					item3.setText(new String[] { "descendent " + k + " 0", "descendent " + k + " 1", "descendent " + k + " 2" });
					current = item3;
				}
			}
		}
		column1.setWidth(200);
		column2.setWidth(100);
		column3.setWidth(100);
		
		cursor = new TreeCursor(tree, SWT.NONE);
		// create an editor to edit the cell when the user hits "ENTER"
		// while over a cell in the tree
		editor = new ControlEditor(cursor);
		editor.grabHorizontal = true;
		editor.grabVertical = true;

		cursor.addSelectionListener(new SelectionAdapter() {
			// when the TreeEditor is over a cell, select the corresponding row
			// in the tree
			@Override
			public void widgetSelected(SelectionEvent e) {
				tree.setSelection(new TreeItem[] { cursor.getRow() });
			}

			// when the user hits "ENTER" in the TreeCursor, pop up a text
			// editor so that they can change the text of the cell
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					final Text text = new Text(cursor, SWT.NONE);
					TreeItem row = cursor.getRow();
					int column = cursor.getColumn();
					text.setText(row.getText(column));
					text.addKeyListener(KeyListener.keyPressedAdapter(event -> {
						// close the text editor and copy the data over
						// when the user hits "ENTER"
						if (event.character == SWT.CR) {
							TreeItem localRow = cursor.getRow();
							int localColumn = cursor.getColumn();
							localRow.setText(localColumn, text.getText());
							text.dispose();
						}
						// close the text editor when the user hits "ESC"
						if (event.character == SWT.ESC) {
							text.dispose();
						}
					}));
					editor.setEditor(text);
					text.setFocus();
				}
			});
			// Hide the TreeCursor when the user hits the "MOD1" or "MOD2" key.
			// This allows the user to select multiple items in the tree.
			cursor.addKeyListener(KeyListener.keyPressedAdapter(e -> {
				if (e.keyCode == SWT.MOD1 || e.keyCode == SWT.MOD2 || (e.stateMask & SWT.MOD1) != 0
						|| (e.stateMask & SWT.MOD2) != 0) {
					cursor.setVisible(false);
				}
			}));
		// Show the TreeCursor when the user releases the "MOD2" or "MOD1" key.
		// This signals the end of the multiple selection task.
		tree.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.MOD1 && (e.stateMask & SWT.MOD2) != 0) return;
				if (e.keyCode == SWT.MOD2 && (e.stateMask & SWT.MOD1) != 0) return;
				if (e.keyCode != SWT.MOD1 && (e.stateMask & SWT.MOD1) != 0) return;
				if (e.keyCode != SWT.MOD2 && (e.stateMask & SWT.MOD2) != 0) return;

				TreeItem[] selection = tree.getSelection();
				TreeItem row = (selection.length == 0) ? tree.getItem(tree.indexOf(tree.getTopItem())) : selection[0];
				tree.showItem(row);
				cursor.setSelection(row, cursor.getColumn());
				cursor.setVisible(true);
				cursor.setFocus();
			}
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.keyCode) {
					case SWT.ARROW_LEFT:
					case SWT.ARROW_RIGHT: {
						if ((e.stateMask & SWT.MOD1) != 0) {
							TreeItem[] selection = tree.getSelection();
							if (selection.length > 0) {
								selection[0].setExpanded(e.keyCode == SWT.ARROW_RIGHT);
							}
							break;
						}
					}
				}
			}
		});
		
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

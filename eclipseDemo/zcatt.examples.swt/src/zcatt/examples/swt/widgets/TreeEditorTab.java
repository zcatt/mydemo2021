package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.custom.TreeCursor;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class TreeEditorTab extends WidgetTab {
	Text resultText;
	Tree tree;
	TreeEditor editor;

	TreeItem[] lastItem;
	
	public TreeEditorTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "TreeEditor";
	}

	@Override
	public String getCategory() {
		return "Trees";
	}

	@Override
	public String getDescription() {
		return "show TreeEditor";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("TreeEditor example.\n");
		
		Group grp;		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("tree");
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));		
		grp.setLayout(new FillLayout());

		tree = new Tree (grp, SWT.BORDER);
		for (int i=0; i<16; i++) {
			TreeItem itemI = new TreeItem (tree, SWT.NONE);
			itemI.setText ("Item " + i);
			for (int j=0; j<16; j++) {
				TreeItem itemJ = new TreeItem (itemI, SWT.NONE);
				itemJ.setText ("Item " + j);
			}
		}

		lastItem = new TreeItem [1];
		editor = new TreeEditor (tree);
		
		
		tree.addListener (SWT.Selection, event -> {
			final TreeItem item = (TreeItem) event.item;
			if (item != null && item == lastItem [0]) {
				boolean showBorder = true;
				final Composite composite = new Composite (tree, SWT.NONE);
				if (showBorder) composite.setBackground (new Color(0,0,0));
				final Text text = new Text (composite, SWT.NONE);
				final int inset = showBorder ? 1 : 0;
				composite.addListener (SWT.Resize, e1 -> {
					Rectangle rect1 = composite.getClientArea ();
					text.setBounds (rect1.x + inset, rect1.y + inset, rect1.width - inset * 2, rect1.height - inset * 2);
				});
				Listener textListener = e2 -> {
					switch (e2.type) {
						case SWT.FocusOut:
							item.setText (text.getText ());
							composite.dispose ();
							break;
						case SWT.Verify:
							String newText = text.getText ();
							String leftText = newText.substring (0, e2.start);
							String rightText = newText.substring (e2.end, newText.length ());
							GC gc = new GC (text);
							Point size = gc.textExtent (leftText + e2.text + rightText);
							gc.dispose ();
							size = text.computeSize (size.x, SWT.DEFAULT);
							editor.horizontalAlignment = SWT.LEFT;
							Rectangle itemRect = item.getBounds (), rect2 = tree.getClientArea ();
							editor.minimumWidth = Math.max (size.x, itemRect.width) + inset * 2;
							int left = itemRect.x, right = rect2.x + rect2.width;
							editor.minimumWidth = Math.min (editor.minimumWidth, right - left);
							editor.minimumHeight = size.y + inset * 2;
							editor.layout ();
							break;
						case SWT.Traverse:
							switch (e2.detail) {
								case SWT.TRAVERSE_RETURN:
									item.setText (text.getText ());
									//FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									composite.dispose ();
									e2.doit = false;
							}
							break;
					}
				};
				text.addListener (SWT.FocusOut, textListener);
				text.addListener (SWT.Traverse, textListener);
				text.addListener (SWT.Verify, textListener);
				editor.setEditor (composite, item);
				text.setText (item.getText ());
				text.selectAll ();
				text.setFocus ();
			}
			lastItem [0] = item;
		});
		
	}

	@Override
	public void createControlPanel(Composite parent) {
		// TODO Auto-generated method stub

	}

}

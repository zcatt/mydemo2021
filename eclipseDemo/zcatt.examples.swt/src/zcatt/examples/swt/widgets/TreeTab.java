package zcatt.examples.swt.widgets;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

public class TreeTab extends WidgetTab {
	final static int ITEM_COUNT = 14;
	final static int COL_COUNT = 3;
	
	
	Text resultText;
	Tree tree;
	Button btnMulti;
	Button btnCheck;
	Button btnFullSelection;
	Button btnHideSelection;
	Button btnHeaderVisible;
	Button btnLineVisible;
	Combo comboSortDirection;
	Combo comboShowItem;
	Combo comboColumnAlignment;
	Button btnMovable;
	Button btnSizable;

	Image image1;
	Image image2;

	public TreeTab(WidgetExample example) {
		super(example);
		image1 = WidgetExample.loadImage(example.display, "pattern1.jpg");		
		image2 = WidgetExample.loadImage(example.display, "pattern2.jpg");		
	}

	@Override
	public void dispose() {
		image1.dispose();
		image1 = null;
		image2.dispose();
		image2 = null;
	}

	@Override
	public String getText() {
		return "Tree";
	}

	@Override
	public String getCategory() {
		return "Trees";
	}

	@Override
	public String getDescription() {
		return "show Tree";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Table example.\n");
		
		Group grp = new Group(comp, SWT.NONE);
		grp.setText("tree");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(600,150));

		//#. new tree
		tree = new Tree(grp, calcTreeStyle());
		tree.setLayoutData(new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL));
		//tree.setLayoutData(new GridData(500, 100));
		
		//#. new tableColumn
		SelectionListener columnListener = SelectionListener.widgetSelectedAdapter(event->{
			TreeColumn curColumn = (TreeColumn) event.widget;
			if(curColumn != tree.getSortColumn())
			{
				tree.setSortColumn(curColumn);
			}
			else
			{
				int sortDirection = tree.getSortDirection();
				switch(sortDirection)
				{
				case SWT.NONE: 
					sortDirection = SWT.UP;
					break;
				case SWT.UP:
					sortDirection = SWT.DOWN;
					break;
				case SWT.DOWN:
					sortDirection = SWT.NONE;
					break;
				}
				tree.setSortDirection(sortDirection);
				
				//do sort
				resultText.append("do sort\n");
			
			}
		});
		
		for(int i = 0; i<COL_COUNT; i++)
		{
			TreeColumn tc = new TreeColumn(tree, calcTreeColumnStyle());
			tc.setImage(image1);
			tc.setText("head "+i);
			tc.setToolTipText("this is column "+i);		
			
			tc.addSelectionListener(columnListener);
		}
		tree.setSortColumn(tree.getColumn(0));
		
		//#. new treeItem, 共计ITEM_COUNT个
		TreeItem ti0 = new TreeItem(tree, SWT.NONE);
		ti0.setImage(image1);
		ti0.setText("node 0");

		TreeItem ti1 = new TreeItem(tree, SWT.NONE);
		ti1.setImage(image1);
		ti1.setText("node 1");
		
		TreeItem ti00;
		TreeItem ti01;
		TreeItem ti02;
		TreeItem ti;
		ti00 = appendChildItem(ti0, "node 0.0", image2);
		ti01 = appendChildItem(ti0, "node 0.1", image1);
		ti02 = appendChildItem(ti0, "node 0.2", image2);
		ti = appendChildItem(ti01, "node 0.1.0", image2);
		ti = appendChildItem(ti01, "node 0.1.1", image2);
		ti = appendChildItem(ti01, "node 0.1.2", image2);
		

		ti00 = appendChildItem(ti1, "node 1.0", image2);
		ti01 = appendChildItem(ti1, "node 1.1", image1);
		ti02 = appendChildItem(ti1, "node 1.2", image2);
		ti = appendChildItem(ti01, "node 1.1.0", image2);
		ti = appendChildItem(ti01, "node 1.1.1", image2);
		ti = appendChildItem(ti01, "node 1.1.2", image2);
		
		//#.pack
		for(TreeColumn tc: tree.getColumns())
		{
			//tc.pack();
			tc.setWidth(200);
		}

	}
	
	TreeItem appendChildItem(TreeItem paren, String name, Image img)
	{
		TreeItem ti = new TreeItem(paren, SWT.NONE);
		
		for(int col = 0; col<COL_COUNT; col++)
		{
			if(col == 0)
			{
				ti.setImage(col, img);
				ti.setText(0, name);
			}
			else
			{
				ti.setText(col, "col "+col);
			}
		}
		return ti;		
	}

	@Override
	public void createControlPanel(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);

		GridLayout gridLayout;
		gridLayout = new GridLayout(3, true);
		comp.setLayout(gridLayout);
		
		Group grp;
		Group grp2;
		
		grp= new Group(comp, SWT.NONE);
		grp.setText("tree styles");
		grp.setLayout(new GridLayout());
		
		
		SelectionListener selectionAdapter = SelectionListener.widgetSelectedAdapter(event ->{
			example.recreateWidgetComp();
		});

		btnMulti = new Button(grp, SWT.CHECK);
		btnMulti.setText("MULTI style");
		//btnSingle.setSelection(false);
		btnMulti.addSelectionListener(selectionAdapter);	

		btnCheck = new Button(grp, SWT.CHECK);
		btnCheck.setText("CHECK style");
		//btnSingle.setSelection(false);
		btnCheck.addSelectionListener(selectionAdapter);	
		
		btnFullSelection = new Button(grp, SWT.CHECK);
		btnFullSelection.setText("full selection style");
		btnFullSelection.addSelectionListener(selectionAdapter);

		btnHideSelection = new Button(grp, SWT.CHECK);
		btnHideSelection.setText("hide selection style");
		btnHideSelection.addSelectionListener(selectionAdapter);
		
		

		grp= new Group(comp, SWT.NONE);
		grp.setText("tree options");
		grp.setLayout(new GridLayout());
		
		btnHeaderVisible = new Button(grp, SWT.CHECK);
		btnHeaderVisible.setText("header visible");
		btnHeaderVisible.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			tree.setHeaderVisible(btnHeaderVisible.getSelection());
		}));
		
		btnLineVisible = new Button(grp, SWT.CHECK);
		btnLineVisible.setText("line visible");
		btnLineVisible.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			tree.setLinesVisible(btnLineVisible.getSelection());
		}));

		grp2= new Group(grp, SWT.NONE);
		grp2.setText("sort direction");
		grp2.setLayout(new GridLayout());

		comboSortDirection = new Combo(grp2, SWT.DROP_DOWN|SWT.READ_ONLY);
		comboSortDirection.setItems(new String[] {
				"NONE",
				"UP",
				"DOWN",
		});
		comboSortDirection.select(0);
		comboSortDirection.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int curSortDirection = tree.getSortDirection();
			
			int sd = comboSortDirection.getSelectionIndex();
			if (sd == 0)
				sd = SWT.NONE;
			else if(sd == 1)
				sd = SWT.UP;
			else 
				sd = SWT.DOWN;
			
			if(sd == curSortDirection)
				return;
			
			tree.setSortDirection(sd);
			
			if(sd != SWT.NONE) {
				TreeItem[] tis = tree.getItems();
				Arrays.sort(tis, (a, b)->{
					String sa = a.getText();
					String sb = b.getText();
					return sa.compareTo(sb) ;				
				});				
			}
			
		}));

		grp2= new Group(grp, SWT.NONE);
		grp2.setText("show item");
		grp2.setLayout(new GridLayout());
		
		comboShowItem = new Combo(grp2, SWT.DROP_DOWN|SWT.READ_ONLY);
		String[] names= new String[ITEM_COUNT];
		for(int i = 0; i< names.length; i++)
		{
			names[i] = "node "+i;
		}
		comboShowItem.setItems(names);
		comboShowItem.select(0);
		comboShowItem.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int i = comboShowItem.getSelectionIndex();
			assert i>=0 && i< tree.getItemCount();
			TreeItem ti = tree.getItem(i);			
			tree.showItem(ti);
		}));

		grp2= new Group(grp, SWT.NONE);
		grp2.setText("top index");
		grp2.setLayout(new GridLayout());
		
		grp= new Group(comp, SWT.NONE);
		grp.setText("tree column options");
		grp.setLayout(new GridLayout());
	
		
		btnMovable = new Button(grp, SWT.CHECK);
		btnMovable.setText("column movable");
		btnMovable.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			boolean movable = btnMovable.getSelection();
			for(TreeColumn tc : tree.getColumns())
			{
				tc.setMoveable(movable);
			}
			
		}));

		btnSizable = new Button(grp, SWT.CHECK);
		btnSizable.setText("column sizable");
		btnSizable.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			boolean sizable = btnSizable.getSelection();
			for(TreeColumn tc : tree.getColumns())
			{
				tc.setMoveable(sizable);
			}
			
		}));
		
		grp2= new Group(grp, SWT.NONE);
		grp2.setText("alignment");
		grp2.setLayout(new GridLayout());

		comboColumnAlignment = new Combo(grp2, SWT.DROP_DOWN|SWT.READ_ONLY);
		comboColumnAlignment.setItems(new String[] {
				"CENTER",
				"LEFT",
				"RIGHT",
		});
		comboColumnAlignment.select(0);
		comboColumnAlignment.addSelectionListener(SelectionListener.widgetSelectedAdapter(event->{
			int alignment = comboColumnAlignment.getSelectionIndex();
			if(alignment == 0)
				alignment = SWT.CENTER;
			else if(alignment == 1)
				alignment = SWT.LEFT;
			else 
				alignment = SWT.RIGHT;
			
			for(TreeColumn tc : tree.getColumns())
			{
				tc.setAlignment(alignment);
			}
			
		}));		
	}
	
	int calcTreeStyle()
	{
		int res = SWT.BORDER |SWT.H_SCROLL | SWT.V_SCROLL;
		res |= btnMulti.getSelection() ? SWT.MULTI : SWT.SINGLE;
		if(btnCheck.getSelection())
			res |= SWT.CHECK;
		if(btnFullSelection.getSelection())
			res |= SWT.FULL_SELECTION;
		if(btnHideSelection.getSelection())
			res |= SWT.HIDE_SELECTION;
		
		return res;
	}

	int calcTreeColumnStyle()
	{
		int i = comboColumnAlignment.getSelectionIndex();
		if(i == 0)
			i = SWT.CENTER;
		else if(i == 1)
			i = SWT.LEFT;
		else 
			i = SWT.RIGHT;
		
		return i;
	}
	

}

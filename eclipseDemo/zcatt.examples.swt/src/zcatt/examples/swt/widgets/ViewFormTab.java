package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.custom.ViewForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class ViewFormTab extends WidgetTab {
	Text resultText;

	public ViewFormTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "ViewForm";
	}

	@Override
	public String getCategory() {
		return "Misc";
	}

	@Override
	public String getDescription() {
		return "show ViewForm";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("ViewForm example.\n");
		
		Group grp;		
		
		grp = new Group(comp, SWT.NONE);
		grp.setText("ViewForm");
		grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));		
		grp.setLayout(new GridLayout());
		
		ViewForm viewForm = new ViewForm(grp, SWT.NONE);
		viewForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));		
		
		Text text = new Text(viewForm, SWT.BORDER|SWT.MULTI);
		text.append("content\n");
		viewForm.setContent(text);
		
		Label label;
		label = new Label(viewForm, SWT.BORDER);
		label.setText("topCenter");
		viewForm.setTopCenter(label);

		label = new Label(viewForm, SWT.BORDER);
		label.setText("topLeft");
		viewForm.setTopLeft(label);

		label = new Label(viewForm, SWT.BORDER);
		label.setText("topRight");
		viewForm.setTopRight(label);
}

	@Override
	public void createControlPanel(Composite parent) {

	}

}

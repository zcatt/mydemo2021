package zcatt.examples.swt.widgets;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class VirtualTableTab extends WidgetTab {
	final static int ROW_COUNT=10000;
	final static int COL_COUNT = 4;
	
	Text resultText;
	Table table;

	public VirtualTableTab(WidgetExample example) {
		super(example);
	}

	@Override
	public void dispose() {
	}

	@Override
	public String getText() {
		return "Virtual Table";
	}

	@Override
	public String getCategory() {
		return "Tables";
	}

	@Override
	public String getDescription() {
		return "show virtual table";
	}

	@Override
	public void createWidget(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayout(new GridLayout());
		
		resultText = new Text(comp, SWT.MULTI);
		resultText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		resultText.append("Table example.\n");
		
		Group grp = new Group(comp, SWT.NONE);
		grp.setText("table");
		grp.setLayout(new GridLayout());
		grp.setLayoutData(new GridData(600,150));

		//#. new table
		table = new Table(grp, SWT.VIRTUAL|SWT.H_SCROLL|SWT.V_SCROLL|SWT.BORDER);
		table.setLayoutData(new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		for(int i = 0; i<COL_COUNT; i++)
		{
			TableColumn tc = new TableColumn(table, SWT.CENTER);
			tc.setText("head "+i);
			tc.setWidth(60);		//virtual table, 必须指定column width
		}
		//table.setSortColumn(table.getColumn(0));

		
		table.setItemCount(ROW_COUNT);
		table.addListener (SWT.SetData, new Listener () {
		      public void handleEvent (Event event) {
		          TableItem item = (TableItem) event.item;
		          int index = table.indexOf (item);
		          for(int i = 0; i< COL_COUNT; i++)
		        	  item.setText(i, "cell "+index+","+i);

		          resultText.append(item.getText () +"\n");
		          
		      }
		});
		table.pack();
	}

	@Override
	public void createControlPanel(Composite parent) {

	}

}

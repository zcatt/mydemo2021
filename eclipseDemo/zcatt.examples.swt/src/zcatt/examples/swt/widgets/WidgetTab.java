package zcatt.examples.swt.widgets;

import org.eclipse.swt.widgets.Composite;

public abstract class WidgetTab {
	WidgetExample example;
	
	public WidgetTab(WidgetExample example)
	{
		this.example = example;
	}
	

	public abstract void dispose();
	public abstract String getText();
	public abstract String getCategory();
	public abstract String getDescription();
	public abstract void createWidget(Composite parent);
	public abstract void createControlPanel(Composite parent);
}

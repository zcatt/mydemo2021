#include <stdio.h>
#include <glib-2.0/glib.h>

int main(int argc, char* argv[])
{
    printf("The version of this glib is %d.%d.%d.\n"
        , glib_major_version
        , glib_minor_version
        , glib_micro_version);

    return 0;
}
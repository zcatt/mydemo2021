﻿#include <glib.h>
#include "drive.h"

G_DEFINE_INTERFACE(Drive, drive, G_TYPE_INTERFACE);

static void drive_default_init(DriveInterface *iface)
{
    //pass
    return;
}

void drive_doDrive(Drive *drive)
{
    g_assert_true( IS_DRIVE(drive) );

    g_print("drive_doDrive.....\n");
    DRIVE_GET_IFACE(drive)->doDrive(drive);
}
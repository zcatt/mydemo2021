﻿#ifndef _INC_Drive
#define _INC_Drive

#include <glib-object.h>

G_BEGIN_DECLS

////////////////////////////////////////////////////////////
//
//
// Drive interface
//
//
////////////////////////////////////////////////////////////

#define DRIVE_TYPE      (drive_get_type())
#define DRIVE(inst)     (G_TYPE_CHECK_INSTANCE_CAST((inst), DRIVE_TYPE, Drive))
#define IS_DRIVE(inst)  (G_TYPE_CHECK_INSTANCE_TYPE((inst), DRIVE_TYPE))
#define DRIVE_GET_IFACE(inst)   (G_TYPE_INSTANCE_GET_INTERFACE((inst), DRIVE_TYPE, DriveInterface))

typedef struct _Drive Drive;        //struct _Drive并没有明确定义， 因不需要, 且interface本就不应有自己的data。
typedef struct _DriveInterface DriveInterface;

struct _DriveInterface
{
    GTypeInterface parent_iface;

    void (*doDrive)(Drive *drive);
};

GType drive_get_type(void);

void drive_doDrive(Drive *drive);

G_END_DECLS

#endif

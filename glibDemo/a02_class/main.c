﻿/*
    a02_class -
    本例中演示了gobject中的以下内容，
        .子类的编程
        .object的prop的实现以及notify的挂接
        .interface的实现
        .class实现一个interface.        


*/
#include "person.h"
#include "drive.h"

static void ageUpdated(Person *person)
{
    g_print("Notify: age Updated to %d\n", person_get_age(person));
}

static void phoneUpdated(Person *person)
{
    g_print("Notify: phone Updated to %s\n", person_get_phone(person));
}

int main(int argc, char** argv)
{
    Person *p = person_new();
    person_set_name(p, "Tom Smith");
    g_print("The person's name is %s\n", person_get_name(p)->str);

    g_signal_connect(p, "notify::age", G_CALLBACK(ageUpdated), NULL);
    g_signal_connect(p, "notify::phone", G_CALLBACK(phoneUpdated), NULL);
    person_set_age(p, 20);
    g_object_set(p, "age", 21, NULL);
    person_set_phone(p, "123-456-78901");
    g_object_set(p, "phone", "111-222-33333", NULL);

    drive_doDrive(DRIVE(p));

    g_object_unref(p);
    return 0;
}


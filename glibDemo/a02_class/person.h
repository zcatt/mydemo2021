﻿#ifndef _INC_Person
#define _INC_Person

#include <glib-object.h>

G_BEGIN_DECLS

////////////////////////////////////////////////////////////
//
//
// Person class
//
//
////////////////////////////////////////////////////////////

#define PERSON_WITH_DRIVE_IFACE         1

#define PERSON_TYPE     person_get_type() 
#define PERSON(obj)     (G_TYPE_CHECK_INSTANCE_CAST(obj, PERSON_TYPE, Person))
#define PERSON_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST(klass, PERSON_TYPE, PersonClass))
#define IS_PERSON(obj)  (G_TYPE_CHECK_INSTANCE_TYPE(obj, PERSON_TYPE))
#define IS_PERSON_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE(klass, PERSON_TYPE))
#define PERSON_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS(obj, PERSON_TYPE, PersonClass))

typedef struct _Person Person;
typedef struct _PersonPrivate  PersonPrivate;   //受G_ADD_PRIVATE()宏的限制， 此处只能取XXXPrivate
typedef struct _PersonClass PersonClass;


struct _Person
{
    GObject parent;
    PersonPrivate *priv;        
};

struct _PersonPrivate
{
    GString *m_name;
    int m_age;                  //as prop
    gchar *m_phone;           //as prop
};

struct _PersonClass
{
    GObjectClass parent_class;

#if 0
    void (*set_name)(Person* obj, const gchar *name);
    GString* (*get_name)(Person* obj);

    void (*set_age)(Person* obj, int age);
    GString* (*get_age)(Person* obj);

    void (*set_phone)(Person* obj, const gchar *phone);
    const gchar* (*get_phone)(Person* obj);

    gboolean (*is_a_driver)(void);
#endif

};

GType person_get_type(void);
Person* person_new(void);
void person_set_name(Person* obj, const gchar *name);
GString* person_get_name(Person* obj);
void person_set_age(Person* obj, int age);
int person_get_age(Person* obj);
void person_set_phone(Person* obj, const gchar *phone);
const gchar* person_get_phone(Person* obj);

gboolean person_is_a_driver(Person* obj);



////////////////////////////////////////////////////////////
//
//
// Drive interface
//
//
////////////////////////////////////////////////////////////




G_END_DECLS

#endif

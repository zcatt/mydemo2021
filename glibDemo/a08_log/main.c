/*
    a08_log -
    默认的g_debug()和g_info()不会输出。 要输出需设定G_MESSAGES_DEBUG环境变量，包含domain name, 例如
        > export G_MESSAGES_DEBUG=all
*/
#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "a08"

#include <glib.h>

int main(int argc, char* argv[])
{
    g_setenv ("G_MESSAGES_DEBUG", G_LOG_DOMAIN, TRUE);    //G_LOG_DOMAIN被重定义为a08
    g_debug("hello, the world");


    return 0;
}
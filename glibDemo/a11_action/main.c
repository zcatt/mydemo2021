#include <gio/gio.h>


static void basic_action(GSimpleAction *simple,
               GVariant      *parameter,            //g_action_activate()中传入. 其类型在g_simple_action_new()时指定.
               gpointer       user_data)            //g_signal_connect()时的arg
{
        g_print("basic_action()\n");
}

static void show_basic_action(void)
{
    g_print("show_basic_action().....\n");

    GAction *action;

    //new action
    action = G_ACTION(g_simple_action_new("basic_action", NULL));

    //set action
    g_signal_connect(action, "activate", G_CALLBACK(basic_action), NULL);

    //trig action
    g_action_activate(action, NULL);


    g_object_unref(action);
}

/////////////////////////////////////////////////////////////////////////////////

static void param_action(GAction *simple,
               GVariant      *parameter,            //g_action_activate()中传入. 其类型在g_simple_action_new()时指定.
               gpointer       user_data)            //g_signal_connect()时的arg
{
        g_print("param_action()\n");

        g_assert(parameter);

        const gchar* s = g_variant_get_string(parameter, NULL);
        g_print("%s\n", s);

}

static void show_param_action(void)
{
    g_print("show_param_action().....\n");

    GAction *action;

    //new action
    action = G_ACTION(g_simple_action_new("param_action", G_VARIANT_TYPE_STRING));

    //set action
    g_signal_connect(action, "activate", G_CALLBACK(param_action), NULL);

    //trig action
    g_action_activate(action, g_variant_new_string ("Hello world!"));


    g_object_unref(action);
}


/////////////////////////////////////////////////////////////////////////////////

static void stateful_action(GAction *simple,
               GVariant      *parameter,            //g_action_activate()中传入. 其类型在g_simple_action_new()时指定.
               gpointer       user_data)            //g_signal_connect()时的arg
{
        g_print("stateful_action()\n");

        GVariant *v = g_action_get_state(simple);
        const gchar* s = g_variant_get_string(v, NULL);
        g_print("%s\n", s);

        g_variant_unref(v);

}

static void show_stateful_action(void)
{
    g_print("show_stateful_action().....\n");

    GAction *action;

    //new action
    action = G_ACTION(g_simple_action_new_stateful("stateful_action"
                                    , NULL      //注意, 是param的 type, 与state无关
                                    , g_variant_new_string("Hi, stateful action.")));

    //set action
    g_signal_connect(action, "activate", G_CALLBACK(stateful_action), NULL);

    //trig action
    g_action_activate(action, NULL);


    g_object_unref(action);
}




int main(int argc, char* argv[])
{
    // show_basic_action();
    show_param_action();
    show_stateful_action();
    return 0;
}
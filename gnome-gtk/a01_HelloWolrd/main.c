﻿#include <gtk/gtk.h>

static void print_hello(GtkWidget *widget, gpointer data)
{
    g_print("hello world.\n");
}

static void activate(GtkApplication *app, gpointer user_data)
{
    GtkWidget *window;
    GtkWidget *box;
    GtkWidget *button;

    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Hello, the world.");
    gtk_window_set_default_size(GTK_WINDOW(window), 300, 200);
    
    box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_container_add(GTK_CONTAINER(window), box);

    button = gtk_button_new_with_label("Hello World.");
    g_signal_connect (button, "clicked", G_CALLBACK (print_hello), NULL);
    //g_signal_connect_swapped (button, "clicked", G_CALLBACK (gtk_widget_destroy), window);
    gtk_container_add(GTK_CONTAINER(box), button);

    gtk_widget_show_all(GTK_WIDGET(window));
    //gtk_window_present(GTK_WINDOW(window));    
}

int main(int argc, char** argv)
{
    GtkApplication *app;
    int status;

    app = gtk_application_new("com.gitee.zcatt.gtk.sample.helloworld", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app),argc, argv);
    g_assert_true((G_OBJECT(app)->ref_count == 1));
    g_object_unref(app);

    return status;
}

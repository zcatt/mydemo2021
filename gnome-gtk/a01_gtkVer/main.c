/*
    a01_gtkVer

*/
#include <gtk/gtk.h>

int main(int argc, char** argv)
{
    g_print("a01_gtkVer...\n");


    g_print("gtk version: %d.%d.%d\n", gtk_get_major_version(), gtk_get_minor_version(), gtk_get_micro_version());
    g_print("bin age: %d, intf age %d\n", gtk_get_binary_age (), gtk_get_interface_age ());
    
    return 1;
}
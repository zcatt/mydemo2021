﻿#ifndef INC_MINE_VIEW
#define INC_MINE_VIEW

#include <gtk/gtk.h>


typedef enum
{
    Mine_Flag_None,
    Mine_Flag_Flag,
    Mine_Flag_Maybe,
} MineFlagType;

typedef struct _MinePoint
{
    gint x;
    gint y;
    GtkWidget *widget;

    gboolean has_mine;
    MineFlagType flag;
    gint adjacent_count;        //相邻地雷数量
    gboolean swept;
} MinePoint;






#define TYPE_MINE_VIEW                  (mine_view_get_type())
#define MINE_VIEW(obj)                  (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_MINE_VIEW, MineView))
#define MINE_VIEW_CLASS(clazz)          (G_TYPTE_CHECK_CLASS_CAST((clazz), TYPE_MINE_VIEW, MineViewClass))
#define IS_MINE_VIEW(obj)               (G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_MINEW_VIEW))
#define IS_MINE_VIEW_CLASS(clazz)       (G_TYPE_CHECK_CLASS_TYPE((clazz), TYPE_MINE_VIEW))
#define MINE_VIEW_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS((obj), TYPE_MINE_VIEW, MineViewClass))

typedef struct _MineView MineView;
typedef struct _MineViewClass MineViewClass;

#if 0
//typedef struct _MineViewPrivate    MineViewPrivate;
struct _MineViewPrivate
{
    gint row_count;
    gint col_count;

    MinePoint **mine_points;
};
#endif

enum{
    Mine_State_0,
    Mine_State_Placed,
    Mine_State_Started,
    Mine_State_Exploded,      //fail
    Mine_State_Over,        //success
};
struct _MineView
{
    GtkGrid parent;
#if 0    
    MineViewPrivate *priv;
#endif

    gint state;            //Mine_State_XXX
    gboolean use_question_flag;
    gint row_count;
    gint col_count;
    gint mine_count;
    gint swept_count;
    gint flag_count;

    MinePoint *mine_points;     //mine_points[row_count*col_count]

    guint clock;
    GTimer *timer;
};

struct _MineViewClass
{
    GtkGridClass parent_class;

    //sig class handlers
#if 0    
    void (*game_over)(MineView *view, gboolean game_fail);
#endif    
};


void mine_point_add_class(MinePoint *mp, const gchar* style_class);
void mine_point_remove_class(MinePoint *mp, const gchar* style_class);



GType mine_view_get_type(void);
GtkWidget* mine_view_new(void);
gboolean mine_view_set_size(MineView *view, gint row_count, gint col_count, gint mine_count);
void mine_view_get_size(MineView *view, gint *row_count, gint *col_count, gint *mine_count);

void mine_view_start_game(MineView *view, gint row_count, gint col_count, gint mine_count);
void mine_view_force_start_game(MineView *view, gint row_count, gint col_count, gint mine_count);



#endif
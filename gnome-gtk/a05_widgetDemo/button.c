#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

static void clicked_cb(GtkButton *button, gpointer   user_data)
{
    g_print("clicked_cb()....\n");
}               

static gboolean button_press_cb(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
    g_print("button_press_cb()....\n");
    // return TRUE;        //stop propagate
    return FALSE;        //propagate
}               

static gboolean button_release_cb(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
    g_print("button_release_cb()....\n");
    // return TRUE;        //stop propagate
    return FALSE;        //propagate
}               

static void toggled_cb(GtkToggleButton *button, gpointer   user_data)
{
    g_print("toggled_cb(), the cur state =%s\n", gtk_toggle_button_get_active(button)? "TRUE":"FALSE");

}               

static void item1_activated (GSimpleAction *action,
                  GVariant      *parameter,
                  gpointer       user_data)
{
  GtkWindow *parent = user_data;
  GtkWidget *dialog;

  dialog = gtk_message_dialog_new (parent,
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_INFO,
                                   GTK_BUTTONS_CLOSE,
                                   "Activated action `%s`",
                                   "item1");

  g_signal_connect_swapped (dialog, "response",
                            G_CALLBACK (gtk_widget_destroy), dialog);

  gtk_widget_show_all (dialog);
}

static void item2_activated (GSimpleAction *action,
                  GVariant      *parameter,
                  gpointer       user_data)
{
  GtkWindow *parent = user_data;
  GtkWidget *dialog;

  dialog = gtk_message_dialog_new (parent,
                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_INFO,
                                   GTK_BUTTONS_CLOSE,
                                   "Activated action `%s`",
                                   "item2");

  g_signal_connect_swapped (dialog, "response",
                            G_CALLBACK (gtk_widget_destroy), dialog);

  gtk_widget_show_all (dialog);
}


static GActionEntry menubutton_entries[] = {
  { "do_item1", item1_activated },
  { "do_item2", item2_activated }
};

const gchar *menu_ui =
  "<interface>"
  "  <menu id='menu1'>"
  "    <section>"
  "      <item>"
  "        <attribute name='label'>_item1</attribute>"
  "        <attribute name='action'>do_item1</attribute>"
  "      </item>"
  "      <item>"
  "        <attribute name='label'>_item2</attribute>"
  "        <attribute name='action'>do_item2</attribute>"
  "      </item>"
  "    </section>"
  "  </menu>"
  "  <menu id='menu2'>"
  "    <section>"
  "      <item>"
  "        <attribute name='label'>_item1</attribute>"
  "        <attribute name='action'>do_item1</attribute>"
  "      </item>"
  "      <item>"
  "        <attribute name='label'>_item2</attribute>"
  "        <attribute name='action'>do_item2</attribute>"
  "      </item>"
  "    </section>"
  "  </menu>"
  "</interface>";


void do_button(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* flowbox;
    GtkWidget* image;
    GtkWidget* button;

    frame = gtk_frame_new("Button");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    flowbox = gtk_flow_box_new();
    gtk_widget_set_halign (flowbox, GTK_ALIGN_START);
    gtk_widget_set_valign (flowbox, GTK_ALIGN_START);
    gtk_flow_box_set_homogeneous(GTK_FLOW_BOX(flowbox), FALSE);
    gtk_flow_box_set_min_children_per_line (GTK_FLOW_BOX (flowbox), 4);
    gtk_flow_box_set_max_children_per_line (GTK_FLOW_BOX (flowbox), 4);
    // gtk_flow_box_set_max_children_per_line (GTK_FLOW_BOX (flowbox), 3);
    // gtk_orientable_set_orientation(GTK_ORIENTABLE(flowbox), GTK_ORIENTATION_HORIZONTAL);
    gtk_flow_box_set_selection_mode (GTK_FLOW_BOX (flowbox), GTK_SELECTION_NONE);    
    gtk_container_set_border_width(GTK_CONTAINER(flowbox), 5);    
    gtk_container_add(GTK_CONTAINER(frame), flowbox);



    button = gtk_button_new_with_label("Button");
    // gtk_flow_box_insert(GTK_FLOW_BOX(flowbox), button, -1);
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "clicked", G_CALLBACK(clicked_cb), button);
    g_signal_connect(button, "button-press-event", G_CALLBACK(button_press_cb), button);
    g_signal_connect(button, "button-release-event", G_CALLBACK(button_release_cb), button);

    button = gtk_button_new_with_mnemonic ("_Mnemonic");
    // gtk_flow_box_insert(GTK_FLOW_BOX(flowbox), button, -1);
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "clicked", G_CALLBACK(clicked_cb), button);

    button = gtk_button_new_from_icon_name("face-surprise", GTK_ICON_SIZE_BUTTON);
    gtk_flow_box_insert(GTK_FLOW_BOX(flowbox), button, -1);
    g_signal_connect(button, "clicked", G_CALLBACK(clicked_cb), button);


    button = gtk_button_new();
    image = gtk_image_new_from_resource("/a05_widgetDemo/face-smile.png");
    gtk_container_add(GTK_CONTAINER(button), image);
    gtk_flow_box_insert(GTK_FLOW_BOX(flowbox), button, -1);
    g_signal_connect(button, "clicked", G_CALLBACK(clicked_cb), button);

    button = gtk_button_new();
    GtkWidget *hbox;
    GtkWidget *label;
    hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    image = gtk_image_new_from_icon_name("face-surprise", GTK_ICON_SIZE_BUTTON);
    label = gtk_label_new("suprise");
    gtk_box_pack_start(GTK_BOX(hbox), image, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
    gtk_container_add(GTK_CONTAINER(button), hbox);
    gtk_flow_box_insert(GTK_FLOW_BOX(flowbox), button, -1);
    g_signal_connect(button, "clicked", G_CALLBACK(clicked_cb), button);


    frame = gtk_frame_new("Toggle Button");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    flowbox = gtk_flow_box_new();
    gtk_widget_set_halign (flowbox, GTK_ALIGN_START);
    gtk_widget_set_valign (flowbox, GTK_ALIGN_START);
    gtk_flow_box_set_homogeneous(GTK_FLOW_BOX(flowbox), FALSE);
    gtk_flow_box_set_min_children_per_line (GTK_FLOW_BOX (flowbox), 4);
    gtk_flow_box_set_max_children_per_line (GTK_FLOW_BOX (flowbox), 4);
    gtk_flow_box_set_selection_mode (GTK_FLOW_BOX (flowbox), GTK_SELECTION_NONE);    
    gtk_container_set_border_width(GTK_CONTAINER(flowbox), 5);    
    gtk_container_add(GTK_CONTAINER(frame), flowbox);


    button = gtk_toggle_button_new_with_label("Toggle Button");
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    button = gtk_check_button_new_with_label("Check Button");
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    button = gtk_check_button_new_with_label("Check Button with FALSE draw-indicator");
    gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(button), FALSE);
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    //menu button

    GSimpleActionGroup *menubutton_actions;
    GtkBuilder *builder;
    GMenuModel *menu1;
    GMenuModel *menu2;
    GMenu *button_menu;
    GMenuItem *section;
    
    menubutton_actions = g_simple_action_group_new ();
    g_action_map_add_action_entries (G_ACTION_MAP (menubutton_actions), menubutton_entries, G_N_ELEMENTS (menubutton_entries), NULL);

    builder = gtk_builder_new ();
    gtk_builder_add_from_string (builder, menu_ui, -1, NULL);

    menu1 = G_MENU_MODEL (gtk_builder_get_object (builder, "menu1"));
    menu2 = G_MENU_MODEL (gtk_builder_get_object (builder, "menu2"));

    button_menu = g_menu_new ();

    section = g_menu_item_new_section (NULL, menu1);
    g_menu_item_set_attribute (section, "action-namespace", "s", "menubutton-namespace");
    g_menu_append_item (button_menu, section);
    g_object_unref (section);

    section = g_menu_item_new_section (NULL, menu2);
    g_menu_item_set_attribute (section, "action-namespace", "s", "menubutton-namespace");
    g_menu_append_item (button_menu, section);
    g_object_unref (section);


    button = gtk_menu_button_new();
    gtk_button_set_label(GTK_BUTTON(button), "Menu");
    gtk_widget_insert_action_group(frame, "menubutton-namespace", G_ACTION_GROUP (menubutton_actions));
    // gtk_widget_insert_action_group (button, "menubutton-namespace", G_ACTION_GROUP (menubutton_actions));
    gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (button), G_MENU_MODEL (button_menu));
    gtk_widget_set_halign (GTK_WIDGET (button), GTK_ALIGN_CENTER);  //控制popup menu的位置
    gtk_widget_set_valign (GTK_WIDGET (button), GTK_ALIGN_START);
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    g_object_unref (button_menu);
    g_object_unref (builder);
    g_object_unref (menubutton_actions);

    // radio buttons

    button = gtk_radio_button_new_with_label(NULL, "radio 1");
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    button = gtk_radio_button_new_with_label(gtk_radio_button_get_group(GTK_RADIO_BUTTON(button)), "radio 2");    
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    button = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(button), "radio 3");
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    button = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(button), "radio 4");
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    //radio buttons 2

    button = gtk_radio_button_new_with_label(NULL, "radio 1");
    gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(button), FALSE);
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    button = gtk_radio_button_new_with_label(gtk_radio_button_get_group(GTK_RADIO_BUTTON(button)), "radio 2");    
    gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(button), FALSE);
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    button = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(button), "radio 3");
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);
    gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(button), FALSE);
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    button = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(button), "radio 4");
    gtk_container_add(GTK_CONTAINER(flowbox), button);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_cb), button);

    //color button 

    GdkRGBA color={1.0,0,0,1.0};  //red
    button = gtk_color_button_new_with_rgba(&color);
    gtk_container_add(GTK_CONTAINER(flowbox), button);

    //font button

    button = gtk_font_button_new();
    gtk_container_add(GTK_CONTAINER(flowbox), button);

    //scale button
    const gchar* icons[]={"audio-volume-muted"
                          , "audio-volume-high"
                          , "audio-volume-low"
                          , "audio-volume-medium"
                          , NULL};
    button = gtk_scale_button_new(GTK_ICON_SIZE_BUTTON, 0, 100, 10, icons);
    gtk_container_add(GTK_CONTAINER(flowbox), button);

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"


static GtkWidget *createbb (gint  horizontal, char *title, gint  spacing, gint  layout)
{
    GtkWidget *frame;
    GtkWidget *bbox;
    GtkWidget *button;

    frame = gtk_frame_new(title);
    bbox = gtk_button_box_new(horizontal ? GTK_ORIENTATION_HORIZONTAL : GTK_ORIENTATION_VERTICAL);
    gtk_container_set_border_width(GTK_CONTAINER(bbox), 5);
    gtk_container_add(GTK_CONTAINER(frame), bbox);

    gtk_button_box_set_layout(GTK_BUTTON_BOX(bbox), layout);
    gtk_box_set_spacing (GTK_BOX (bbox), spacing);

    button = gtk_button_new_with_label (_("OK"));
    gtk_container_add (GTK_CONTAINER (bbox), button);

    button = gtk_button_new_with_label (_("Cancel"));
    gtk_container_add (GTK_CONTAINER (bbox), button);

    button = gtk_button_new_with_label (_("Help"));
    gtk_container_add (GTK_CONTAINER (bbox), button);

    return frame;
}



void do_button_box(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* hbox;

    frame = gtk_frame_new("Horizontal Button Box");
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);

    gtk_box_pack_start(GTK_BOX(vbox), createbb(TRUE, "spread", 40, GTK_BUTTONBOX_SPREAD), TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), createbb(TRUE, "Edge", 40, GTK_BUTTONBOX_EDGE), TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), createbb(TRUE, "Start", 40, GTK_BUTTONBOX_START), TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), createbb(TRUE, "End", 40, GTK_BUTTONBOX_END), TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), createbb(TRUE, "Center", 40, GTK_BUTTONBOX_CENTER), TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), createbb(TRUE, "Expand", 0, GTK_BUTTONBOX_EXPAND), TRUE, TRUE, 5);

    frame = gtk_frame_new("Vertical Button Box");
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(hbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), hbox);

    gtk_box_pack_start(GTK_BOX(hbox), createbb(FALSE, "spread", 40, GTK_BUTTONBOX_SPREAD), TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), createbb(FALSE, "Edge", 40, GTK_BUTTONBOX_EDGE), TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), createbb(FALSE, "Start", 40, GTK_BUTTONBOX_START), TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), createbb(FALSE, "End", 40, GTK_BUTTONBOX_END), TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), createbb(FALSE, "Center", 40, GTK_BUTTONBOX_CENTER), TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), createbb(FALSE, "Expand", 0, GTK_BUTTONBOX_EXPAND), TRUE, TRUE, 5);

    gtk_widget_show_all(GTK_WIDGET(parent));
}

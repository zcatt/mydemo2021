#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"


static gchar* calendar_detail_cb (GtkCalendar *calendar,
                    guint        year,
                    guint        month,
                    guint        day,
                    gpointer     data)
{
  if(day == 15)
    return g_strdup("月中");
  return NULL;
}

void do_calendar(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* calendar;

    frame = gtk_frame_new("calendar");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    calendar = gtk_calendar_new();
    gtk_calendar_mark_day(GTK_CALENDAR(calendar), 15);
    gtk_calendar_set_detail_func (GTK_CALENDAR (calendar),
                                  calendar_detail_cb, NULL, NULL);

    gtk_widget_set_halign (calendar, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (calendar, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), calendar, FALSE, FALSE, 5);

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
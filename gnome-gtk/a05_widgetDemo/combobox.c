#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"


enum
{
  ICON_NAME_COL,
  TEXT_COL
};

static GtkTreeModel *create_icon_store (void)
{
  const gchar *icon_names[6] = {
    "dialog-warning",
    "process-stop",
    "document-new",
    "edit-clear",
    NULL,
    "document-open"
  };
  const gchar *labels[6] = {
    N_("Warning"),
    N_("Stop"),
    N_("New"),
    N_("Clear"),
    NULL,
    N_("Open")
  };

  GtkTreeIter iter;
  GtkListStore *store;
  gint i;

  store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);

  for (i = 0; i < G_N_ELEMENTS (icon_names); i++)
    {
      if (icon_names[i])
        {
          gtk_list_store_append (store, &iter);
          gtk_list_store_set (store, &iter,
                              ICON_NAME_COL, icon_names[i],
                              TEXT_COL, _(labels[i]),
                              -1);
        }
      else
        {
          gtk_list_store_append (store, &iter);
          gtk_list_store_set (store, &iter,
                              ICON_NAME_COL, NULL,
                              TEXT_COL, "separator",
                              -1);
        }
    }

  return GTK_TREE_MODEL (store);
}


static void
set_sensitive (GtkCellLayout   *cell_layout,
               GtkCellRenderer *cell,
               GtkTreeModel    *tree_model,
               GtkTreeIter     *iter,
               gpointer         data)
{
  GtkTreePath *path;
  gint *indices;
  gboolean sensitive;

  path = gtk_tree_model_get_path (tree_model, iter);
  indices = gtk_tree_path_get_indices (path);
  sensitive = indices[0] != 1;                //'stop' item
  gtk_tree_path_free (path);

  g_object_set (cell, "sensitive", sensitive, NULL);
}


static gboolean
is_separator (GtkTreeModel *model,
              GtkTreeIter  *iter,
              gpointer      data)
{
  GtkTreePath *path;
  gboolean result;

  path = gtk_tree_model_get_path (model, iter);
  result = gtk_tree_path_get_indices (path)[0] == 4;
  gtk_tree_path_free (path);

  return result;
}




void do_combobox(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* combo;
    GtkTreeModel *model;
    GtkCellRenderer *renderer;


    frame = gtk_frame_new("combobox");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);

    model = create_icon_store ();
    combo = gtk_combo_box_new_with_model(model);
    g_object_unref (model);                         //GtkListStore不是GInitiallyUnowned, 没有sink机制. 故需unref

    gtk_widget_set_halign (combo, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (combo, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), combo, FALSE, FALSE, 5);

    renderer = gtk_cell_renderer_pixbuf_new ();
    gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, FALSE);
    gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
                                    "icon-name", ICON_NAME_COL,         //used by GtkCellRendererPixbuf
                                    NULL);

    gtk_cell_layout_set_cell_data_func (GTK_CELL_LAYOUT (combo),
                                        renderer,
                                        set_sensitive,
                                        NULL, NULL);

    renderer = gtk_cell_renderer_text_new ();
    gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
    gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
                                    "text", TEXT_COL,               //used by GtkCellRendererText.
                                    NULL);

    gtk_cell_layout_set_cell_data_func (GTK_CELL_LAYOUT (combo),
                                        renderer,
                                        set_sensitive,
                                        NULL, NULL);

    gtk_combo_box_set_row_separator_func (GTK_COMBO_BOX (combo),
                                          is_separator, NULL, NULL);

    gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 0);



    gtk_widget_show_all(GTK_WIDGET(parent));    
}
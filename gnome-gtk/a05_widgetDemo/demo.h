
typedef void (*DemoFunc)(GtkBox *box);


typedef struct 
{
    gchar* parent;
    gchar* name;
    gchar* title;
    DemoFunc func;
} DemoInfo;

//gtkTreeView的treeModel
enum 
{
    NAME_COLUMN,
    TITLE_COLUMN,
    FUNC_COLUMN,
    STYLE_COLUMN,
    NUM_COLUMN
};

void do_button_box(GtkBox *parent);
void do_label(GtkBox *parent);
void do_image(GtkBox *parent);
void do_spinner(GtkBox *parent);
void do_infobar(GtkBox *parent);
void do_button(GtkBox *parent);
void do_progressbar(GtkBox *parent);
void do_levelbar(GtkBox *parent);
void do_statusbar(GtkBox *parent);
void do_entry(GtkBox *parent);
void do_scale(GtkBox *parent);
void do_scrollbar(GtkBox *parent);
void do_spinbutton(GtkBox *parent);
void do_calendar(GtkBox *parent);
void do_dialogs(GtkBox *parent);
void do_combobox(GtkBox *parent);
void do_treeview(GtkBox *parent);
void do_popover(GtkBox *parent);

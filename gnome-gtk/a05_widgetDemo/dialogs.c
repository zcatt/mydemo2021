#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

static void color_set_cb (GtkColorButton *button, gpointer data)
{
    GdkRGBA color;

    gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (button), &color);
    GtkWidget* message_dialog = gtk_message_dialog_new (NULL,
                                        GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_MESSAGE_INFO,
                                        GTK_BUTTONS_OK,
                                        "Color you choosed: rgba(%f, %f, %f, %f)",
                                        color.red,
                                        color.green,
                                        color.blue,
                                        color.alpha);
    gtk_dialog_run (GTK_DIALOG (message_dialog));
    gtk_widget_destroy (message_dialog);
}


static void response_cb (GtkDialog *dialog,
             gint       response_id,
             gpointer   user_data)
{
    if (response_id == GTK_RESPONSE_OK)
    {
        GdkRGBA color;

        gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (dialog), &color);
        GtkWidget* message_dialog = gtk_message_dialog_new (NULL,
                                            GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                            GTK_MESSAGE_INFO,
                                            GTK_BUTTONS_OK,
                                            "Color you choosed: rgba(%f, %f, %f, %f)",
                                            color.red,
                                            color.green,
                                            color.blue,
                                            color.alpha);
        gtk_dialog_run (GTK_DIALOG (message_dialog));
        gtk_widget_destroy (message_dialog);
        // g_signal_connect (message_dialog, "response",   G_CALLBACK (gtk_widget_destroy), NULL);
        // gtk_widget_show (message_dialog);
    }

    gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void color_chooser_cb (GtkWidget *button, gpointer data)
{
    GtkWidget *dialog;
    GdkRGBA color={1.0, 0, 0, 1.0};
    
    dialog = gtk_color_chooser_dialog_new ("Changing color", GTK_WINDOW(gtk_widget_get_toplevel(button)));
    gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
    gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (dialog), &color);
    g_signal_connect (dialog, "response",  G_CALLBACK (response_cb), NULL);

    gtk_widget_show_all (dialog);
}


static void file_set_cb (GtkFileChooserButton *button, gpointer data)
{
    gchar *file_name = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(button));
    

    GtkWidget* message_dialog = gtk_message_dialog_new (NULL,
                                        GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_MESSAGE_INFO,
                                        GTK_BUTTONS_OK,
                                        "File you choosed: %s",
                                        file_name);
    gtk_dialog_run (GTK_DIALOG (message_dialog));
    gtk_widget_destroy (message_dialog);

    g_free(file_name);
}

// static void file_chooser_cb (GtkWidget *button, gpointer data)
// {
//     GtkWidget *dialog;
//     gint response;    
//     dialog = gtk_file_chooser_dialog_new  ("Open file"
//                                     , GTK_WINDOW(gtk_widget_get_toplevel(button))
//                                     , GTK_FILE_CHOOSER_ACTION_OPEN
//                                     , "_Cancel", GTK_RESPONSE_CANCEL
//                                     , "_Open", GTK_RESPONSE_OK
//                                     , NULL
//                                     );
//     gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
//     response = gtk_dialog_run (GTK_DIALOG (dialog));
//     if (response == GTK_RESPONSE_OK)
//     {
//         gchar* save_filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

//         GtkWidget* message_dialog = gtk_message_dialog_new (NULL,
//                                             GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
//                                             GTK_MESSAGE_INFO,
//                                             GTK_BUTTONS_OK,
//                                             "File you selected: %s",
//                                             save_filename);
//         gtk_dialog_run (GTK_DIALOG (message_dialog));
//         gtk_widget_destroy (message_dialog);

//         g_free (save_filename);

//     }

//     gtk_widget_destroy (GTK_WIDGET (dialog));
// }


void do_dialogs(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* button;

    frame = gtk_frame_new("dialogs");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);

    GdkRGBA color={1.0, 0, 0, 1.0};
    button = gtk_color_button_new_with_rgba(&color);
    gtk_widget_set_halign (button, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);
    g_signal_connect(button, "color-set", G_CALLBACK(color_set_cb), NULL);


    button = gtk_button_new_with_label("color chooser dialog");
    gtk_widget_set_halign (button, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);
    g_signal_connect(button, "clicked", G_CALLBACK(color_chooser_cb), NULL);

    button = gtk_file_chooser_button_new("Open file", GTK_FILE_CHOOSER_ACTION_OPEN);
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(button), "~");
    gtk_widget_set_halign (button, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);

    GtkFileFilter* filter = gtk_file_filter_new ();
    gtk_file_filter_set_name (filter, "c");
    gtk_file_filter_add_pattern (filter, "*.c");
    gtk_file_filter_add_pattern (filter, "*.h");
    gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(button), filter);
    g_signal_connect(button, "file-set", G_CALLBACK(file_set_cb), NULL);


    // button = gtk_button_new_with_label("file chooser dialog");
    // gtk_widget_set_halign (button, GTK_ALIGN_CENTER);
    // gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
    // gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);
    // g_signal_connect(button, "clicked", G_CALLBACK(file_chooser_cb), NULL);

    button = gtk_font_button_new ();
    gtk_widget_set_halign (button, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
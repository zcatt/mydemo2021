#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

static void inserted_text_cb (GtkEntryBuffer *buffer,
               guint           position,
               char           *chars,
               guint           n_chars,
               gpointer        user_data)
{
    GtkWidget *label = user_data;
    gchar txt[64];

    g_print("insert_at_cursor_cb()....\n");
    sprintf(txt, "count: %d", gtk_entry_buffer_get_length(buffer));
    gtk_label_set_label(GTK_LABEL(label), txt);
}

static void deleted_text_cb (GtkEntryBuffer *buffer,
               guint           position,
               guint           n_chars,
               gpointer        user_data)
{
    GtkWidget *label = user_data;
    gchar txt[64];

    g_print("insert_at_cursor_cb()....\n");
    sprintf(txt, "count: %d", gtk_entry_buffer_get_length(buffer));
    gtk_label_set_label(GTK_LABEL(label), txt);
}



static GtkTreeModel *create_completion_model (void)
{
  GtkListStore *store;
  GtkTreeIter iter;
  
  store = gtk_list_store_new (1, G_TYPE_STRING);

  /* Append one word */
  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, 0, "GNOME", -1);

  /* Append another word */
  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, 0, "total", -1);

  /* And another word */
  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, 0, "totally", -1);

  return GTK_TREE_MODEL (store);
}

void do_entry(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* entry;
    GtkWidget* label;
    GtkEntryBuffer* buf;


    frame = gtk_frame_new("simple entry");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    entry = gtk_entry_new();
    gtk_widget_set_halign (entry, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (entry, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 5);

    label = gtk_label_new("count: 0");
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);

    buf = gtk_entry_get_buffer(GTK_ENTRY(entry));
    g_signal_connect(buf, "inserted-text", G_CALLBACK(inserted_text_cb), label);
    g_signal_connect(buf, "deleted-text", G_CALLBACK(deleted_text_cb), label);

    frame = gtk_frame_new("invisibility entry");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    //通知input method输入的是password.
    g_object_set(G_OBJECT(entry), "input-purpose", GTK_INPUT_PURPOSE_PASSWORD, NULL);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    entry = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry), "input password here.");
    gtk_entry_set_visibility(GTK_ENTRY(entry), FALSE);
    gtk_entry_set_invisible_char (GTK_ENTRY(entry), '$');
    gtk_widget_set_halign (entry, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (entry, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 5);


    frame = gtk_frame_new("icon entry");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    entry = gtk_entry_new();
    gtk_entry_set_icon_from_icon_name(GTK_ENTRY (entry), GTK_ENTRY_ICON_PRIMARY, "edit-copy");
    gtk_entry_set_icon_tooltip_text(GTK_ENTRY (entry), GTK_ENTRY_ICON_PRIMARY, "tooltip text.");
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 5);

    entry = gtk_entry_new();
    gtk_entry_set_icon_from_icon_name(GTK_ENTRY (entry), GTK_ENTRY_ICON_SECONDARY, "edit-clear");
    gtk_entry_set_icon_tooltip_text(GTK_ENTRY (entry), GTK_ENTRY_ICON_SECONDARY, "tooltip text.");
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 5);



    frame = gtk_frame_new("two entres with one buf");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    entry = gtk_entry_new();
    gtk_widget_set_halign (entry, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (entry, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 5);

    buf = gtk_entry_get_buffer(GTK_ENTRY(entry));
 
    entry = gtk_entry_new_with_buffer(buf);
    gtk_widget_set_halign (entry, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (entry, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 5);


    frame = gtk_frame_new("entry completion");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    entry = gtk_entry_new();
    gtk_widget_set_halign (entry, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (entry, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 5);

    buf = gtk_entry_get_buffer(GTK_ENTRY(entry));
 
    GtkEntryCompletion *completion;
    completion = gtk_entry_completion_new();
    gtk_entry_completion_set_inline_completion(completion, TRUE);
    gtk_entry_set_completion(GTK_ENTRY(entry), completion);

    GtkTreeModel *completion_model;
    completion_model = create_completion_model();
    gtk_entry_completion_set_model(completion, completion_model);
    g_object_unref(completion_model);

    /* Use model column 0 as the text column */
    gtk_entry_completion_set_text_column (completion, 0);


    label = gtk_label_new("completion words: GNOME, total, totally");
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);



    gtk_widget_show_all(GTK_WIDGET(parent));    
}
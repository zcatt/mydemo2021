#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"


void do_image(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* image;

    frame = gtk_frame_new("Image");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 0);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    image = gtk_image_new_from_resource("/a05_widgetDemo/face-smile.png");
    gtk_box_pack_start(GTK_BOX(vbox), image, FALSE, FALSE, 5);

    frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
    gtk_widget_set_halign (frame, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (frame, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 5);

    image = gtk_image_new_from_resource("/a05_widgetDemo/face-smile.png");
    gtk_container_add(GTK_CONTAINER(frame), image);


    gtk_widget_show_all(GTK_WIDGET(parent));    
}
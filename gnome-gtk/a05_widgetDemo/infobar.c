#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"


static void on_bar_response(GtkInfoBar *info_bar, gint response_id, gpointer user_data)
{
  GtkWidget *dialog;
  GtkWidget *window;

  if (response_id == GTK_RESPONSE_CLOSE)
    {
      gtk_widget_hide (GTK_WIDGET (info_bar));
      return;
    }

  window = gtk_widget_get_toplevel (GTK_WIDGET (info_bar));
  dialog = gtk_message_dialog_new (GTK_WINDOW (window),
                                   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                   GTK_MESSAGE_INFO,
                                   GTK_BUTTONS_OK,
                                   "You clicked a button on an info bar");
  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                            "Your response has id %d", response_id);

  g_signal_connect_swapped (dialog,
                            "response",
                            G_CALLBACK (gtk_widget_destroy),
                            dialog);

  gtk_widget_show_all (dialog);
}

void do_infobar(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* action_box;
    GtkWidget* infobar;    
    GtkWidget* label;
    GtkWidget* button;

    frame = gtk_frame_new("infoBar");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
    // gtk_box_set_homogeneous(GTK_BOX(vbox), FALSE);
    gtk_container_add(GTK_CONTAINER(frame), vbox);

    action_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);


    //info

    infobar = gtk_info_bar_new();
    gtk_box_pack_start(GTK_BOX(vbox), infobar, FALSE, FALSE, 5);
    gtk_info_bar_set_message_type(GTK_INFO_BAR(infobar), GTK_MESSAGE_INFO);
    label = gtk_label_new("This is an info bar with GTK_MESSAGE_INFO type.");
    gtk_label_set_xalign(GTK_LABEL(label), 0);
    gtk_box_pack_start(GTK_BOX(gtk_info_bar_get_content_area(GTK_INFO_BAR(infobar))), label, FALSE, FALSE,0);

    button = gtk_toggle_button_new_with_label ("Info");
    g_object_bind_property (button, "active", infobar, "visible", G_BINDING_BIDIRECTIONAL);
    gtk_container_add (GTK_CONTAINER(action_box), button);    


    //warning

    infobar = gtk_info_bar_new();
    gtk_box_pack_start(GTK_BOX(vbox), infobar, FALSE, FALSE, 5);
    gtk_info_bar_set_message_type(GTK_INFO_BAR(infobar), GTK_MESSAGE_WARNING);
    label = gtk_label_new("This is an info bar with GTK_MESSAGE_WARNING type.");
    gtk_label_set_xalign(GTK_LABEL(label), 0);
    gtk_box_pack_start(GTK_BOX(gtk_info_bar_get_content_area(GTK_INFO_BAR(infobar))), label, FALSE, FALSE,0);

    button = gtk_toggle_button_new_with_label ("Warning");
    g_object_bind_property (button, "active", infobar, "visible", G_BINDING_BIDIRECTIONAL);
    gtk_container_add (GTK_CONTAINER(action_box), button);    

    //question

    infobar = gtk_info_bar_new_with_buttons("_Ok", GTK_RESPONSE_OK, NULL);
    gtk_info_bar_set_show_close_button(GTK_INFO_BAR(infobar), TRUE);
    g_signal_connect(infobar, "response", G_CALLBACK (on_bar_response), NULL);
    gtk_box_pack_start(GTK_BOX(vbox), infobar, FALSE, FALSE, 5);
    gtk_info_bar_set_message_type(GTK_INFO_BAR(infobar), GTK_MESSAGE_QUESTION);
    label = gtk_label_new("This is an info bar with GTK_MESSAGE_QUESTION type.");
    gtk_label_set_xalign(GTK_LABEL(label), 0);
    gtk_box_pack_start(GTK_BOX(gtk_info_bar_get_content_area(GTK_INFO_BAR(infobar))), label, FALSE, FALSE,0);

    button = gtk_toggle_button_new_with_label ("Question");
    g_object_bind_property (button, "active", infobar, "visible", G_BINDING_BIDIRECTIONAL);
    gtk_container_add (GTK_CONTAINER(action_box), button);    

    //error

    infobar = gtk_info_bar_new();
    gtk_box_pack_start(GTK_BOX(vbox), infobar, FALSE, FALSE, 5);
    gtk_info_bar_set_message_type(GTK_INFO_BAR(infobar), GTK_MESSAGE_ERROR);
    label = gtk_label_new("This is an info bar with GTK_MESSAGE_ERROR type.");
    gtk_label_set_xalign(GTK_LABEL(label), 0);
    gtk_box_pack_start(GTK_BOX(gtk_info_bar_get_content_area(GTK_INFO_BAR(infobar))), label, FALSE, FALSE,0);

    button = gtk_toggle_button_new_with_label ("Error");
    g_object_bind_property (button, "active", infobar, "visible", G_BINDING_BIDIRECTIONAL);
    gtk_container_add (GTK_CONTAINER(action_box), button);    

    //other

    infobar = gtk_info_bar_new();
    gtk_box_pack_start(GTK_BOX(vbox), infobar, FALSE, FALSE, 5);
    gtk_info_bar_set_message_type(GTK_INFO_BAR(infobar), GTK_MESSAGE_OTHER);
    label = gtk_label_new("This is an info bar with GTK_MESSAGE_OTHER type.");
    gtk_label_set_xalign(GTK_LABEL(label), 0);
    gtk_box_pack_start(GTK_BOX(gtk_info_bar_get_content_area(GTK_INFO_BAR(infobar))), label, FALSE, FALSE,0);

    button = gtk_toggle_button_new_with_label ("Other");
    g_object_bind_property (button, "active", infobar, "visible", G_BINDING_BIDIRECTIONAL);
    gtk_container_add (GTK_CONTAINER(action_box), button);    
    

    //actions section

    frame = gtk_frame_new ("Info bars");
    gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 8);

    GtkWidget *vbox2;
    vbox2 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 8);
    gtk_container_set_border_width (GTK_CONTAINER (vbox2), 8);
    gtk_container_add (GTK_CONTAINER (frame), vbox2);

    /* Standard message dialog */
    label = gtk_label_new ("An example of different info bars");
    gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);

    // gtk_widget_show_all (action_box);
    gtk_box_pack_start (GTK_BOX (vbox2), action_box, FALSE, FALSE, 0);


    gtk_widget_show_all(GTK_WIDGET(parent));    
}
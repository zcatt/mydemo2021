#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

static gboolean activate_link_cb(GtkLabel *label, char *uri, gpointer user_data)
{
    g_print("activate link: %s\n", uri);
    return TRUE;    //the link is activated.
}

void do_label(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* label;

    frame = gtk_frame_new("Label");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 0);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    label = gtk_label_new("Normal Label");
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);

    frame = gtk_frame_new(NULL);
    gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
    label = gtk_label_new("Normal Label in Frame");
    gtk_label_set_max_width_chars(GTK_LABEL(label), 20);
    gtk_container_add(GTK_CONTAINER(frame), label);
    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 5);

    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), "<small>styled text</small>");
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);

    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label)
            , "Goto <a href=\"http://www.gtk.org\" title=\"<i>Our</i> website\">GTK web site</a> for more...");
    g_signal_connect(label, "activate-link", G_CALLBACK(activate_link_cb), NULL);
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);

    label = gtk_label_new_with_mnemonic("_Mnemonic Label");
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);

    label = gtk_label_new_with_mnemonic("Selected Label");
    gtk_label_set_selectable(GTK_LABEL(label), TRUE);
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);

    label = gtk_label_new_with_mnemonic("30 angle Label");
    gtk_label_set_angle (GTK_LABEL(label), 30.0);
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 5);



    PangoAttrList *attrs;
    PangoAttribute *attribute;

    attrs = pango_attr_list_new ();
    attribute = pango_attr_foreground_new (0xffff,
                                            0xfff,
                                            0);
    pango_attr_list_insert (attrs, attribute);                                            
    attribute = pango_attr_background_new (0,
                                            0,
                                            0xffff);
    pango_attr_list_insert(attrs, attribute);              

    label = gtk_label_new("pango attr Label");
    gtk_label_set_attributes(GTK_LABEL(label), attrs);
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

    frame = gtk_frame_new(NULL);
    gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
    label = gtk_label_new("pango attr Label in Frame");
    gtk_label_set_attributes(GTK_LABEL(label), attrs);
    gtk_container_add(GTK_CONTAINER(frame), label);
    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 5);


    pango_attr_list_unref(attrs);

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
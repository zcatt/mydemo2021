#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"



void do_levelbar(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* lbar;

    frame = gtk_frame_new("levelbar");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    // lbar = gtk_level_bar_new();
    lbar = gtk_level_bar_new_for_interval(0,1.0);
    gtk_level_bar_set_value(GTK_LEVEL_BAR(lbar), 0.75);
    // gtk_widget_set_halign (lbar, GTK_ALIGN_CENTER);
    // gtk_widget_set_valign (lbar, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), lbar, FALSE, FALSE, 5);

    //离散型以1为单位,即指示器的一节.
    lbar = gtk_level_bar_new_for_interval(0,10);
    gtk_level_bar_set_value(GTK_LEVEL_BAR(lbar), 6);
    gtk_level_bar_set_mode(GTK_LEVEL_BAR(lbar), GTK_LEVEL_BAR_MODE_DISCRETE);
    // gtk_widget_set_halign (lbar, GTK_ALIGN_CENTER);
    // gtk_widget_set_valign (lbar, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), lbar, FALSE, FALSE, 5);

    gdouble value;
    gtk_level_bar_get_offset_value(GTK_LEVEL_BAR(lbar), GTK_LEVEL_BAR_OFFSET_LOW, &value);
    g_print("GTK_LEVEL_BAR_OFFSET_LOW value=%f\n", value);
    gtk_level_bar_get_offset_value(GTK_LEVEL_BAR(lbar), GTK_LEVEL_BAR_OFFSET_HIGH, &value);
    g_print("GTK_LEVEL_BAR_OFFSET_HIGH value=%f\n", value);
    gtk_level_bar_get_offset_value(GTK_LEVEL_BAR(lbar), GTK_LEVEL_BAR_OFFSET_FULL, &value);
    g_print("GTK_LEVEL_BAR_OFFSET_FULL value=%f\n", value);


    gtk_widget_show_all(GTK_WIDGET(parent));    
}
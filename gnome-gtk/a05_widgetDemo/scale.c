#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"



void do_scale(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* scale;


    frame = gtk_frame_new("scale");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    scale = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1.0, 0.1);    
    gtk_scale_set_digits(GTK_SCALE(scale), 2);  //number of decimal
    gtk_scale_set_value_pos(GTK_SCALE(scale), GTK_POS_TOP);
    gtk_scale_set_draw_value(GTK_SCALE(scale), TRUE);
    gtk_range_set_value(GTK_RANGE(scale), .5);
    gtk_widget_set_size_request(scale, 100, -1);
    gtk_widget_set_halign (scale, GTK_ALIGN_FILL);
    // gtk_widget_set_valign (scale, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), scale, FALSE, FALSE, 5);

    scale = gtk_scale_new_with_range(GTK_ORIENTATION_VERTICAL, 0, 1.0, 0.1);
    gtk_scale_set_digits(GTK_SCALE(scale), 2);  //number of decimal
    gtk_scale_set_value_pos(GTK_SCALE(scale), GTK_POS_RIGHT);
    gtk_scale_set_draw_value(GTK_SCALE(scale), TRUE);
    gtk_range_set_value(GTK_RANGE(scale), 0.5);
    gtk_widget_set_size_request(scale, -1, 100);
    gtk_widget_set_halign (scale, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (scale, GTK_ALIGN_FILL);
    gtk_box_pack_start(GTK_BOX(vbox), scale, TRUE, TRUE, 5);


    gtk_widget_show_all(GTK_WIDGET(parent));    
}
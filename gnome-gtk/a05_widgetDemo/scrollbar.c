#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"



void do_scrollbar(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* scrollbar;
    GtkAdjustment* adj;


    frame = gtk_frame_new("scrollbar");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    adj = gtk_adjustment_new(0.5, 0, 1.0, 0.1, 0.2, 0.5);
    scrollbar = gtk_scrollbar_new(GTK_ORIENTATION_HORIZONTAL, adj);
    gtk_widget_set_size_request(scrollbar, 100, -1);
    gtk_widget_set_halign (scrollbar, GTK_ALIGN_FILL);
    // gtk_widget_set_valign (scrollbar, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), scrollbar, FALSE, FALSE, 5);

    scrollbar = gtk_scrollbar_new(GTK_ORIENTATION_VERTICAL, adj);
    gtk_widget_set_size_request(scrollbar, -1, 100);
    gtk_widget_set_halign (scrollbar, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (scrollbar, GTK_ALIGN_FILL);
    gtk_box_pack_start(GTK_BOX(vbox), scrollbar, TRUE, TRUE, 5);


    gtk_widget_show_all(GTK_WIDGET(parent));    
}
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"


void do_spinbutton(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* spinbutton;
    GtkAdjustment* adj;


    frame = gtk_frame_new("spinbutton");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    adj = gtk_adjustment_new (2.500, 0.0, 5.0, 0.001, 0.1, 0.0);
    spinbutton = gtk_spin_button_new(adj, 0.001, 3);
    gtk_widget_set_size_request(spinbutton, 100, -1);
    gtk_widget_set_halign (spinbutton, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (spinbutton, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), spinbutton, FALSE, FALSE, 5);

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"


static void on_play_clicked (GtkButton *button, gpointer user_data)
{
    GtkSpinner *spinner = GTK_SPINNER(user_data);
    gtk_spinner_start (spinner);
}

static void on_stop_clicked (GtkButton *button, gpointer user_data)
{
    GtkSpinner *spinner = user_data;
    gtk_spinner_stop(spinner);
}


void do_spinner(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* spinner;
    GtkWidget* button;

    frame = gtk_frame_new("spinner");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
    // gtk_box_set_homogeneous(GTK_BOX(vbox), FALSE);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    spinner = gtk_spinner_new ();
    gtk_box_pack_start(GTK_BOX(vbox), spinner, FALSE, FALSE, 5);

    // frame = gtk_frame_new(NULL);
    // gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
    // gtk_widget_set_halign (frame, GTK_ALIGN_CENTER);
    // gtk_widget_set_valign (frame, GTK_ALIGN_CENTER);
    // gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 5);

    button = gtk_button_new_with_label(_("Play"));
    g_signal_connect(button, "clicked",  G_CALLBACK(on_play_clicked), spinner);
    // gtk_widget_set_hexpand(button, FALSE);
    // gtk_widget_set_vexpand(button, FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);

    button = gtk_button_new_with_label(_("Stop"));
    g_signal_connect(button, "clicked",G_CALLBACK(on_stop_clicked), spinner);
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);


    gtk_widget_show_all(GTK_WIDGET(parent));    
}
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"



void do_statusbar(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* vbox;
    GtkWidget* sbar;


    frame = gtk_frame_new("statusbar");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox);


    sbar = gtk_statusbar_new();
    gtk_widget_set_halign (sbar, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (sbar, GTK_ALIGN_CENTER);
    gtk_box_pack_start(GTK_BOX(vbox), sbar, FALSE, FALSE, 5);

#if 0
    int context1;
    int context2;
    context1 = gtk_statusbar_get_context_id(GTK_STATUSBAR(sbar), "c1");
    context2 = gtk_statusbar_get_context_id(GTK_STATUSBAR(sbar), "c2");

    gtk_statusbar_push(GTK_STATUSBAR(sbar), context1, "This is context1.");
    gtk_statusbar_push(GTK_STATUSBAR(sbar), context2, "This is context2.");
#else
    gtk_statusbar_push(GTK_STATUSBAR(sbar), 0, "This is statusbar.");
#endif

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

static gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    guint width, height;
    // GdkRGBA color;
    GtkStyleContext *context;


    context = gtk_widget_get_style_context (widget);

    width = gtk_widget_get_allocated_width (widget);
    height = gtk_widget_get_allocated_height (widget);

    g_print("on_draw()....width=%d,height=%d\n", width, height);

    gtk_render_background (context, cr, 0, 0, width, height);


    draw_coord(cr, 0, 0);



    //png 

    cairo_surface_t *image;

    // image = cairo_image_surface_create_from_png ("hamburger.jpeg");
    image = cairo_image_surface_create_from_png ("face-smile.png");

    cairo_set_source_surface (cr, image, 100, 5);
    cairo_paint (cr);
    cairo_surface_destroy (image);


    //jpeg等pixbuf支持的图像格式

    GError *err=NULL;
    GdkPixbuf* pb;
    pb = gdk_pixbuf_new_from_file("hamburger.jpeg", &err);
    g_assert_no_error(err);

    gdk_cairo_set_source_pixbuf(cr, pb, 0,50);  //pixbuf作为source
    cairo_paint (cr);

    g_object_unref(pb);


    cairo_arc(cr, 200,200, 100, 0, 2*G_PI);
    cairo_set_source_rgba(cr, 0.5,0.5,1,1);
    cairo_set_line_width(cr, 5);
    cairo_stroke(cr);

    return FALSE;
}

void do_image(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* drawing_area;

    frame = gtk_frame_new("image");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    drawing_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(frame), drawing_area);

    g_signal_connect(drawing_area, "draw", G_CALLBACK (on_draw), NULL);        

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
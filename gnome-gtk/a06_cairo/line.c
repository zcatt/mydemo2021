#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <math.h>
// #include <cairo.h>
#include "demo.h"

static gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    guint width, height;
    // GdkRGBA color;
    GtkStyleContext *context;

    context = gtk_widget_get_style_context (widget);

    width = gtk_widget_get_allocated_width (widget);
    height = gtk_widget_get_allocated_height (widget);

    gtk_render_background (context, cr, 0, 0, width, height);


    cairo_move_to (cr, 50, 50);
    cairo_line_to (cr, 150, 50);
    cairo_move_to (cr, 150, 50);
    cairo_line_to (cr, 150, 100);
    cairo_move_to (cr, 150, 100);
    cairo_line_to (cr, 50, 50);

    cairo_set_line_width (cr, 5);
    cairo_set_source_rgba(cr, 1,0,0, 1);
    cairo_stroke (cr);

    cairo_move_to (cr, 200, 50);
    cairo_rel_line_to (cr, 100, 0);
    cairo_rel_line_to (cr, 0, 50);
    cairo_close_path(cr);

    cairo_set_line_width (cr, 5);
    cairo_set_source_rgba(cr, 0,1,0, 1);
    cairo_stroke (cr);

    //rectangle
    cairo_rectangle(cr, 400, 50, 100, 50);
    cairo_set_line_width (cr, 5);
    cairo_set_source_rgba(cr, 0,1,0, 1);
    cairo_stroke (cr);

    //arc
    cairo_move_to(cr, 50,150);
    cairo_arc(cr, 50, 150, 50, 0, 60* M_PI / 180);
    cairo_set_line_width (cr, 5);
    cairo_set_source_rgba(cr, 0,0,1, 1);
    cairo_stroke (cr);

    cairo_arc(cr, 200, 150, 50, 0, -60* M_PI / 180);
    cairo_set_line_width (cr, 5);
    cairo_set_source_rgba(cr, 0,0,1, 1);
    cairo_stroke (cr);

    cairo_arc_negative(cr, 350, 150, 50, 0, 60* M_PI / 180);
    cairo_set_line_width (cr, 5);
    cairo_set_source_rgba(cr, 0,0,1, 1);
    cairo_stroke (cr);

    cairo_arc_negative(cr, 500, 150, 50, 0, -60* M_PI / 180);
    cairo_set_line_width (cr, 5);
    cairo_set_source_rgba(cr, 0,0,1, 1);
    cairo_stroke (cr);


    //cubic bezier spline
    cairo_move_to(cr, 50, 300);
    cairo_curve_to(cr, 
                    100, 350,       //ctrl p1
                    200, 250,       //ctrl p2
                    250, 300        //end p
                    );
    cairo_set_line_width (cr, 5);
    cairo_set_source_rgba(cr, 0,0,0, 1);
    cairo_stroke (cr);

    cairo_move_to(cr, 50, 300);
    cairo_line_to(cr, 100, 350);
    cairo_set_line_width (cr, 1);
    cairo_set_source_rgba(cr, 1,0,0, 1);
    cairo_stroke (cr);

    cairo_move_to(cr, 250, 300);
    cairo_line_to(cr, 200, 250);
    cairo_set_line_width (cr, 1);
    cairo_set_source_rgba(cr, 1,0,0, 1);
    cairo_stroke (cr);

    //text path

    cairo_move_to(cr, 50, 500);
    cairo_set_font_size(cr, 200);
    cairo_text_path(cr, "a");
    cairo_path_t *path = cairo_copy_path(cr);


    //text bounding
    double t,l,b,r;
    cairo_path_extents(cr, &l, &t, &r, &b);
    cairo_new_path(cr);
    cairo_rectangle(cr, l, t, r-l, b-t);
    cairo_set_source_rgba(cr, 1,0,0, 1);
    cairo_stroke(cr);


    cairo_append_path(cr, path);
    cairo_path_destroy(path);
    cairo_set_line_width (cr, 1);
    cairo_set_source_rgba(cr, 0,0,1, 1);
    cairo_stroke (cr);

    // line dash
    cairo_save(cr);
    cairo_move_to(cr, 200, 500);
    cairo_rel_line_to(cr, 400, 0);
    double dashs[]={30,50,80, 10};
    cairo_set_dash(cr, dashs, 4, 0);

    cairo_set_line_width(cr, 5);
    cairo_stroke(cr);
    cairo_restore(cr);


    return FALSE;
}

void do_line(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* drawing_area;

    frame = gtk_frame_new("line");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    drawing_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(frame), drawing_area);
    // gtk_widget_set_size_request (drawing_area, 100, 100);

    g_signal_connect(drawing_area, "draw", G_CALLBACK (on_draw), NULL);    



    gtk_widget_show_all(GTK_WIDGET(parent));    
}
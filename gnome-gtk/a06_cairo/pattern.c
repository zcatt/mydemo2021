#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

static gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    guint width, height;
    // GdkRGBA color;
    GtkStyleContext *context;


    context = gtk_widget_get_style_context (widget);

    width = gtk_widget_get_allocated_width (widget);
    height = gtk_widget_get_allocated_height (widget);

    g_print("on_draw()....width=%d,height=%d\n", width, height);

    gtk_render_background (context, cr, 0, 0, width, height);


    draw_coord(cr, 0, 0);

    cairo_scale(cr, width, height);

    cairo_pattern_t *linpat, *radpat;
    linpat = cairo_pattern_create_linear (0, 0, 1, 1);
    cairo_pattern_add_color_stop_rgb (linpat, 0, 0, 0.3, 0.8);
    cairo_pattern_add_color_stop_rgb (linpat, 1, 0, 0.8, 0.3);

    radpat = cairo_pattern_create_radial (0.5, 0.5, 0.25, 0.5, 0.5, 0.75);
    cairo_pattern_add_color_stop_rgba (radpat, 0, 0, 0, 0, 1);
    cairo_pattern_add_color_stop_rgba (radpat, 0.5, 0, 0, 0, 0);

    cairo_set_source (cr, linpat);
    cairo_mask (cr, radpat);


    return FALSE;
}

void do_pattern(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* drawing_area;

    frame = gtk_frame_new("pattern");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    drawing_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(frame), drawing_area);

    g_signal_connect(drawing_area, "draw", G_CALLBACK (on_draw), NULL);        

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
#include <gtk/gtk.h>



#if 0
//quit app
static void activate_quit (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
  GtkApplication *app = user_data;
  GtkWidget *win;
  GList *list, *next;

  g_print("quit()....\n");
  
  list = gtk_application_get_windows (app);
  while (list)
    {
      win = list->data;
      next = list->next;

      gtk_widget_destroy (GTK_WIDGET (win));

      list = next;
    }
}

//app startup callback
static void startup (GApplication *app)
{
}

#endif

static void apply_cb (GtkToggleButton *togglebutton, gpointer user_data)
{
  GtkWidget *widget = user_data;
  GtkStyleContext *styleCtx;
  styleCtx = gtk_widget_get_style_context(widget);

  if(gtk_toggle_button_get_active(togglebutton))
    gtk_style_context_add_class(styleCtx, "apply");
  else
    gtk_style_context_remove_class(styleCtx, "apply");
}


static void add_button(GtkGrid *grid, gint row, const gchar* name, const gchar* css)
{
  GtkWidget *widget;
  GtkWidget *check_button;
  GError *err = NULL;

  widget = gtk_button_new_with_label(name);
  gtk_widget_set_halign(widget, GTK_ALIGN_CENTER);
  gtk_widget_set_hexpand (widget, TRUE);
  // gtk_widget_set_vexpand (widget, TRUE);
  gtk_grid_attach(GTK_GRID(grid), widget, 0, row, 1, 1);

  check_button = gtk_check_button_new_with_label("add 'apply' class");
  gtk_widget_set_halign(check_button, GTK_ALIGN_CENTER);
  // gtk_widget_set_hexpand (check_button, TRUE);
  gtk_grid_attach(GTK_GRID(grid), check_button, 1, row, 1, 1);

  g_signal_connect(check_button, "toggled", G_CALLBACK(apply_cb), widget);


  //使用css修饰widget有几个步骤,

  //#1.取得gtkStyleContext

  GtkStyleContext *styleCtx;
  styleCtx = gtk_widget_get_style_context(widget);
  // gtk_style_context_add_class(styleCtx, "apply");

  //#2.生成cssProvider
  GtkCssProvider *provider;
  provider = gtk_css_provider_new();

  gtk_css_provider_load_from_data(provider, css, -1, &err);
  g_assert_no_error(err);

  gtk_style_context_add_provider(styleCtx, GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_button), TRUE);

}

static gint row = 0;

static void add_buttons_with_box_properties(GtkGrid *grid)
{  
  GtkWidget *widget;
  GtkWidget *check_button;
  GError *err = NULL;
  static gchar* css=
      ".apply {"
      " margin: 20px 60px 60px 20px;"
      " padding: 20px 60px 60px 20px;"
      "}"
      ;

  add_button(grid, row++, "show box properties", css);
}

static void add_buttons_with_border_properties(GtkGrid *grid)
{  
  GtkWidget *widget;
  GtkWidget *check_button;
  GError *err = NULL;
  static gchar* css=
      ".apply {"
      " border-width: 5px 5px 10px 10px;"
      " border-style: solid inset outset groove;"
      " border-color: red green blue black;"
      " border-radius: 20px;"
      " margin: 0px 2px 2px 0px;"
      // " padding: 0px 2px 2px 0px;"
      "}"
      ".apply:active {"
      " border-width: 5px 5px 10px 10px;"
      " border-style: solid inset outset groove;"
      " border-color: red green blue black;"
      " border-radius: 20px;"
      " margin: 2px 0px 0px 2px;"
      // " padding: 2px 0px 0px 2px;"
      "}"
      ;

  add_button(grid, row++, "show border properties", css);

}


static void add_buttons_with_background_properties(GtkGrid *grid)
{  
  GtkWidget *widget;
  GtkWidget *check_button;
  GError *err = NULL;
  static gchar* css=
      ".apply {"
      // " background-color: red;"
      // " background-color:#ff0;"
      // " background-image: none;"
      " background-image: image(yellow);"
      // " background-clip: padding-box;"
      // " box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.4);"
      // " border-color: red;"
      "}"
      "button.apply:hover {"
      // " background-color: red;"
      // " background-color:#ff0;"
      " background-image: image(green);"
      // " background-image: none;"
      // " background-clip: padding-box;"
      " box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.4);"
      // " border-color: red;"
      "}"
      "button.apply:active {"
      " color: white;"
      // " background-color:#ff0;"
      " background-image: image(blue);"
      // " background-image: none;"
      // " background-clip: padding-box;"
      " box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.4);"
      // " border-color: red;"
      "}"
      ;

  add_button(grid, row++, "show background properties", css);
}

static void add_buttons_with_outline_properties(GtkGrid *grid)
{  
  GtkWidget *widget;
  GtkWidget *check_button;
  GError *err = NULL;
  static gchar* css=      //outline默认用于表现focus
      ".apply {"
      // " outline-style: groove;"
      " outline-style: dotted;"
      " outline-offset: -8px;"
      " outline-width: 2px;"
      " outline-color: green;"
      // " background-clip: padding-box;"
      // " box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.4);"
      // " border-color: red;"
      "}"
      ;

  add_button(grid, row++, "show outline properties", css);
}

static void add_buttons_with_transition_properties(GtkGrid *grid)
{  
  GtkWidget *widget;
  GtkWidget *check_button;
  GError *err = NULL;
  static gchar* css=      //outline默认用于表现focus
      ".apply {"
      " margin: 2px;"
      " padding: 2px;"
      // " background-image: image(red);"
      "}"
      ".apply:hover {"
      // " transition: all 1000ms cubic-bezier(0.25, 0.46, 0.45, 0.94);"
      " transition: all 250ms ease-out;"
      " margin: 8px;"
      " padding: 8px;"
      // " background-image: image(green);"
      " border-color: red;"
      "}"
      ;

  add_button(grid, row++, "show transition properties", css);
}


static void add_buttons_with_font_properties(GtkGrid *grid)
{  
  GtkWidget *widget;
  GtkWidget *check_button;
  GError *err = NULL;
  static gchar* css=      //outline默认用于表现focus
      ".apply {"
      " font-style:normal;"
      " font-weight: normal;"
      "}"
      ".apply:hover {"
      " font-style:italic;"
      " font-weight: bold;"
      "}"
      ;

  add_button(grid, row++, "show font properties", css);
}



//app activate callback
static void activate (GApplication *app)
{
  GtkWidget *window;
  GtkWidget *scrolled;
  GtkWidget *widget;
  // GtkWidget *box;
  GtkWidget *grid;

  g_print("activate()....\n");
  window = gtk_application_window_new(GTK_APPLICATION(app));
  gtk_window_set_title(GTK_WINDOW(window), "a07_cssDemo");
  gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);

  scrolled = gtk_scrolled_window_new(NULL, NULL);    
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled), GTK_POLICY_ALWAYS, GTK_POLICY_ALWAYS);
  gtk_widget_set_hexpand (scrolled, TRUE);
  gtk_widget_set_vexpand (scrolled, TRUE);
  gtk_container_add(GTK_CONTAINER(window), scrolled);

  grid = gtk_grid_new();
  // gtk_grid_set_row_homogeneous(GTK_GRID(grid), FALSE);
  // gtk_grid_set_column_homogeneous(GTK_GRID(grid), FALSE);
  gtk_container_add(GTK_CONTAINER(scrolled), grid);

  add_buttons_with_box_properties(GTK_GRID(grid));
  add_buttons_with_border_properties(GTK_GRID(grid));
  add_buttons_with_background_properties(GTK_GRID(grid));
  add_buttons_with_outline_properties(GTK_GRID(grid));
  add_buttons_with_transition_properties(GTK_GRID(grid));
  add_buttons_with_font_properties(GTK_GRID(grid));

  gtk_widget_show_all (GTK_WIDGET (window));
}


int main(int argc, char* argv[])
{
    GtkApplication *app;

    app = gtk_application_new("com.zcatt.a07_cssDemo", G_APPLICATION_FLAGS_NONE);

#if 0
    static GActionEntry app_entries[] = {
            { "quit", activate_quit, NULL, NULL, NULL },
        };
    g_action_map_add_action_entries(G_ACTION_MAP(app), app_entries, G_N_ELEMENTS(app_entries), app);

    g_signal_connect (app, "startup", G_CALLBACK (startup), NULL);
#endif

    g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);

    g_application_run(G_APPLICATION(app), argc, argv);

    return 0;
}
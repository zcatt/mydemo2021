#include <gtk/gtk.h>


static GdkPixbuf *get_image_pixbuf (GtkImage *image)
{
    const gchar *icon_name;
    GtkIconSize size;
    GtkIconTheme *icon_theme;
    int width;

    switch (gtk_image_get_storage_type (image))
    {
    case GTK_IMAGE_PIXBUF:
        return g_object_ref (gtk_image_get_pixbuf (image));
    case GTK_IMAGE_ICON_NAME:
        gtk_image_get_icon_name (image, &icon_name, &size);
        icon_theme = gtk_icon_theme_get_for_screen (gtk_widget_get_screen (GTK_WIDGET (image)));
        gtk_icon_size_lookup (size, &width, NULL);
        return gtk_icon_theme_load_icon (icon_theme,
                                        icon_name,
                                        width,
                                        GTK_ICON_LOOKUP_GENERIC_FALLBACK,
                                        NULL);
    default:
        g_warning ("Image storage type %d not handled",
                    gtk_image_get_storage_type (image));
        return NULL;
    }
}


static void drag_begin (GtkWidget      *widget,
            GdkDragContext *context,
            gpointer        data)
{
    GdkPixbuf *pixbuf;

    g_print("drag_begin....\n");

#if 0
    pixbuf = get_image_pixbuf(GTK_IMAGE (data));
    gtk_drag_set_icon_pixbuf(context, pixbuf, -2, -2);
    g_object_unref(pixbuf);
#else
    gtk_drag_set_icon_name(context, "edit-paste", 0 , 0);
#endif

}

void
drag_data_get (GtkWidget        *widget,
               GdkDragContext   *context,
               GtkSelectionData *selection_data,
               guint             info,
               guint             time,
               gpointer          data)
{
    GdkPixbuf *pixbuf;

    g_print("drag_get....\n");
    pixbuf = get_image_pixbuf(GTK_IMAGE (data));
    gtk_selection_data_set_pixbuf(selection_data, pixbuf);
    g_object_unref(pixbuf);
}

static void
drag_data_received (GtkWidget        *widget,
                    GdkDragContext   *context,
                    gint              x,
                    gint              y,
                    GtkSelectionData *selection_data,
                    guint             info,
                    guint32           time,
                    gpointer          data)
{
    GdkPixbuf *pixbuf;

    g_print("drag_data_received....\n");

    if (gtk_selection_data_get_length (selection_data) > 0)
    {
      pixbuf = gtk_selection_data_get_pixbuf (selection_data);
      gtk_image_set_from_pixbuf (GTK_IMAGE (data), pixbuf);
      g_object_unref (pixbuf);
    }
}




//app activate callback
static void activate (GApplication *app)
{
    GtkWidget *window;
    GtkWidget *hbox;
    GtkWidget *frame;
    GtkWidget *widget;
    GtkWidget *img1;
    GtkWidget *img2;
    GtkWidget *ebox1;
    GtkWidget *ebox2;

    g_print("activate()....\n");
    window = gtk_application_window_new(GTK_APPLICATION(app));
    gtk_window_set_title(GTK_WINDOW(window), "a08_dnd");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);

    hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_box_set_homogeneous(GTK_BOX(hbox), TRUE);
    gtk_container_add(GTK_CONTAINER(window), hbox);

    //client 1

    frame = gtk_frame_new("client1");
    gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 0);


    ebox1 = gtk_event_box_new ();
    gtk_container_add(GTK_CONTAINER(frame), ebox1);

    GdkPixbuf *pb;
    pb = gdk_pixbuf_new_from_file_at_scale("walywa.jpeg", 300, 300, TRUE, NULL);
    img1 = gtk_image_new_from_pixbuf(pb);
    g_object_unref(pb);

    // img1 = gtk_image_new_from_file("walywa.jpeg");
    //   gtk_widget_set_hexpand (img, TRUE);
    //   gtk_widget_set_vexpand (img, TRUE);
    gtk_container_add(GTK_CONTAINER(ebox1), img1);


    //client 2
    frame = gtk_frame_new("client2");
    gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 0);

    ebox2 = gtk_event_box_new ();                           //GtkImage是no window的widget, 故需使用eventBox以支持DnD event.
    gtk_container_add(GTK_CONTAINER(frame), ebox2);

    //   img2 = gtk_image_new_from_file("hamburger.jpeg");
    //   img2 = gtk_image_new();
    img2 = gtk_image_new_from_icon_name ("face-smile", GTK_ICON_SIZE_DIALOG);  
    //   gtk_widget_set_hexpand (img, TRUE);
    //   gtk_widget_set_vexpand (img, TRUE);
    gtk_container_add(GTK_CONTAINER(ebox2), img2);

    //dnd相关

    gtk_drag_source_set(ebox1, GDK_BUTTON1_MASK, NULL, 0, GDK_ACTION_MOVE);
    gtk_drag_source_add_image_targets (ebox1);
    g_signal_connect (ebox1, "drag-begin", G_CALLBACK (drag_begin), img1);
    g_signal_connect (ebox1, "drag-data-get", G_CALLBACK (drag_data_get), img1);

    gtk_drag_dest_set(ebox1, GTK_DEST_DEFAULT_ALL, NULL, 0, GDK_ACTION_MOVE);
    gtk_drag_dest_add_image_targets(ebox1);
    g_signal_connect (ebox1, "drag-data-received", G_CALLBACK (drag_data_received), img1);



    gtk_drag_source_set(ebox2, GDK_BUTTON1_MASK, NULL, 0, GDK_ACTION_MOVE);
    gtk_drag_source_add_image_targets (ebox2);
    g_signal_connect (ebox2, "drag-begin", G_CALLBACK (drag_begin), img2);
    g_signal_connect (ebox2, "drag-data-get", G_CALLBACK (drag_data_get), img2);

    gtk_drag_dest_set(ebox2, GTK_DEST_DEFAULT_ALL, NULL, 0, GDK_ACTION_MOVE);
    gtk_drag_dest_add_image_targets(ebox2);
    g_signal_connect (ebox2, "drag-data-received", G_CALLBACK (drag_data_received), img2);


    gtk_widget_show_all (GTK_WIDGET (window));
}



int main(int argc, char* argv[])
{
    GtkApplication *app;

    app = gtk_application_new("com.zcatt.a08_dnd", G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);

    g_application_run(G_APPLICATION(app), argc, argv);

    return 0;
}

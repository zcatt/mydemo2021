#include <gtk/gtk.h>


static GdkPixbuf *get_image_pixbuf (GtkImage *image)
{
    const gchar *icon_name;
    GtkIconSize size;
    GtkIconTheme *icon_theme;
    int width;

    switch (gtk_image_get_storage_type (image))
    {
    case GTK_IMAGE_PIXBUF:
        return g_object_ref (gtk_image_get_pixbuf (image));
    case GTK_IMAGE_ICON_NAME:
        gtk_image_get_icon_name (image, &icon_name, &size);
        icon_theme = gtk_icon_theme_get_for_screen (gtk_widget_get_screen (GTK_WIDGET (image)));
        gtk_icon_size_lookup (size, &width, NULL);
        return gtk_icon_theme_load_icon (icon_theme,
                                        icon_name,
                                        width,
                                        GTK_ICON_LOOKUP_GENERIC_FALLBACK,
                                        NULL);
    default:
        g_warning ("Image storage type %d not handled",
                    gtk_image_get_storage_type (image));
        return NULL;
    }
}


static void copy_image (GtkMenuItem *item, gpointer data)
{
    GtkClipboard *clipboard;
    GdkPixbuf *pixbuf;

    clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
    pixbuf = get_image_pixbuf(GTK_IMAGE (data));

    gtk_clipboard_set_image(clipboard, pixbuf);
    g_object_unref (pixbuf);
}

static void paste_image (GtkMenuItem *item, gpointer data)
{
    GtkClipboard *clipboard;
    GdkPixbuf *pixbuf;

    clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
    pixbuf = gtk_clipboard_wait_for_image(clipboard);          //read img from clipboard

    if (pixbuf)
    {
        gtk_image_set_from_pixbuf(GTK_IMAGE(data), pixbuf);
        g_object_unref (pixbuf);
    }
}

static gboolean button_press (GtkWidget      *widget,
              GdkEventButton *button,
              gpointer        data)
{
    GtkWidget *menu;
    GtkWidget *item;

    if (button->button != GDK_BUTTON_SECONDARY)
        return FALSE;
                            //zf, 按右键, 弹出popMenu
    menu = gtk_menu_new ();

    item = gtk_menu_item_new_with_mnemonic ("_Copy");
    g_signal_connect (item, "activate", G_CALLBACK (copy_image), data);
    gtk_widget_show (item);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

    item = gtk_menu_item_new_with_mnemonic ("_Paste");
    g_signal_connect (item, "activate", G_CALLBACK (paste_image), data);
    gtk_widget_show (item);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

    gtk_menu_popup_at_pointer (GTK_MENU (menu), (GdkEvent *) button);
    return TRUE;
}


//app activate callback
static void activate (GApplication *app)
{
    GtkWidget *window;
    GtkWidget *hbox;
    GtkWidget *frame;
    GtkWidget *widget;
    GtkWidget *img1;
    GtkWidget *img2;
    GtkWidget *ebox1;
    GtkWidget *ebox2;

    g_print("activate()....\n");
    window = gtk_application_window_new(GTK_APPLICATION(app));
    gtk_window_set_title(GTK_WINDOW(window), "a09_clipboard");
    gtk_window_set_icon_name(GTK_WINDOW(window), "face-smile");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);

    hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_box_set_homogeneous(GTK_BOX(hbox), TRUE);
    gtk_container_add(GTK_CONTAINER(window), hbox);

    //client 1

    frame = gtk_frame_new("client1");
    gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, TRUE, 0);

    ebox1 = gtk_event_box_new ();
    gtk_container_add(GTK_CONTAINER(frame), ebox1);

    img1 = gtk_image_new_from_file("walywa.jpeg");
    //   gtk_widget_set_hexpand (img, TRUE);
    //   gtk_widget_set_vexpand (img, TRUE);
    gtk_container_add(GTK_CONTAINER(ebox1), img1);


    //client 2
    frame = gtk_frame_new("client2");
    gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, TRUE, 0);

    ebox2 = gtk_event_box_new ();                           //GtkImage是no window的widget, 故需使用eventBox以支持DnD event.
    gtk_container_add(GTK_CONTAINER(frame), ebox2);

    //   img2 = gtk_image_new_from_file("hamburger.jpeg");
    //   img2 = gtk_image_new();
    img2 = gtk_image_new_from_icon_name ("face-smile", GTK_ICON_SIZE_DIALOG);  
    //   gtk_widget_set_hexpand (img, TRUE);
    //   gtk_widget_set_vexpand (img, TRUE);
    gtk_container_add(GTK_CONTAINER(ebox2), img2);

    //clipboard相关
    g_signal_connect(ebox1, "button-press-event", G_CALLBACK(button_press), img1);
    g_signal_connect(ebox2, "button-press-event", G_CALLBACK(button_press), img2);


    gtk_widget_show_all (GTK_WIDGET (window));
}



int main(int argc, char* argv[])
{
    GtkApplication *app;

    app = gtk_application_new("com.zcatt.a09_clipboard", G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);

    g_application_run(G_APPLICATION(app), argc, argv);

    return 0;
}

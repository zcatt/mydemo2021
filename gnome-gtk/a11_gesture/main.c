/*
    a11_gesture -

    示范主要的gesture的使用, 包括gtkGestureMutliPress/LongPress/Drag
    几个要点,
        .有classPrivate时, 无法使用G_DEFINE_TYPE_* macros.展示了classPrivate, instancePrivate情形的no macro实现.
        .gesuture的sigs
        .gesture group. group内seq最多只能1个claimed.
        .
*/
#include <gtk/gtk.h>
#include "mywidget.h"



static void close_window(void)
{
}

static void activate(GtkApplication *app, gpointer *user_data)
{
    GtkWidget *window;
    GtkWidget *vbox;
    GtkWidget *widget;
    GtkWidget *frame;

    //测试touchscreen.
    // gtk_set_debug_flags(gtk_get_debug_flags() | GTK_DEBUG_TOUCHSCREEN);

    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "a11_gesture");
    gtk_window_set_icon_name(GTK_WINDOW(window), "face-smile");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);

    g_signal_connect(window, "destroy", G_CALLBACK(close_window), NULL);
    gtk_container_set_border_width(GTK_CONTAINER(window), 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(window), vbox);

    frame = gtk_frame_new("mywidget");
    gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);

    widget = mywidget_new();
    gtk_container_add(GTK_CONTAINER(frame), widget);

    widget = gtk_statusbar_new();
    gtk_statusbar_push(GTK_STATUSBAR(widget), 0, "statusbar.......");
    gtk_box_pack_start(GTK_BOX(vbox), widget, TRUE, TRUE, 0);

    gtk_widget_show_all(window);
}

int main(int argc, char** argv)
{
    GtkApplication *app;
    int status;

    app = gtk_application_new("com.gitee.zcatt.gnome.example.a11_gesture", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
    return status;
}
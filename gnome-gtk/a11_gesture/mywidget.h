#include <gtk/gtkbin.h>
#include <gtk/gtkgesturesingle.h>

typedef enum 
{
    GESTURE_SIG_0,

    GESTURE_SIG_BEGIN,
    GESTURE_SIG_CANCEL,
    GESTURE_SIG_END,
    GESTURE_SIG_SEQUENCE_STATE_CHANGED,
    GESTURE_SIG_UPDATE,
    
    GESTURE_SIG_MULTIPRESS_PRESSED,
    GESTURE_SIG_MULTIPRESS_RELEASED,
    GESTURE_SIG_MULTIPRESS_STOPPED,

    GESTURE_SIG_LONGPRESS_PRESSED,
    GESTURE_SIG_LONGPRESS_CANCELLED,

    GESTURE_SIG_DRAG_BEGIN,
    GESTURE_SIG_DRAG_UPDATE,
    GESTURE_SIG_DRAG_END,

    GESTURE_SIG_KEY_FOCUS_IN,
    GESTURE_SIG_KEY_FOCUS_OUT,
    GESTURE_SIG_KEY_IM_UPDATE,
    GESTURE_SIG_KEY_PRESSED,
    GESTURE_SIG_KEY_RELEASED,
    GESTURE_SIG_KEY_MODIFIERS,

    GESTURE_SIG_SCROLL_DECELERATE,
    GESTURE_SIG_SCROLL_BEGIN,
    GESTURE_SIG_SCROLL_SCROLL,
    GESTURE_SIG_SCROLL_END,

    GESTURE_SIG_MOTION_ENTER,
    GESTURE_SIG_MOTION_MOTION,
    GESTURE_SIG_MOTION_LEAVE,
} GestureSignalType;

GType gesture_signal_type_get_type(void) G_GNUC_CONST;
#define TYPE_GESTURE_SIGNAL     (gesture_signal_type_get_type())


#define TYPE_MYWIDGET                 (mywidget_get_type ())
#define MYWIDGET(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MYWIDGET, MyWidget))
#define MYWIDGET_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MYWIDGET, MyWidgetClass))
#define IS_MYWIDGET(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MYWIDGET))
#define IS_MYWIDGET_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MYWIDGET))
#define MYWIDGET_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MYWIDGET, MyWidgetClass))

typedef struct _MyWidget                MyWidget;
typedef struct _MyWidgetPrivate         MyWidgetPrivate;
typedef struct _MyWidgetClass           MyWidgetClass;
typedef struct _MyWidgetClassPrivate    MyWidgetClassPrivate;


struct _MyWidgetClass
{
    GtkBinClass parent_class;

    //
    //public
    //

    //signals
    void (*gesture_signal)(MyWidget *widget, GestureSignalType sig_type, gint x, gint y);


    //
    //private
    //
    MyWidgetClassPrivate *priv;
};


struct _MyWidgetClassPrivate
{
    gint no_use;
};


struct _MyWidget
{
    GtkBin bin;

    //
    //private 
    //

    MyWidgetPrivate *priv;
};

struct _MyWidgetPrivate
{
    GtkGesture *multipress_gesture;
    GtkGesture *longpress_gesture;
    GtkGesture *drag_gesture;

    GtkEventController *key_event_controller;
    GtkEventController *motion_event_controller;
    GtkEventController *scroll_event_controller;

    GdkWindow *event_window;        //inputOnly gdkwindow

    gchar* label_text;
    GestureSignalType last_type;
};

GtkWidget* mywidget_new(void);
void mywidget_set_label(MyWidget *my_widget,  const gchar *label);
const gchar *mywidget_get_label(MyWidget *my_widget);


#include "MyDockLibrary.h"



static void my_dock_bar_class_init(MyDockBarClass *klass);
static void my_dock_bar_init(MyDockBar *object);
static void my_dock_bar_dispose(GObject *object);
static void my_dock_bar_finalize(GObject *obj);
static void my_dock_bar_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void my_dock_bar_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);


static GObjectClass *parent_class = NULL;
static gint private_offset;        //用于定位instance中的private.


static inline gpointer my_dock_bar_get_instance_private(MyDockBar *self)
{
  return G_STRUCT_MEMBER_P(self, private_offset);
}


GType my_dock_bar_get_type(void)
{
    static GType my_dock_bar_type = 0;

    if (!my_dock_bar_type)       //lazy creation
    {
        const GTypeInfo type_info =
        {
            sizeof(MyDockBarClass),
            NULL,                                                         //base_class_init()
            NULL,                                                      //base_class_finalize()
            (GClassInitFunc)my_dock_bar_class_init,         //class_finalize
            NULL,                                                     //class_init()
            NULL,                                                       //class_data
            sizeof(MyDockBar),                                          //instance size
            0,                                                           //n_preallocs
            (GInstanceInitFunc)my_dock_bar_init,                          //instance_init
            NULL,                                                       //value_table
        };

        my_dock_bar_type = g_type_register_static (GTK_TYPE_BOX, "MyDockBar", &type_info, 0);
        private_offset = g_type_add_instance_private(my_dock_bar_type, sizeof(MyDockBarPrivate));
    }

    return my_dock_bar_type;
}


MyDockBar* my_dock_bar_new(void)
{
    return MY_DOCK_BAR(g_object_new(TYPE_MY_DOCK_BAR, NULL));
}



static void my_dock_bar_class_init(MyDockBarClass *klass)
{
    GObjectClass *object_class;
    // GtkWidgetClass    *widget_class;
    // GtkContainerClass *container_class;

    parent_class = g_type_class_peek_parent (klass);
    g_type_class_adjust_private_offset(klass, &private_offset);

    object_class = G_OBJECT_CLASS (klass);
    // widget_class = GTK_WIDGET_CLASS (klass);
    // container_class = GTK_CONTAINER_CLASS (klass);

    //
    //methods
    //
    
    object_class->dispose = my_dock_bar_dispose;
    object_class->finalize = my_dock_bar_finalize;
    object_class->set_property = my_dock_bar_set_property;
    object_class->get_property = my_dock_bar_get_property;

    //
    //props
    //

    //
    //signals
    //

}

static void my_dock_bar_init(MyDockBar *object)
{

    //priv members
    object->priv = my_dock_bar_get_instance_private(object);
    // TODO;
}

//清除对外部obj的引用
static void my_dock_bar_dispose(GObject *obj)
{
    // MyDockBar *object = MY_DOCK_BAR(obj);

    // TODO;

    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

//释放资源
static void my_dock_bar_finalize(GObject *obj)
{
    // MyDockBar *object = MY_DOCK_BAR(obj);

//   if (object->buffer)
//     g_object_unref(object->buffer);

    G_OBJECT_CLASS(parent_class)->finalize(obj);
}

static void my_dock_bar_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
    // TODO;
}

static void my_dock_bar_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
    // TODO;
}



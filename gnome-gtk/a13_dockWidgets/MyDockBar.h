#ifndef _MY_DOCK_BAR_H_
#define _MY_DOCK_BAR_H_

#include <gtk/gtk.h>



G_BEGIN_DECLS

#define TYPE_MY_DOCK_BAR           (my_dock_bar_get_type())
#define MY_DOCK_BAR(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_BAR, MyDockBar))
#define MY_DOCK_BAR_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_BAR, MyDockBarClass))
#define IS_MY_DOCK_BAR(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_BAR))
#define IS_MY_DOCK_BAR_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_BAR))
#define MY_DOCK_BAR_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_BAR, MyDockBarClass))

typedef struct _MyDockBarClass           MyDockBarClass;
typedef struct _MyDockBar                MyDockBar;
typedef struct _MyDockBarPrivate         MyDockBarPrivate;


struct _MyDockBarClass
{
    GtkBoxClass parent_class;
    
};


struct _MyDockBar
{
    GtkBox parent;

    MyDockBarPrivate *priv;
};

struct _MyDockBarPrivate
{

};

GType my_dock_bar_get_type(void);
MyDockBar* my_dock_bar_new(void);






G_END_DECLS

#endif /* _MY_DOCK_BAR_H_ */


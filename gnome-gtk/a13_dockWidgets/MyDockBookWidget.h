#ifndef _MY_DOCK_BOOK_WIDGET_H_
#define _MY_DOCK_BOOK_WIDGET_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define TYPE_MY_DOCK_BOOK_WIDGET           (my_dock_book_widget_get_type())
#define MY_DOCK_BOOK_WIDGET(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_BOOK_WIDGET, MyDockBookWidget))
#define MY_DOCK_BOOK_WIDGET_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_BOOK_WIDGET, MyDockBookWidgetClass))
#define IS_MY_DOCK_BOOK_WIDGET(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_BOOK_WIDGET))
#define IS_MY_DOCK_BOOK_WIDGET_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_BOOK_WIDGET))
#define MY_DOCK_BOOK_WIDGET_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_BOOK_WIDGET, MyDockBookWidgetClass))

typedef struct _MyDockBookWidgetClass           MyDockBookWidgetClass;
typedef struct _MyDockBookWidget                MyDockBookWidget;
typedef struct _MyDockBookWidgetPrivate         MyDockBookWidgetPrivate;


struct _MyDockBookWidgetClass
{
    MyDockContainerClass parent_class;
    
};


struct _MyDockBookWidget
{
    MyDockContainer parent;

    MyDockBookWidgetPrivate *priv;
};

struct _MyDockBookWidgetPrivate
{
    gboolean is_floating;

};

GType my_dock_book_widget_get_type(void);
MyDockBookWidget* my_dock_book_widget_new(void);






G_END_DECLS

#endif /* _MY_DOCK_BOOK_WIDGET_H_ */


#include "MyDockLibrary.h"



static void my_dock_box_class_init(MyDockBoxClass *klass);
static void my_dock_box_init(MyDockBox *object);
static void my_dock_box_dispose(GObject *object);
static void my_dock_box_finalize(GObject *obj);
static void my_dock_box_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void my_dock_box_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);


static GObjectClass *parent_class = NULL;
static gint private_offset;        //用于定位instance中的private.


static inline gpointer my_dock_box_get_instance_private(MyDockBox *self)
{
  return G_STRUCT_MEMBER_P(self, private_offset);
}


GType my_dock_box_get_type(void)
{
    static GType my_dock_box_type = 0;

    if (!my_dock_box_type)       //lazy creation
    {
        const GTypeInfo type_info =
        {
            sizeof(MyDockBoxClass),
            NULL,                                                         //base_class_init()
            NULL,                                                      //base_class_finalize()
            (GClassInitFunc)my_dock_box_class_init,         //class_finalize
            NULL,                                                     //class_init()
            NULL,                                                       //class_data
            sizeof(MyDockBox),                                          //instance size
            0,                                                           //n_preallocs
            (GInstanceInitFunc)my_dock_box_init,                          //instance_init
            NULL,                                                       //value_table
        };

        my_dock_box_type = g_type_register_static (TYPE_MY_DOCK_CONTAINER, "MyDockBox", &type_info, 0);
        private_offset = g_type_add_instance_private(my_dock_box_type, sizeof(MyDockBoxPrivate));
    }

    return my_dock_box_type;
}


MyDockBox* my_dock_box_new(void)
{
    return MY_DOCK_BOX(g_object_new(TYPE_MY_DOCK_BOX, NULL));
}



static void my_dock_box_class_init(MyDockBoxClass *klass)
{
    GObjectClass *object_class;
    // GtkWidgetClass    *widget_class;
    // GtkContainerClass *container_class;

    parent_class = g_type_class_peek_parent (klass);
    g_type_class_adjust_private_offset(klass, &private_offset);

    object_class = G_OBJECT_CLASS (klass);
    // widget_class = GTK_WIDGET_CLASS (klass);
    // container_class = GTK_CONTAINER_CLASS (klass);

    //
    //methods
    //
    
    object_class->dispose = my_dock_box_dispose;
    object_class->finalize = my_dock_box_finalize;
    object_class->set_property = my_dock_box_set_property;
    object_class->get_property = my_dock_box_get_property;

    //
    //props
    //

    //
    //signals
    //

}

static void my_dock_box_init(MyDockBox *object)
{

    //priv members
    object->priv = my_dock_box_get_instance_private(object);
    // TODO;
}

//清除对外部obj的引用
static void my_dock_box_dispose(GObject *obj)
{
    // MyDockBox *object = MY_DOCK_BOX(obj);

    // TODO;

    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

//释放资源
static void my_dock_box_finalize(GObject *obj)
{
    // MyDockBox *object = MY_DOCK_RESOURCE_OBJECT(obj);

//   if (object->buffer)
//     g_object_unref(object->buffer);

    G_OBJECT_CLASS(parent_class)->finalize(obj);
}

static void my_dock_box_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
    // TODO;
}

static void my_dock_box_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
    // TODO;
}

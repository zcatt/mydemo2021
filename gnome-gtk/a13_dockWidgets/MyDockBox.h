#ifndef _MY_DOCK_BOX_H_
#define _MY_DOCK_BOX_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define TYPE_MY_DOCK_BOX           (my_dock_box_get_type())
#define MY_DOCK_BOX(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_BOX, MyDockBox))
#define MY_DOCK_BOX_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_BOX, MyDockBoxClass))
#define IS_MY_DOCK_BOX(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_BOX))
#define IS_MY_DOCK_BOX_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_BOX))
#define MY_DOCK_BOX_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_BOX, MyDockBoxClass))

typedef struct _MyDockBoxClass           MyDockBoxClass;
typedef struct _MyDockBox                MyDockBox;
typedef struct _MyDockBoxPrivate         MyDockBoxPrivate;


struct _MyDockBoxClass
{
    MyDockContainerClass parent_class;
    
};


struct _MyDockBox
{
    MyDockContainer parent;

    MyDockBoxPrivate *priv;
};

struct _MyDockBoxPrivate
{

};

GType my_dock_box_get_type(void);
MyDockBox* my_dock_box_new(void);






G_END_DECLS

#endif /* _MY_DOCK_BOX_H_ */


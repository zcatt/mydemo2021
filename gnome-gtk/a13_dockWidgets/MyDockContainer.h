#ifndef _MY_DOCK_CONTAINER_H_
#define _MY_DOCK_CONTAINER_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define TYPE_MY_DOCK_CONTAINER           (my_dock_container_get_type())
#define MY_DOCK_CONTAINER(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_CONTAINER, MyDockContainer))
#define MY_DOCK_CONTAINER_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_CONTAINER, MyDockContainerClass))
#define IS_MY_DOCK_CONTAINER(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_CONTAINER))
#define IS_MY_DOCK_CONTAINER_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_CONTAINER))
#define MY_DOCK_CONTAINER_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_CONTAINER, MyDockContainerClass))

typedef struct _MyDockContainerClass           MyDockContainerClass;
typedef struct _MyDockContainer                MyDockContainer;
typedef struct _MyDockContainerPrivate         MyDockContainerPrivate;


struct _MyDockContainerClass
{
    MyDockObjectClass parent_class;
    
};


struct _MyDockContainer
{
    MyDockObject parent;

    MyDockContainerPrivate *priv;
};

struct _MyDockContainerPrivate
{

};

GType my_dock_container_get_type(void);
MyDockContainer* my_dock_container_new(void);






G_END_DECLS

#endif /* _MY_DOCK_CONTAINER_H_ */


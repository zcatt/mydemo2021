#ifndef _MY_DOCK_LIBRARY_H_
#define _MY_DOCK_LIBRARY_H_

#include <gtk/gtk.h>
#include "MyDockResourceMaster.h"
#include "MyDockPreviewWindow.h"
#include "MyDockBoardWindow.h"
#include "MyDockMaster.h"
#include "MyDockBar.h"
#include "MyDockObject.h"
#include "MyDockPage.h"
#include "MyDockPlaceholder.h"
#include "MyDockContainer.h"
#include "MyDockBox.h"
#include "MyDockNotebook.h"
#include "MyDockReadWrite.h"
#include "MyDockRootWidget.h"
#include "MyDockBookWidget.h"
// #include "MyDockGrid.h"
// #include "MyDockBorderLayout.h"


G_BEGIN_DECLS


typedef enum {
    MY_DOCK_PAGE_BEH_NORMAL           = 0,
    MY_DOCK_PAGE_BEH_NEVER_FLOATING   = 1 << 0,
    MY_DOCK_PAGE_BEH_NEVER_VERTICAL   = 1 << 1,
    MY_DOCK_PAGE_BEH_NEVER_HORIZONTAL = 1 << 2,
    MY_DOCK_PAGE_BEH_LOCKED           = 1 << 3,
    MY_DOCK_PAGE_BEH_CANT_DOCK_TOP    = 1 << 4,
    MY_DOCK_PAGE_BEH_CANT_DOCK_BOTTOM = 1 << 5,
    MY_DOCK_PAGE_BEH_CANT_DOCK_LEFT   = 1 << 6,
    MY_DOCK_PAGE_BEH_CANT_DOCK_RIGHT  = 1 << 7,
    MY_DOCK_PAGE_BEH_CANT_DOCK_CENTER = 1 << 8,
    MY_DOCK_PAGE_BEH_CANT_CLOSE       = 1 << 9,
    MY_DOCK_PAGE_BEH_CANT_ICONIFY     = 1 << 10,
    MY_DOCK_PAGE_BEH_NO_GRIP          = 1 << 11
} MyDockPageBehavior;


gboolean my_dock_library_init(void);
void my_dock_library_exit(void);

G_END_DECLS

#endif /* _MY_DOCK_LIBRARY_H_ */


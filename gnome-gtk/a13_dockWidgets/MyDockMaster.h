#ifndef _MY_DOCK_MASTER_H_
#define _MY_DOCK_MASTER_H_

#include <gtk/gtk.h>
#include "MyDockObject.h"


G_BEGIN_DECLS

#define TYPE_MY_DOCK_MASTER                 (my_dock_master_get_type())
#define MY_DOCK_MASTER(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_MASTER, MyDockMaster))
#define MY_DOCK_MASTER_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_MASTER, MyDockMasterClass))
#define IS_MY_DOCK_MASTER(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_MASTER))
#define IS_MY_DOCK_MASTER_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_MASTER))
#define MY_DOCK_MASTER_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_MASTER, MyDockMasterClass))

typedef struct _MyDockMasterClass           MyDockMasterClass;
typedef struct _MyDockMaster                MyDockMaster;
typedef struct _MyDockMasterPrivate         MyDockMasterPrivate;


struct _MyDockMasterClass
{
    GObjectClass parent_class;
    
};


struct _MyDockMaster
{
    GObject parent;

    MyDockMasterPrivate *priv;
};

struct _MyDockMasterPrivate
{
    GHashTable *dock_objects;       //hashtable< const gchar* obj_name, MyDockObject *obj>
    GList *toplevel_objects;         //list<MyDockRootWidget*>, 按照头是floating window的, 尾巴是非floating window的情形
    MyDockObject *controller;       //代表该dock master的dock obj, 称作master的controller


    gint serial_number;             //用于nameless dock obj的命名. 由此递增而命名 
    gchar *default_name;


    //管理dock dragging过程,
    MyDockObject *applicant;
    MyDockObject *target;
    //todo

};

GType my_dock_master_get_type(void);
MyDockMaster* my_dock_master_new(void);






G_END_DECLS

#endif /* _MY_DOCK_MASTER_H_ */


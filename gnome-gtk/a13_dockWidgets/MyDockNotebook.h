#ifndef _MY_DOCK_NOTEBOOK_H_
#define _MY_DOCK_NOTEBOOK_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define TYPE_MY_DOCK_NOTEBOOK           (my_dock_notebook_get_type())
#define MY_DOCK_NOTEBOOK(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_NOTEBOOK, MyDockNotebook))
#define MY_DOCK_NOTEBOOK_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_NOTEBOOK, MyDockNotebookClass))
#define IS_MY_DOCK_NOTEBOOK(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_NOTEBOOK))
#define IS_MY_DOCK_NOTEBOOK_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_NOTEBOOK))
#define MY_DOCK_NOTEBOOK_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_NOTEBOOK, MyDockNotebookClass))

typedef struct _MyDockNotebookClass           MyDockNotebookClass;
typedef struct _MyDockNotebook                MyDockNotebook;
typedef struct _MyDockNotebookPrivate         MyDockNotebookPrivate;


struct _MyDockNotebookClass
{
    MyDockContainerClass parent_class;
    
};


struct _MyDockNotebook
{
    MyDockContainer parent;

    MyDockNotebookPrivate *priv;
};

struct _MyDockNotebookPrivate
{

};

GType my_dock_notebook_get_type(void);
MyDockNotebook* my_dock_notebook_new(void);






G_END_DECLS

#endif /* _MY_DOCK_NOTEBOOK_H_ */


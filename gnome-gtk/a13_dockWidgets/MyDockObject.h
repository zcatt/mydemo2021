#ifndef _MY_DOCK_OBJECT_H_
#define _MY_DOCK_OBJECT_H_

#include <gtk/gtk.h>



G_BEGIN_DECLS

#define TYPE_MY_DOCK_OBJECT           (my_dock_object_get_type())
#define MY_DOCK_OBJECT(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_OBJECT, MyDockObject))
#define MY_DOCK_OBJECT_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_OBJECT, MyDockObjectClass))
#define IS_MY_DOCK_OBJECT(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_OBJECT))
#define IS_MY_DOCK_OBJECT_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_OBJECT))
#define MY_DOCK_OBJECT_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_OBJECT, MyDockObjectClass))

typedef struct _MyDockObjectClass           MyDockObjectClass;
typedef struct _MyDockObject                MyDockObject;
typedef struct _MyDockObjectPrivate         MyDockObjectPrivate;


struct _MyDockObjectClass
{
    GtkContainerClass parent_class;
    
};


struct _MyDockObject
{
    GtkContainer parent;

    MyDockObjectPrivate *priv;
};

struct _MyDockObjectPrivate
{
    int id;                     //unique in dock master.
    gchar* name;                //noname时, 由master负责分配default_name
    MyDockMaster *master;       //所在的dock master

    gchar* icon;
    gchar* title;
    gboolean is_pin_on;         //true, pin icon处于pin状态
    gboolean is_pin_shown;
    
    gboolean is_automatic;      //master controls this dockObj's lifecycle. 自动释放
    gboolean is_locked;         //不允许dock dragging this obj.
    gboolean  show_close_button;//强制显示close button. 

    //中间量, 临时产生

};

GType my_dock_object_get_type(void);
MyDockObject* my_dock_object_new(void);






G_END_DECLS

#endif /* _MY_DOCK_OBJECT_H_ */


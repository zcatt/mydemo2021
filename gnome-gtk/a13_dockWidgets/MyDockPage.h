#ifndef _MY_DOCK_PAGE_H_
#define _MY_DOCK_PAGE_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define TYPE_MY_DOCK_PAGE           (my_dock_page_get_type())
#define MY_DOCK_PAGE(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_PAGE, MyDockPage))
#define MY_DOCK_PAGE_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_PAGE, MyDockPageClass))
#define IS_MY_DOCK_PAGE(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_PAGE))
#define IS_MY_DOCK_PAGE_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_PAGE))
#define MY_DOCK_PAGE_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_PAGE, MyDockPageClass))

typedef struct _MyDockPageClass           MyDockPageClass;
typedef struct _MyDockPage                MyDockPage;
typedef struct _MyDockPagePrivate         MyDockPagePrivate;


struct _MyDockPageClass
{
    MyDockObjectClass parent_class;
    
};


struct _MyDockPage
{
    MyDockObject parent;

    MyDockPagePrivate *priv;
};

struct _MyDockPagePrivate
{
    GtkWidget *child;               //仅容纳1个widget child

    MyDockPageBehavior behavior;    //this page follows the behaviors.
    GtkOrientation orientation;

    gboolean is_iconified;          //隐藏, 出现在myDockBar中. 
    gboolean can_resized;           //myDockBox中允许拖动改变尺寸

    //dock dragging相关

    gboolean in_predrag;
    gboolean in_drag;

    gint dragoff_x;
    gint dragoff_y;
    
};

GType my_dock_page_get_type(void);
MyDockPage* my_dock_page_new(void);






G_END_DECLS

#endif /* _MY_DOCK_PAGE_H_ */


#ifndef _MY_DOCK_PLACEHOLDER_H_
#define _MY_DOCK_PLACEHOLDER_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define TYPE_MY_DOCK_PLACEHOLDER           (my_dock_placeholder_get_type())
#define MY_DOCK_PLACEHOLDER(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_PLACEHOLDER, MyDockPlaceholder))
#define MY_DOCK_PLACEHOLDER_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_PLACEHOLDER, MyDockPlaceholderClass))
#define IS_MY_DOCK_PLACEHOLDER(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_PLACEHOLDER))
#define IS_MY_DOCK_PLACEHOLDER_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_PLACEHOLDER))
#define MY_DOCK_PLACEHOLDER_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_PLACEHOLDER, MyDockPlaceholderClass))

typedef struct _MyDockPlaceholderClass           MyDockPlaceholderClass;
typedef struct _MyDockPlaceholder                MyDockPlaceholder;
typedef struct _MyDockPlaceholderPrivate         MyDockPlaceholderPrivate;


struct _MyDockPlaceholderClass
{
    MyDockObjectClass parent_class;
    
};


struct _MyDockPlaceholder
{
    MyDockObject parent;

    MyDockPlaceholderPrivate *priv;
};

struct _MyDockPlaceholderPrivate
{

};

GType my_dock_placeholder_get_type(void);
MyDockPlaceholder* my_dock_placeholder_new(void);






G_END_DECLS

#endif /* _MY_DOCK_PLACEHOLDER_H_ */


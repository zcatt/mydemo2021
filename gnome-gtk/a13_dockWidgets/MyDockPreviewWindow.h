#ifndef _MY_DOCK_PREVIEW_WINDOW_H_
#define _MY_DOCK_PREVIEW_WINDOW_H_

#include <gtk/gtk.h>

G_BEGIN_DECLS



#define TYPE_MY_DOCK_PREVIEW_WINDOW                 (my_dock_preview_window_get_type ())
#define MY_DOCK_PREVIEW_WINDOW(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_PREVIEW_WINDOW, MyDockPreviewWindow))
#define MY_DOCK_PREVIEW_WINDOW_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_PREVIEW_WINDOW, MyDockPreviewWindowClass))
#define IS_MY_DOCK_PREVIEW_WINDOW(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_PREVIEW_WINDOW))
#define IS_MY_DOCK_PREVIEW_WINDOW_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_PREVIEW_WINDOW))
#define MY_DOCK_PREVIEW_WINDOW_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_PREVIEW_WINDOW, MyDockPreviewWindowClass))

typedef struct _MyDockPreviewWindowClass           MyDockPreviewWindowClass;
typedef struct _MyDockPreviewWindow                MyDockPreviewWindow;
typedef struct _MyDockPreviewWindowPrivate         MyDockPreviewWindowPrivate;


struct _MyDockPreviewWindowClass
{
    GtkWindowClass parent_class;
};


struct _MyDockPreviewWindow
{
    GtkWindow parent_instance;

    //
    //private 
    //

    MyDockPreviewWindowPrivate *priv;
};

struct _MyDockPreviewWindowPrivate
{
    //no use
};

GType my_dock_preview_window_get_type(void);
GtkWidget* my_dock_preview_window_new(void);
void my_dock_preview_window_update(MyDockPreviewWindow *widget,  const GdkRectangle *rect);



G_END_DECLS

#endif /* _MY_DOCK_PREVIEW_WINDOW_H_ */


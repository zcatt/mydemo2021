#include "MyDockReadWrite.h"



static void my_dock_read_write_class_init(MyDockReadWriteClass *klass);
static void my_dock_read_write_init(MyDockReadWrite *master);
static void my_dock_read_write_dispose(GObject *object);
static void my_dock_read_write_finalize(GObject *obj);
static void my_dock_read_write_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void my_dock_read_write_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);


static GObjectClass *parent_class = NULL;
static gint private_offset;        //用于定位instance中的private.


static inline gpointer my_dock_read_write_get_instance_private(MyDockReadWrite *self)
{
  return G_STRUCT_MEMBER_P(self, private_offset);
}


GType my_dock_read_write_get_type(void)
{
    static GType my_dock_read_write_type = 0;

    if (!my_dock_read_write_type)       //lazy creation
    {
        const GTypeInfo type_info =
        {
            sizeof(MyDockReadWriteClass),
            NULL,                                                         //base_class_init()
            NULL,                                                      //base_class_finalize()
            (GClassInitFunc)my_dock_read_write_class_init,         //class_finalize
            NULL,                                                     //class_init()
            NULL,                                                       //class_data
            sizeof(MyDockReadWrite),                                          //instance size
            0,                                                           //n_preallocs
            (GInstanceInitFunc)my_dock_read_write_init,                          //instance_init
            NULL,                                                       //value_table
        };

        my_dock_read_write_type = g_type_register_static (G_TYPE_OBJECT, "MyDockReadWrite", &type_info, 0);
        private_offset = g_type_add_instance_private(my_dock_read_write_type, sizeof(MyDockReadWritePrivate));
    }

    return my_dock_read_write_type;
}


MyDockReadWrite* my_dock_read_write_new(void)
{
    return MY_DOCK_READ_WRITE(g_object_new(TYPE_MY_DOCK_READ_WRITE, NULL));
}



static void my_dock_read_write_class_init(MyDockReadWriteClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);
    g_type_class_adjust_private_offset(klass, &private_offset);


    gobject_class->dispose = my_dock_read_write_dispose;
    gobject_class->finalize = my_dock_read_write_finalize;
    gobject_class->set_property = my_dock_read_write_set_property;
    gobject_class->get_property = my_dock_read_write_get_property;

}

static void my_dock_read_write_init(MyDockReadWrite *master)
{

    //priv members
    master->priv = my_dock_read_write_get_instance_private(master);
    // TODO;
}

//清除对外部obj的引用
static void my_dock_read_write_dispose(GObject *obj)
{
    // MyDockReadWrite *master = MY_DOCK_READ_WRITE(obj);

    // TODO;

    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

//释放资源
static void my_dock_read_write_finalize(GObject *obj)
{
    // MyDockReadWrite *master = MY_DOCK_RESOURCE_MASTER(obj);

//   if (master->buffer)
//     g_object_unref(master->buffer);

    G_OBJECT_CLASS(parent_class)->finalize(obj);
}

static void my_dock_read_write_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
    // TODO;
}

static void my_dock_read_write_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
    // TODO;
}

#ifndef _MY_DOCK_READ_WRITE_H_
#define _MY_DOCK_READ_WRITE_H_

#include <gtk/gtk.h>



G_BEGIN_DECLS

#define TYPE_MY_DOCK_READ_WRITE           (my_dock_read_write_get_type())
#define MY_DOCK_READ_WRITE(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_READ_WRITE, MyDockReadWrite))
#define MY_DOCK_READ_WRITE_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_READ_WRITE, MyDockReadWriteClass))
#define IS_MY_DOCK_READ_WRITE(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_READ_WRITE))
#define IS_MY_DOCK_READ_WRITE_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_READ_WRITE))
#define MY_DOCK_READ_WRITE_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_READ_WRITE, MyDockReadWriteClass))

typedef struct _MyDockReadWriteClass           MyDockReadWriteClass;
typedef struct _MyDockReadWrite                MyDockReadWrite;
typedef struct _MyDockReadWritePrivate         MyDockReadWritePrivate;


struct _MyDockReadWriteClass
{
    GObjectClass parent_class;
    
};


struct _MyDockReadWrite
{
    GObject parent;

    MyDockReadWritePrivate *priv;
};

struct _MyDockReadWritePrivate
{

};

GType my_dock_read_write_get_type(void);
MyDockReadWrite* my_dock_read_write_new(void);






G_END_DECLS

#endif /* _MY_DOCK_READ_WRITE_H_ */


#include "MyDockResourceMaster.h"



static void my_dock_resource_master_class_init(MyDockResourceMasterClass *klass);
static void my_dock_resource_master_init(MyDockResourceMaster *res_master);
static void my_dock_resource_master_finalize(GObject *obj);

static GObjectClass *parent_class = NULL;


GType my_dock_resource_master_get_type(void)
{
    static GType my_dock_resource_master_type = 0;

    if (!my_dock_resource_master_type)       //lazy creation
    {
        const GTypeInfo type_info =
        {
            sizeof(MyDockResourceMasterClass),
            NULL,                                                         //base_class_init()
            NULL,                                                      //base_class_finalize()
            (GClassInitFunc)my_dock_resource_master_class_init,         //class_finalize
            NULL,                                                     //class_init()
            NULL,                                                       //class_data
            sizeof(MyDockResourceMaster),                                          //instance size
            0,                                                           //n_preallocs
            (GInstanceInitFunc)my_dock_resource_master_init,                          //instance_init
            NULL,                                                       //value_table
        };

        my_dock_resource_master_type = g_type_register_static (G_TYPE_OBJECT, "MyDockResourceMaster", &type_info, 0);
    }

    return my_dock_resource_master_type;
}


MyDockResourceMaster* my_dock_resource_master_new(void)
{
    return MY_DOCK_RESOURCE_MASTER(g_object_new(TYPE_MY_DOCK_RESOURCE_MASTER, NULL));
}



static void my_dock_resource_master_class_init(MyDockResourceMasterClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  gobject_class->finalize = my_dock_resource_master_finalize;

}

static void my_dock_resource_master_init(MyDockResourceMaster *res_master)
{
    // TODO;
}

static void my_dock_resource_master_finalize(GObject *obj)
{
    // MyDockResourceMaster *resource_master = MY_DOCK_RESOURCE_MASTER(obj);

//   if (resource_master->buffer)
//     g_object_unref(resource_master->buffer);

    G_OBJECT_CLASS(parent_class)->finalize(obj);
}


#ifndef _MY_DOCK_RESOURCE_MASTER_H_
#define _MY_DOCK_RESOURCE_MASTER_H_

#include <gtk/gtk.h>



G_BEGIN_DECLS

#define TYPE_MY_DOCK_RESOURCE_MASTER           (my_dock_resource_master_get_type())
#define MY_DOCK_RESOURCE_MASTER(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_RESOURCE_MASTER, MyDockResourceMaster))
#define MY_DOCK_RESOURCE_MASTER_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_RESOURCE_MASTER, MyDockResourceMasterClass))
#define IS_MY_DOCK_RESOURCE_MASTER(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_RESOURCE_MASTER))
#define IS_MY_DOCK_RESOURCE_MASTER_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_RESOURCE_MASTER))
#define MY_DOCK_RESOURCE_MASTER_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_RESOURCE_MASTER, MyDockResourceMasterClass))

typedef struct _MyDockResourceMasterClass           MyDockResourceMasterClass;
typedef struct _MyDockResourceMaster                MyDockResourceMaster;


struct _MyDockResourceMasterClass
{
    GObjectClass parent_class;

    
};


struct _MyDockResourceMaster
{
    GObject parent;

    //
    //member
    //

};


GType my_dock_resource_master_get_type(void);
MyDockResourceMaster* my_dock_resource_master_new(void);



G_END_DECLS

#endif /* _MY_DOCK_RESOURCE_MASTER_H_ */


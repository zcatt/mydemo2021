#ifndef _MY_DOCK_ROOT_WIDGET_H_
#define _MY_DOCK_ROOT_WIDGET_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define TYPE_MY_DOCK_ROOT_WIDGET           (my_dock_root_widget_get_type())
#define MY_DOCK_ROOT_WIDGET(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_DOCK_ROOT_WIDGET, MyDockRootWidget))
#define MY_DOCK_ROOT_WIDGET_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_DOCK_ROOT_WIDGET, MyDockRootWidgetClass))
#define IS_MY_DOCK_ROOT_WIDGET(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_DOCK_ROOT_WIDGET))
#define IS_MY_DOCK_ROOT_WIDGET_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_DOCK_ROOT_WIDGET))
#define MY_DOCK_ROOT_WIDGET_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_DOCK_ROOT_WIDGET, MyDockRootWidgetClass))

typedef struct _MyDockRootWidgetClass           MyDockRootWidgetClass;
typedef struct _MyDockRootWidget                MyDockRootWidget;
typedef struct _MyDockRootWidgetPrivate         MyDockRootWidgetPrivate;


struct _MyDockRootWidgetClass
{
    MyDockContainerClass parent_class;
    
};


struct _MyDockRootWidget
{
    MyDockContainer parent;

    MyDockRootWidgetPrivate *priv;
};

struct _MyDockRootWidgetPrivate
{
    gboolean is_floating;           //true, 若this是在floating dock window中.
    
};

GType my_dock_root_widget_get_type(void);
MyDockRootWidget* my_dock_root_widget_new(void);






G_END_DECLS

#endif /* _MY_DOCK_ROOT_WIDGET_H_ */


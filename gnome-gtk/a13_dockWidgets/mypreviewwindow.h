#ifndef _MY_PREVIEW_WINDOW_H_
#define _MY_PREVIEW_WINDOW_H_

#include <gtk/gtk.h>

G_BEGIN_DECLS



#define TYPE_MY_PREVIEW_WINDOW                 (my_preview_window_get_type ())
#define MY_PREVIEW_WINDOW(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MY_PREVIEW_WINDOW, MyPreviewWindow))
#define MY_PREVIEW_WINDOW_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MY_PREVIEW_WINDOW, MyPreviewWindowClass))
#define IS_MY_PREVIEW_WINDOW(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MY_PREVIEW_WINDOW))
#define IS_MY_PREVIEW_WINDOW_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MY_PREVIEW_WINDOW))
#define MY_PREVIEW_WINDOW_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MY_PREVIEW_WINDOW, MyPreviewWindowClass))

typedef struct _MyPreviewWindowClass           MyPreviewWindowClass;
typedef struct _MyPreviewWindow                MyPreviewWindow;
typedef struct _MyPreviewWindowPrivate         MyPreviewWindowPrivate;


struct _MyPreviewWindowClass
{
    GtkWindowClass parent_class;
};


struct _MyPreviewWindow
{
    GtkWindow parent_instance;

    //
    //private 
    //

    MyPreviewWindowPrivate *priv;
};

struct _MyPreviewWindowPrivate
{
    //no use
};

GType my_preview_window_get_type(void);
GtkWidget* my_preview_window_new(void);
void my_preview_window_update(MyPreviewWindow *widget,  const GdkRectangle *rect);



G_END_DECLS

#endif /* _MY_PREVIEW_WINDOW_H_ */


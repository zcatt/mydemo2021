/*
    a11_gesture -

    示范主要的gesture的使用, 包括gtkGestureMutliPress/LongPress/Drag
    几个要点,
        .有classPrivate时, 无法使用G_DEFINE_TYPE_* macros.展示了classPrivate, instancePrivate情形的no macro实现.
        .gesuture的sigs
        .gesture group. group内seq最多只能1个claimed.
        .
*/

extern int test_preview_window(int argc, char** argv);
extern int test_dock_obj(int argc, char** argv);

int main(int argc, char** argv)
{
    // return test_preview_window(argc, argv);
    return test_dock_obj(argc, argv);
}
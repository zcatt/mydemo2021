/*
    a11_gesture -

    示范主要的gesture的使用, 包括gtkGestureMutliPress/LongPress/Drag
    几个要点,
        .有classPrivate时, 无法使用G_DEFINE_TYPE_* macros.展示了classPrivate, instancePrivate情形的no macro实现.
        .gesuture的sigs
        .gesture group. group内seq最多只能1个claimed.
        .
*/

#include "MyDockLibrary.h"


static void activate(GtkApplication *app, gpointer *user_data)
{
    GtkWidget *window;
    GtkWidget *vbox;
    // GtkWidget *widget;
    GtkWidget *frame;
    GtkWidget *vbox2;
    // GtkWidget *btn;

    //测试touchscreen.
    // gtk_set_debug_flags(gtk_get_debug_flags() | GTK_DEBUG_TOUCHSCREEN);

    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "a13_dockWidgets");
    gtk_window_set_icon_name(GTK_WINDOW(window), "face-smile");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);

    // gtk_container_set_border_width(GTK_CONTAINER(window), 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(window), vbox);


    frame = gtk_frame_new("DockObj");
    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 0);

    vbox2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    // gtk_container_set_border_width(GTK_CONTAINER(vbox2), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox2);

    //TODO;

    gtk_widget_show_all(window);
}

int test_dock_obj(int argc, char** argv)
{
    GtkApplication *app;
    int status;

    app = gtk_application_new("com.gitee.zcatt.gnome.example.a13_dockWidgets", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
    return status;
}
/*
    a11_gesture -

    示范主要的gesture的使用, 包括gtkGestureMutliPress/LongPress/Drag
    几个要点,
        .有classPrivate时, 无法使用G_DEFINE_TYPE_* macros.展示了classPrivate, instancePrivate情形的no macro实现.
        .gesuture的sigs
        .gesture group. group内seq最多只能1个claimed.
        .
*/

#include "mypreviewwindow.h"

MyPreviewWindow *preview_window = NULL;

static void close_window(void)
{
    if(preview_window)
    {
        gtk_widget_destroy(GTK_WIDGET(preview_window));
        preview_window = NULL;
    }
}


static void increase_x(GtkWidget *button, gpointer user_data)
{
    if(!preview_window)
        return;

    GdkRectangle rect;
    gtk_window_get_position(GTK_WINDOW(preview_window), &rect.x, &rect.y);
    gtk_window_get_size(GTK_WINDOW(preview_window), &rect.width, &rect.height);
    rect.x += 100;

    my_preview_window_update(preview_window, &rect);
}

static void decrease_x(GtkWidget *button, gpointer user_data)
{
    if(!preview_window)
        return;

    GdkRectangle rect;
    gtk_window_get_position(GTK_WINDOW(preview_window), &rect.x, &rect.y);
    gtk_window_get_size(GTK_WINDOW(preview_window), &rect.width, &rect.height);
    rect.x -= 100;

    my_preview_window_update(preview_window, &rect);
}

static void show_my_preview_window(GtkWidget *button, gpointer user_data)
{
    if(!preview_window)
    {
        GtkWidget *toplevel = gtk_widget_get_toplevel(button);
        GtkAllocation alloc;
        GdkRectangle rect;
        gtk_widget_get_allocation(toplevel, &alloc);

        GdkWindow *gdkwindow = gtk_widget_get_window(toplevel);
#if 0
        gdk_window_get_frame_extents(gdkwindow, &rect);
#else        
        gint x, y;
        gdk_window_get_position(gdkwindow, &x, &y);

        rect.x = alloc.x + x +400;
        rect.y = alloc.y + y;
        rect.width = alloc.width;
        rect.height = alloc.height;
#endif

        preview_window = MY_PREVIEW_WINDOW(my_preview_window_new());
        my_preview_window_update(preview_window, &rect);
    }

    gtk_widget_show(GTK_WIDGET(preview_window));
}

static void activate(GtkApplication *app, gpointer *user_data)
{
    GtkWidget *window;
    GtkWidget *vbox;
    // GtkWidget *widget;
    GtkWidget *frame;
    GtkWidget *vbox2;
    GtkWidget *btn;

    //测试touchscreen.
    // gtk_set_debug_flags(gtk_get_debug_flags() | GTK_DEBUG_TOUCHSCREEN);

    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "a13_dockWidgets");
    gtk_window_set_icon_name(GTK_WINDOW(window), "face-smile");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);

    g_signal_connect(window, "destroy", G_CALLBACK(close_window), NULL);
    gtk_container_set_border_width(GTK_CONTAINER(window), 0);

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    gtk_container_add(GTK_CONTAINER(window), vbox);


    frame = gtk_frame_new("myPreviewViewWindow");
    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 0);

    vbox2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    // gtk_container_set_border_width(GTK_CONTAINER(vbox2), 10);
    gtk_container_add(GTK_CONTAINER(frame), vbox2);


    btn = gtk_button_new_with_label("open MyPreviewWindow");
    gtk_box_pack_start(GTK_BOX(vbox2), btn, FALSE, TRUE, 0);
    g_signal_connect(btn, "clicked", G_CALLBACK(show_my_preview_window), 0);

    btn = gtk_button_new_with_label("inc x");
    gtk_box_pack_start(GTK_BOX(vbox2), btn, FALSE, TRUE, 0);
    g_signal_connect(btn, "clicked", G_CALLBACK(increase_x), 0);

    btn = gtk_button_new_with_label("dec x");
    gtk_box_pack_start(GTK_BOX(vbox2), btn, FALSE, TRUE, 0);
    g_signal_connect(btn, "clicked", G_CALLBACK(decrease_x), 0);

    gtk_widget_show_all(window);
}

int test_preview_window(int argc, char** argv)
{
    GtkApplication *app;
    int status;

    app = gtk_application_new("com.gitee.zcatt.gnome.example.a13_dockWidgets", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
    return status;
}

typedef void (*DemoFunc)(GtkBox *box);


typedef struct 
{
    gchar* parent;
    gchar* name;
    gchar* title;
    DemoFunc func;
} DemoInfo;

//gtkTreeView的treeModel
enum 
{
    NAME_COLUMN,
    TITLE_COLUMN,
    FUNC_COLUMN,
    STYLE_COLUMN,
    NUM_COLUMN
};

void draw_coord(cairo_t *cr, double x, double y);

void do_line(GtkBox *parent);
void do_shape(GtkBox *parent);
void do_operator(GtkBox *parent);
void do_transformation(GtkBox *parent);
void do_ellipse(GtkBox *parent);
void do_clip(GtkBox *parent);
void do_image(GtkBox *parent);
void do_pattern(GtkBox *parent);
void do_pango(GtkBox *parent);
void do_pango_layout(GtkBox *parent);
void do_pango_glyphs(GtkBox *parent);

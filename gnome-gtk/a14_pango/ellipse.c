#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

static gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    guint width, height;
    // GdkRGBA color;
    GtkStyleContext *context;


    context = gtk_widget_get_style_context (widget);

    width = gtk_widget_get_allocated_width (widget);
    height = gtk_widget_get_allocated_height (widget);

    g_print("on_draw()....width=%d,height=%d\n", width, height);

    gtk_render_background (context, cr, 0, 0, width, height);


    draw_coord(cr, 0, 0);


    cairo_matrix_t m;

    cairo_save(cr);                     
    cairo_translate(cr, 150, 200);
    cairo_scale(cr, 1, 0.5);
    cairo_arc(cr, 0, 0, 100, 0, 2*G_PI);
	cairo_set_source_rgba (cr, 0, 0, 0, 1);
    cairo_set_line_width(cr, 20);               //line width会受matrix影响
	cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);                     
    cairo_get_matrix(cr, &m);
    cairo_translate(cr, 450, 200);
    cairo_scale(cr, 1, 0.5);
    cairo_arc(cr, 0, 0, 100, 0, 2*G_PI);
    cairo_set_matrix(cr, &m);                   //在set_line_width前恢复matrix是为了保证line width恒定.
	cairo_set_source_rgba (cr, 0, 0, 0, 1);
    cairo_set_line_width(cr, 20);
	cairo_stroke(cr);
    cairo_restore(cr);



    return FALSE;
}

void do_ellipse(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* drawing_area;

    frame = gtk_frame_new("ellipse");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    drawing_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(frame), drawing_area);

    g_signal_connect(drawing_area, "draw", G_CALLBACK (on_draw), NULL);        

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
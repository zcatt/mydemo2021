#include <math.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

//derived from pango-html-1.42.4/pango-Cairo-Rendering.html
static gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    guint width, height;
    // GdkRGBA color;
    GtkStyleContext *context;


    context = gtk_widget_get_style_context (widget);

    width = gtk_widget_get_allocated_width (widget);
    height = gtk_widget_get_allocated_height (widget);

    g_print("on_draw()....width=%d,height=%d\n", width, height);

    gtk_render_background (context, cr, 0, 0, width, height);


    draw_coord(cr, 0, 0);

    PangoLayout *layout;
    PangoFontDescription *desc;

    //draw text along circle

    cairo_save (cr);

#define RADIUS 150
#define N_WORDS 4
#define FONT "Sans Bold 27"

    int i;

    /* Center coordinates on the middle of the region we are drawing
    */
    cairo_translate (cr, RADIUS, RADIUS);

    /* Create a PangoLayout, set the font and text */
    layout = pango_cairo_create_layout (cr);

    desc = pango_font_description_from_string (FONT);
    pango_layout_set_font_description (layout, desc);
    pango_font_description_free (desc);

    pango_layout_set_text (layout, "Text", -1);

    /* Draw the layout N_WORDS times in a circle */
    for (i = 0; i < N_WORDS; i++)
    {
        int width, height;
        double angle = (360. * i) / N_WORDS;
        double red;

        cairo_save (cr);

        /* Gradient from red at angle == 60 to blue at angle == 240 */
        // red   = (1 + cos((angle - 60) * G_PI / 180.)) / 2;
        red = 1.0 - 1.0/N_WORDS * i;
        cairo_set_source_rgb(cr, red, 0, 1.0 - red);

        cairo_rotate (cr, angle * G_PI / 180.);

        /* Inform Pango to re-layout the text with the new transformation */
        pango_cairo_update_layout (cr, layout);

        pango_layout_get_size (layout, &width, &height);
        cairo_move_to (cr, - ((double)width / PANGO_SCALE) / 2, - RADIUS);
        pango_cairo_show_layout (cr, layout);   //画在current point位置

        cairo_restore (cr);
    }
    /* free the layout object */
    g_object_unref (layout);

    cairo_new_path(cr);
    cairo_arc(cr, 0, 0, RADIUS, 0, 2*G_PI);
    cairo_set_line_width(cr, 1.0);
    cairo_set_source_rgba(cr, 0,0,1, 1);
    cairo_stroke(cr);

    cairo_restore(cr);

    //draw paragraph

    cairo_translate (cr, 10, 300);

    layout = pango_cairo_create_layout (cr);

    desc = pango_font_description_from_string ("sans 12");
    pango_layout_set_font_description (layout, desc);
    pango_font_description_free (desc);

    pango_layout_set_width (layout, 600 * PANGO_SCALE);
    char* text="The Cairo library is a vector graphics library with a powerful rendering model.\n It has such features as anti-aliased primitives, alpha-compositing, and gradients. Multiple backends for Cairo are available, to allow rendering to images, to PDF files, and to the screen on X and on other windowing systems. The functions in this section allow using Pango to render to Cairo surfaces.";
    pango_layout_set_text (layout, text, -1);

    cairo_move_to(cr, 0, 0);
    cairo_set_source_rgb(cr, 1, 0, 0);

    pango_cairo_show_layout(cr, layout);

    g_object_unref (layout);

    return FALSE;
}

void do_pango(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* drawing_area;

    frame = gtk_frame_new("pango");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    drawing_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(frame), drawing_area);

    g_signal_connect(drawing_area, "draw", G_CALLBACK (on_draw), NULL);        

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "demo.h"

void draw_coord(cairo_t *cr, double x, double y)
{
    cairo_save(cr);

    cairo_move_to(cr, x, y);
    cairo_line_to(cr, x+100, y);
	cairo_set_source_rgba (cr, 1, 0, 0, 1);
    cairo_set_line_width(cr, 1);
    cairo_stroke(cr);

    cairo_move_to(cr, x, y);
    cairo_line_to(cr, x, y+100);
	cairo_set_source_rgba (cr, 0, 1, 0, 1);
    cairo_set_line_width(cr, 1);
    cairo_stroke(cr);

    cairo_restore(cr);
}
static gboolean on_draw(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    guint width, height;
    // GdkRGBA color;
    GtkStyleContext *context;


    context = gtk_widget_get_style_context (widget);

    width = gtk_widget_get_allocated_width (widget);
    height = gtk_widget_get_allocated_height (widget);

    g_print("on_draw()....width=%d,height=%d\n", width, height);

    gtk_render_background (context, cr, 0, 0, width, height);


    draw_coord(cr, 0, 0);

	cairo_rectangle (cr, 0, 0, 100, 50);
	cairo_set_source_rgba (cr, 0.5, 0.5, 0.5, 0.5);
	cairo_fill (cr);

#if 0
    cairo_save(cr);
    cairo_translate(cr, 100, 50);
	cairo_rectangle (cr, 0, 0, 100, 50);
	cairo_set_source_rgba (cr, 0.5, 1, 0.5, 0.5);
	cairo_fill (cr);
    cairo_restore(cr);
    
    cairo_save(cr);
    cairo_scale(cr, 1, 2);
	cairo_rectangle (cr, 200, 50, 100, 50);
	cairo_set_source_rgba (cr, 1, 0.5, 0.5, 0.5);
	cairo_fill (cr);
    cairo_restore(cr);

    cairo_save(cr);
	cairo_rectangle (cr, 200, 50, 100, 50);
    cairo_rotate(cr, 60 * G_PI/180);
	cairo_rectangle (cr, 200, 50, 100, 50);
	cairo_set_source_rgba (cr, 1, 1, 0.5, 0.9);
	cairo_fill (cr);
    cairo_restore(cr);
#endif

#if 0
    cairo_save(cr);                     //Pt = T*S*P0
    cairo_translate(cr, 100, 50);
    cairo_scale(cr, 1, 2);
	cairo_rectangle (cr, 0, 0, 100, 50);
	cairo_set_source_rgba (cr, 0.5, 1, 0.5, 0.7);
	cairo_fill (cr);
    cairo_restore(cr);

    cairo_save(cr);                     //Pt = S*T*P0
    cairo_scale(cr, 1, 2);
    cairo_translate(cr, 100, 50);
	cairo_rectangle (cr, 0, 0, 100, 50);
	cairo_set_source_rgba (cr, 0.5, 0.5, 1, 0.7);
	cairo_fill (cr);
    cairo_restore(cr);
#endif    

    cairo_matrix_t m;

    cairo_save(cr);                     //Pt = Scale*Translate*P0, 即先translate再scale
    // cairo_get_matrix(cr, &m);
    cairo_matrix_init_identity(&m);
    cairo_matrix_scale(&m, 1, 2);
    cairo_matrix_translate(&m, 100, 50);
    cairo_transform(cr, &m);
	cairo_rectangle (cr, 0, 0, 100, 50);
	cairo_set_source_rgba (cr, 0.5, 1, 0.5, 0.7);
	cairo_fill (cr);
    cairo_restore(cr);


    cairo_save(cr);                     //Pt = Translate*Scale*P0, 即先translate再scale
    cairo_matrix_init_identity(&m);
    cairo_matrix_translate(&m, 100, 50);
    cairo_matrix_scale(&m, 1, 2);
    cairo_transform(cr, &m);
	cairo_rectangle (cr, 0, 0, 100, 50);
	cairo_set_source_rgba (cr, 0.5, 0.5, 1, 0.7);
	cairo_fill (cr);
    cairo_restore(cr);

#if 1
    cairo_save(cr);                     //Pt = Rotate*Translate*P0, 即先translate再scale
    // cairo_get_matrix(cr, &m);
    cairo_matrix_init_identity(&m);
    cairo_matrix_rotate(&m, 30*G_PI/180);
    cairo_matrix_translate(&m, 200, 50);
    cairo_transform(cr, &m);
	cairo_rectangle (cr, 0, 0, 100, 50);
	cairo_set_source_rgba (cr, 1, 1, 0.5, 0.7); //yellow
	cairo_fill (cr);
    cairo_restore(cr);

    cairo_save(cr);                     //Pt = Translate*Rotate*P0, 即先translate再scale
    cairo_matrix_init_identity(&m);
    cairo_matrix_translate(&m, 200, 50);
    cairo_matrix_rotate(&m, 30*G_PI/180);
    cairo_transform(cr, &m);
	cairo_rectangle (cr, 0, 0, 100, 50);
	cairo_set_source_rgba (cr, 0.5, 1, 1, 0.7); //purple
	cairo_fill (cr);
    cairo_restore(cr);
#endif

    return FALSE;
}

void do_transformation(GtkBox *parent)
{
    GtkWidget* frame;
    GtkWidget* drawing_area;

    frame = gtk_frame_new("transformation");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_box_pack_start(parent, frame, TRUE, TRUE, 0);

    drawing_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(frame), drawing_area);

    g_signal_connect(drawing_area, "draw", G_CALLBACK (on_draw), NULL);        

    gtk_widget_show_all(GTK_WIDGET(parent));    
}
﻿/*
演示，
$./siguser&
[1]   4117
$kill -s USR1 4117
rx SIGUSER1.
$ kill -s USR2 4117
rx SIGUSER2.
$ kill 4117
[1]+  Terminated              ./siguser.exe




*/
#include <signal.h>
#include <unistd.h>
#include <stdio.h>

static void sigUser(int sigNo)
{
    if(sigNo == SIGUSR1)
    {
        printf("rx SIGUSER1.\n");
    }
    else if(sigNo == SIGUSR2)
    {
        printf("rx SIGUSER2.\n");
    }
    else
    {
        printf("rx sig=%d.\n", sigNo);
    }
}

int main(void)
{

    if(signal(SIGUSR1, sigUser) == SIG_ERR)
        printf("Cannot catch SIGUSER1.\n");
    if(signal(SIGUSR2, sigUser) == SIG_ERR)
        printf("Cannot catch SIGUSER2.\n");

    while(1)
    {
        pause();
    }

    return 0;
}
﻿/*
    使用gettext实现i18n的步骤
    .源码中初始化i18n环境， 包括setlocale, bindtextdomain, textdomain
    .源码中使用gettext()包裹i18n字符串
    .使用xgettext工具处理src， 生成pot文件
        $xgettext -a main.cpp -o a03_i18n.pot
    .使用msginit工具处理pot文件， 生成各语言的po文件
        $msginit -l zh_CN.UTF-8 -i a03_i18n.pot  -o a03_i18n.zh_CN.po      生成a03_i18n.zh_CN.po
    .编辑po文件，翻译文本
    .使用msgfmt工具生成mo文件
        $msgfmt a03_i18n.zh_CN.po -o a03_i18.zh_CN.mo
    .部署po文件。 将mo文件拷贝到指定位置， 即源码中的LOCALEDDIR定义的位置
        $mkdir -p zh_CN/LC_MESSAGES
        $cp a03_18n.zh_CN.mo zh_CN/LC_MESSAGES/a03_18n.mo
    .测试， 
        $export LANG=zh_CN.UTF-8
        $./main.exe
    .增量翻译。源码变动后， 生成新的new.pot文件， 使用msgmerge合并到po中。
        msgmerge -s -U a03_18.zh_CN.po new.pot
    .重新部署po

*/
#include <stdio.h>
#include <locale.h>
#include <libintl.h>

#define LOCALEDIR   "."     // /usr/share/locale/<lang>/LC_MESSAGES<package>.po
#define PACKAGE "a03_i18n"
#define _(string)   gettext(string)

int main(int argv, char** argc)
{
    setlocale(LC_ALL, "");
    bindtextdomain(PACKAGE, LOCALEDIR);
    textdomain(PACKAGE);

    printf(_("Hello, the world!\n"));

    return 0;
}